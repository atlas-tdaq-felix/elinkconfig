#
# Project file for the ElinkConfig tool
#
# To generate a Visual Studio project:
#   qmake -t vcapp ElinkConfig.pro
# To generate a Makefile:
#   qmake ElinkConfig.pro
# (or just: qmake)
#
TEMPLATE = app
TARGET   = elinkconfig 

# Create a Qt app
QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += qt thread warn_on exceptions debug_and_release

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug-obj
  MOC_DIR     = debug-obj
  UI_DIR      = debug-obj
  RCC_DIR     = debug-obj
  win32 {
    DESTDIR   = ../Debug
  }
  unix {
    DESTDIR   = debug
  }
# LIBS       += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release-obj
  MOC_DIR     = release-obj
  UI_DIR      = release-obj
  RCC_DIR     = release-obj
  win32 {
    DESTDIR   = ../Release
  }
  unix {
    DESTDIR   = release
  }
# LIBS       += -L../Release
}

unix {
  INCLUDEPATH += ../flxcard
  INCLUDEPATH += ../drivers_rcc
  INCLUDEPATH += ../regmap
  LIBS += ../flxcard/build/libFlxCard.so
  LIBS += ../drivers_rcc/lib64/libDFDebug.so
  LIBS += ../regmap/build/libregmap.a
}

win32 {
  INCLUDEPATH += ../software/flxcard
  INCLUDEPATH += ../software/drivers_rcc
  INCLUDEPATH += ../regmap
  # Application icon reference
  RC_FILE = src/ElinkConfig.rc
}

HEADERS   += src/flxdefs.h
HEADERS   += src/GbtConfig.h
win32 {
  HEADERS += src/DummyFlxCard.h 
}

RESOURCES += forms/resources.qrc

FORMS     += forms/ElinkConfigDialog.ui
FORMS     += forms/ReplicateDialog.ui
FORMS     += forms/GenerateDialog.ui

SOURCES   += src/ElinkConfig.cpp
HEADERS   += src/ElinkConfig.h
SOURCES   += src/main.cpp

SOURCES   += src/ReplicateDialog.cpp
HEADERS   += src/ReplicateDialog.h

SOURCES   += src/FanOutSelectDialog.cpp
HEADERS   += src/FanOutSelectDialog.h

SOURCES   += src/GenerateDialog.cpp
HEADERS   += src/GenerateDialog.h

SOURCES   += src/Encoder8b10b.cpp
HEADERS   += src/Encoder8b10b.h

SOURCES   += src/EmuDataGenerator.cpp
HEADERS   += src/EmuDataGenerator.h

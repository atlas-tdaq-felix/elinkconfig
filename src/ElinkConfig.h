#include <vector>

#include "ui_ElinkConfigDialog.h"
#include <QDialog>
#include <QFile>
#include <QString>

#include "flxdefs.h"
#include "regmap/regmap.h" // For REGMAP_VERSION
#if REGMAP_VERSION < 0x500
#include "GbtConfig.h"
#else
#include "GbtConfig5.h"
#endif // REGMAP_VERSION

class QCheckBox;
class QComboBox;
class QFrame;
class QLabel;
class QPushButton;
class QSpinBox;
class QSignalMapper;

typedef struct epathWidget {
  QFrame     *frame;
  QCheckBox  *enableCheck;
  QComboBox  *modeCombo;
  QLabel     *label;
} epathWidget_t;

typedef struct egroupWidget {
  int         id;
  bool        enabled;
  QFrame     *frame;
  QLabel     *label;
  QComboBox  *ewidthCombo;
  QComboBox  *ttcOptCombo;
  std::vector<epathWidget_t> epaths;
} egroupWidget_t;

class ElinkConfig: public QDialog, Ui_ElinkConfigDialog
{
  Q_OBJECT

 public:
  ElinkConfig();
  ~ElinkConfig();

  std::vector<egroupWidget_t> _egroupToHostWidgets;
  std::vector<egroupWidget_t> _egroupFromHostWidgets;

  std::vector<QPushButton *>  _egroupToHostButtons;
  std::vector<QPushButton *>  _egroupFromHostButtons;

  QSignalMapper              *_ewidthToHostMapper;
  QSignalMapper              *_ewidthFromHostMapper;

  QSignalMapper              *_enableToHostMapper;
  QSignalMapper              *_enableFromHostMapper;

  QSignalMapper              *_modeToHostMapper;
  QSignalMapper              *_modeFromHostMapper;

  QSignalMapper              *_egroupToHostMapper;
  QSignalMapper              *_egroupFromHostMapper;

  QSignalMapper              *_ttcOptionMapper;

  void populateToHostPanel         ( );
  void populateFromHostPanel       ( );

  void configureEpathColors        ( egroupWidget_t &widget,
                                     int linkmode = LINKMODE_GBT );

  void displayEgroupToHost         ( int group );
  void displayEgroupFromHost       ( int group );

  void displayEgroupToHostInfo     ( int group );
  void displayEgroupFromHostInfo   ( int group );

  void displayToHostConfig         ( );
  void displayFromHostConfig       ( );

  void applyEgroupConfiguration    ( );
  void applyLinkMode               ( bool init = true );

 public slots:
  void selectedDeviceChanged       ( int index );

  void changeToHostEwidth          ( int group );
  void changeFromHostEwidth        ( int group );

  void changeFromHostTtcOption     ( int group );

  void changeToHostEnable          ( int index );
  void changeFromHostEnable        ( int index );

  void changeToHostMode            ( int index );
  void changeFromHostMode          ( int index );

  void changeEgroupToHost          ( int index );
  void changeEgroupFromHost        ( int index );

  void changeEcToHostEnable        ( );
  void changeEcFromHostEnable      ( );

  void changeEcToHostMode          ( );
  void changeEcFromHostMode        ( );

  void changeIcToHostEnable        ( );
  void changeIcFromHostEnable      ( );
  void changeIcToHostDmaIndex      ( );

  void changeAuxToHostEnable       ( );
  void changeAuxFromHostEnable     ( );

  void changeTtc2HostEnable        ( );
  void changeTtc2HostDmaIndex      ( );

  void changeTtcClock              ( );

  void changeFromHostLtiTtc        ( );

  void changeLink                  ( int index );
  void changeLinkMode              ( );
  void changeMaxChunk              ( int index );
  void changeHdlcTruncation        ( );
  void changeStreamBit             ( int index );
  void changeToHostDisplay         ( );

  void replicateEgroupToHost       ( );
  void replicateEgroupToHostToAll  ( );
  void disableEgroupToHost         ( );
  void enableEgroupToHost          ( );

  void replicateEgroupFromHost     ( );
  void replicateEgroupFromHostToAll( );
  void disableEgroupFromHost       ( );
  void enableEgroupFromHost        ( );

  void replicateLink               ( );
  void replicateLinkToAll          ( );

  void showAdvancedOptions         ( int checked );
  void showStreamIdOptions         ( int checked );

  void readLinkConfig              ( );
  void toHostFanOutSelect          ( );
  void fromHostFanOutSelect        ( );
  void timeoutConfig               ( );
  void clockConfig                 ( );
  void openConfigFile              ( );
  void saveConfigFile              ( );

  void showSettingsDialog          ( );

  void showGenerateDialog          ( );

  void hideMessage                 ( );

 private:
  void disableGuiItems             ( );
  void reEnableGuiItems            ( );
  void fanOutSelect                ( uint64_t reg_index, QString &title );
  QString epathBitsString          ( uint32_t enables, uint32_t width_code );
  bool openFile                    ( QFile   &filename,
                                     QIODevice::OpenMode mode );
  void showMessage                 ( QString qs );
  void readSettings                ( );
  void writeSettings               ( );
  void closeEvent                  ( QCloseEvent *event );
  void setComboItemEnabled         ( QComboBox *comboBox,
                                     int        index,
                                     bool       enabled );
  bool comboItemEnabled            ( QComboBox *comboBox,
                                     int        index );
  void initFwParameters            ( );
 private:
  std::vector<QSpinBox *>  _spins;
  std::vector<QCheckBox *> _streamChecks;

  QSignalMapper *_spinMapper;
  QSignalMapper *_streamBitMapper;

  // The currently selected GBT and e-group numbers
  int _gbtNr;
  int _egroupNrToHost;
  int _egroupNrFromHost;

  // The firmware configuration read from a device
  uint64_t _fwMode;
  uint64_t _fwDmaToHostDescriptorCount;
  bool _fwDirectModeIncluded;
  flxcard_include_egroup_t _fwEgroupIncludes[7];
  uint64_t _fwElinksAligned[FLX_LINKS+1];

  // The E-link configurations for all GBT links, plus one for the Emulator
  GbtConfig _gbtConfig[FLX_LINKS + 1];

  // E-link number offsets for EC, IC and AUX
  int _ecToHostIndex, _ecFromHostIndex;
  int _icToHostIndex, _icFromHostIndex;
  int _auxToHostIndex, _auxFromHostIndex;

  // Remember AUX visibility
  bool _auxVisible;

  // ITk STRIP firmware setting (LCB/R3L1 swap)
  uint64_t _itkStripLcbR3l1ElinkSwap;

  // Configuration file name
  QString _cfgFileName;

  // Remember elink configuration file directory
  QString _cfgDir;

  // Space to store additional register settings
  std::vector<regsetting_t> _regSettings;

  // Custom image (representing E-link configuration) for ITK STRIP firmware,
  // replacing FromHost Egroup widgets
  QLabel *_stripsImage;
};

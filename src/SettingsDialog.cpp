#include "SettingsDialog.h"
#include <QMessageBox>
#include <QTimer>
#include <QIntValidator>

#include "flxcard/FlxCard.h"

// ----------------------------------------------------------------------------

SettingsDialog::SettingsDialog( QWidget *parent,
                                std::vector<regsetting_t> *settings )
  : QDialog( parent ),
    _settings( settings )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );
  this->setWindowTitle( "Additional register settings" );

  _tableWidgetSettings->setColumnCount( 3 );
  _tableWidgetSettings->setHorizontalHeaderLabels( {"Name","Value",""} );
  _tableWidgetSettings->setSelectionBehavior( QAbstractItemView::SelectRows );
  _tableWidgetSettings->setSelectionMode( QAbstractItemView::ExtendedSelection );

  _pushButtonUp->setEnabled( false );
  _pushButtonDown->setEnabled( false );

  // Fill table with the current settings
  QTableWidgetItem *item;
  int row = 0;
  for( regsetting_t &rs : *settings )
    {
      _tableWidgetSettings->insertRow( row );
      // Name
      item = new QTableWidgetItem( rs.name.c_str() );
      _tableWidgetSettings->setItem( row, 0, item );
      // Value
      QString qs = QString( "%1" ).arg( rs.value, 0, 16 ).toUpper();
      item = new QTableWidgetItem( QString("0x") + qs );
      _tableWidgetSettings->setItem( row, 1, item );
      // Remove 'button'
      item = new QTableWidgetItem( "" );
      item->setTextAlignment( Qt::AlignHCenter );
      item->setIcon( QIcon(QPixmap(":/icon/icons/red-x.png")) );
      item->setToolTip( "Remove" );
      _tableWidgetSettings->setItem( row, 2, item );
      ++row;
    }

  connect( _pushButtonAdd, SIGNAL( clicked() ), this, SLOT( addSetting() ) );
  connect( _pushButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( _pushButtonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );

  connect( _pushButtonUp, SIGNAL( clicked() ), this, SLOT( moveUp() ) );
  connect( _pushButtonDown, SIGNAL( clicked() ), this, SLOT( moveDown() ) );

  connect( _tableWidgetSettings, SIGNAL( itemClicked(QTableWidgetItem*) ),
           this, SLOT( itemClicked(QTableWidgetItem*) ) );
  connect( _tableWidgetSettings, SIGNAL( itemDoubleClicked(QTableWidgetItem*) ),
           this, SLOT( itemDoubleClicked(QTableWidgetItem*) ) );
  connect( _tableWidgetSettings, SIGNAL( itemChanged(QTableWidgetItem*) ),
           this, SLOT( itemChanged(QTableWidgetItem*) ) );
  connect( _tableWidgetSettings, SIGNAL( itemSelectionChanged() ),
           this, SLOT( itemSelectionChanged() ) );

  // To get the tablewidget properly sized
  QTimer::singleShot( 0, this, SLOT(resizeIt()) );
}

// ----------------------------------------------------------------------------

SettingsDialog::~SettingsDialog()
{
}

// ----------------------------------------------------------------------------

void SettingsDialog::resizeEvent( QResizeEvent *event )
{
  QHeaderView *hv = _tableWidgetSettings->horizontalHeader();
  int w = hv->width();
  hv->resizeSection( 0, (w*63)/100 );
  hv->resizeSection( 1, (w*30)/100 );
  hv->resizeSection( 2, (w*7)/100 );

  QDialog::resizeEvent( event );
}

// ----------------------------------------------------------------------------

void SettingsDialog::addSetting()
{
  // Remove whitespace
  QString qname = _lineEditName->text().trimmed();

  if( qname.isEmpty() )
    return;

  // First check whether the name given is a valid FELIX register or bitfield;
  // if not, give warning (and provide suggestions?);
  // also give a warning about being read-only
  std::string name = qname.toStdString();
  regmap_bitfield_t *bf = FlxCard::cfg_bitfield( name.c_str() );
  regmap_register_t *reg = 0;
  if( bf == 0 )
    {
      // Not a bitfield name, perhaps a register name?
      reg = FlxCard::cfg_register( name.c_str() );
      if( reg == 0 )
        {
          // Suggest only names that start with the given name string
          std::string options_list;
          options_list = FlxCard::cfg_bitfield_options( name.c_str(), false );
          QString lst;
          if( !options_list.empty() )
            lst = QString("\nSuggested names:\n") + QString( options_list.c_str() );
          QMessageBox::warning( this, "Bitfield or Register",
                                QString( "Unknown name: " ) +
                                qname + lst );
          return;
        }

      // Check if the register can be written
      if( (reg->flags & REGMAP_REG_WRITE) == 0 )
        {
          QMessageBox::warning( this, "Register",
                                QString( "%1\nis read-only!" ).
                                arg( qname ) );
          return;
        }
    }
  else
    {
      // Check if the bitfield can be written
      if( (bf->flags & REGMAP_REG_WRITE) == 0 )
        {
          QMessageBox::warning( this, "Bitfield or Register",
                                QString( "%1\nis read-only!" ).
                                arg( qname ) );
          return;
        }
    }

  // Don't add anything to the settings list if no value is entered yet
  if( _lineEditValue->text().isEmpty() )
    return;

  // The value given
  uint64_t val = 0;
  QString sval = _lineEditValue->text().trimmed();
  bool val_ok;
  if( sval[0] == '0' && (sval[1] == 'x' || sval[1] == 'X') )
    val = sval.toULong( &val_ok, 16 );
  else
    val = sval.toULong( &val_ok, 10 );

  if( bf )
    {
      // Check if the value given falls within range of the bitfield
      uint64_t maxval = bf->mask >> bf->shift;
      QString max = QString( "%1" ).arg( maxval, 0, 16 ).toUpper();
      if( val_ok && val > maxval )
        {
          QMessageBox::warning( this, "Bitfield",
                                QString( "%1:\nValue not in range: "
                                         "[0..0x%2]" ).
                                arg( qname ).arg( max ) );
          return;
        }
    }
  else
    {
      // It's a register name (or we wouldn't end up here; see above)
    }

  if( !val_ok )
    {
      QMessageBox::warning( this, "Value",
                            QString( "Illegal value given: " ) + sval );
      return;
    }

  // Add the new setting at the top of the table
  int row = 0;

  // Name
  QTableWidgetItem *item;
  _tableWidgetSettings->insertRow( 0 );
  item = new QTableWidgetItem( qname.toUpper() );
  //item->setFlags( item->flags() & ~Qt::ItemIsSelectable );
  _tableWidgetSettings->setItem( row, 0, item );

  // Value
  sval = QString( "%1" ).arg( val, 0, 16 ).toUpper();
  item = new QTableWidgetItem( QString("0x") + sval );
  //item->setFlags( item->flags() & ~Qt::ItemIsSelectable );
  _tableWidgetSettings->setItem( row, 1, item );

  // Remove 'button'
  item = new QTableWidgetItem( "" );
  item->setTextAlignment( Qt::AlignHCenter );
  item->setIcon( QIcon(QPixmap(":/icon/icons/red-x.png")) );
  item->setToolTip( "Remove" );
  _tableWidgetSettings->setItem( row, 2, item );
}

// ----------------------------------------------------------------------------

void SettingsDialog::itemClicked( QTableWidgetItem *item )
{
  if( !item ) return;
  if( item->column() != 2 ) return;

  // Move item's name and value into line edits
  int row = item->row();
  _lineEditName->setText( _tableWidgetSettings->item( row, 0 )->text() );
  _lineEditValue->setText( _tableWidgetSettings->item( row, 1 )->text() );

  // Remove the item from the table
  _tableWidgetSettings->removeRow( row );
  _tableWidgetSettings->clearSelection();
}

// ----------------------------------------------------------------------------

void SettingsDialog::itemDoubleClicked( QTableWidgetItem *item )
{
  if( !item ) return;

  _itemTextCopy = item->text();

  // Deselect everything selected so far, except this item
  QList<QTableWidgetItem *> items = _tableWidgetSettings->selectedItems();
  for( QTableWidgetItem *i : items )
    if( i != item )
      i->setSelected( false );
}

// ----------------------------------------------------------------------------

void SettingsDialog::itemChanged( QTableWidgetItem *item )
{
  if( !item ) return;

  // Only 'name' or 'value' column can be edited
  if( !(item->column() == 0 || item->column() == 1) )
    return;

  int row = item->row();
  QTableWidgetItem *name_item = _tableWidgetSettings->item( row, 0 );
  QTableWidgetItem *val_item  = _tableWidgetSettings->item( row, 1 );

  if( !name_item || !val_item ) return;

  // Check (new?) name
  QString qname = name_item->text().trimmed();
  std::string name = qname.toStdString();
  regmap_bitfield_t *bf = FlxCard::cfg_bitfield( name.c_str() );
  if( bf == 0 )
    {
      // Not a bitfield name, perhaps a register name?
      regmap_register_t *reg = FlxCard::cfg_register( name.c_str() );
      if( reg == 0 )
        {
          // Not a register name either?
          // Compile a list of options based on the text entered and show it
          std::string options_list;
          options_list = FlxCard::cfg_bitfield_options( name.c_str(), false );
          QString lst;
          if( !options_list.empty() )
            lst = QString("\nSuggested names:\n") + QString( options_list.c_str() );
          QMessageBox::warning( this, "Bitfield or Register",
                                QString( "Unknown name: " ) +
                                qname + lst );

          // Restore the original name
          name_item->setText( _itemTextCopy );
          return;
        }
    }
  name_item->setText( qname );

  // Check (new?) value
  uint64_t val = 0;
  QString sval = val_item->text().trimmed();
  bool val_ok;
  if( sval[0] == '0' && (sval[1] == 'x' || sval[1] == 'X') )
    val = sval.toULong( &val_ok, 16 );
  else
    val = sval.toULong( &val_ok, 10 );

  if( bf )
    {
      // Check if the value given falls within range of the bitfield
      uint64_t maxval = bf->mask >> bf->shift;
      QString max = QString( "%1" ).arg( maxval, 0, 16 ).toUpper();
      if( val_ok && val > maxval )
        {
          QMessageBox::warning( this, "Bitfield",
                                QString( "%1:\nValue not in range: "
                                         "[0..0x%2]" ).
                                arg( qname ).arg( max ) );

          // Restore the original value
          val_item->setText( _itemTextCopy );
          return;
        }
    }
  else
    {
      // It's a register name (or we wouldn't end up here; see above)
    }

  if( !val_ok )
    {
      QMessageBox::warning( this, "Value",
                            QString( "Illegal value given: " ) + sval );
      // Restore the original value
      val_item->setText( _itemTextCopy );
      return;
    }
}

// ----------------------------------------------------------------------------

void SettingsDialog::itemSelectionChanged()
{
  if( _tableWidgetSettings->selectedItems().isEmpty() )
    {
      _pushButtonUp->setEnabled( false );
      _pushButtonDown->setEnabled( false );
    }
  else
    {
      _pushButtonUp->setEnabled( true );
      _pushButtonDown->setEnabled( true );

      // If the selection contains first row keep 'up' disabled,
      // and if selection contains last row keep 'down' disabled
      QList<QTableWidgetItem *> items = _tableWidgetSettings->selectedItems();
      for( QTableWidgetItem *item : items )
        {
          if( item->row() == 0 )
            _pushButtonUp->setEnabled( false );
          if( item->row() == _tableWidgetSettings->rowCount()-1 )
            _pushButtonDown->setEnabled( false );
        }
    }
}

// ----------------------------------------------------------------------------

void SettingsDialog::moveUp()
{
  // Determine the rows being moved up, in order
  std::vector<int> rows;
  QList<QTableWidgetItem *> items = _tableWidgetSettings->selectedItems();
  for( QTableWidgetItem *item : items )
    {
      bool found = false;
      for( size_t i=0; i<rows.size(); ++i )
        if( rows[i] == item->row() )
          found = true;
      if( !found )
        rows.push_back( item->row() );
    }
  std::sort( rows.begin(), rows.end() );

  // Move them up from the top down
  int cols = _tableWidgetSettings->columnCount();
  for( int row : rows )
    {
      for( int col=0; col<cols-1; ++col )
        {
          QTableWidgetItem *item1 =
            new QTableWidgetItem( *_tableWidgetSettings->item( row-1, col ) );
          QTableWidgetItem *item2 =
            new QTableWidgetItem( *_tableWidgetSettings->item( row, col ) );
          _tableWidgetSettings->setItem( row-1, col, item2 );
          _tableWidgetSettings->setItem( row, col, item1 );
        }
    }

  // Move the selection along with the moving rows
  _tableWidgetSettings->clearSelection();
  _tableWidgetSettings->setSelectionMode( QAbstractItemView::MultiSelection );
  for( int row : rows )
    _tableWidgetSettings->selectRow( row-1 );
  _tableWidgetSettings->setSelectionMode( QAbstractItemView::ExtendedSelection );
}

// ----------------------------------------------------------------------------

void SettingsDialog::moveDown()
{
  // Determine the rows being moved down, in order
  std::vector<int> rows;
  QList<QTableWidgetItem *> items = _tableWidgetSettings->selectedItems();
  for( QTableWidgetItem *item : items )
    {
      bool found = false;
      for( size_t i=0; i<rows.size(); ++i )
        if( rows[i] == item->row() )
          found = true;
      if( !found )
        rows.push_back( item->row() );
    }
  std::sort( rows.begin(), rows.end() );
  std::reverse( rows.begin(), rows.end() );

  // Move them down from the bottom up
  int cols = _tableWidgetSettings->columnCount();
  for( int row : rows )
    {
      for( int col=0; col<cols-1; ++col )
        {
          QTableWidgetItem *item1 =
            new QTableWidgetItem( *_tableWidgetSettings->item( row+1, col ) );
          QTableWidgetItem *item2 =
            new QTableWidgetItem( *_tableWidgetSettings->item( row, col ) );
          _tableWidgetSettings->setItem( row+1, col, item2 );
          _tableWidgetSettings->setItem( row, col, item1 );
        }
    }

  // Move the selection along with the moving rows
  _tableWidgetSettings->clearSelection();
  _tableWidgetSettings->setSelectionMode( QAbstractItemView::MultiSelection );
  for( int row : rows )
    _tableWidgetSettings->selectRow( row+1 );
  _tableWidgetSettings->setSelectionMode( QAbstractItemView::ExtendedSelection );
}

// ----------------------------------------------------------------------------

void SettingsDialog::resizeIt()
{
  this->resizeEvent( 0 );
}

// ----------------------------------------------------------------------------

void SettingsDialog::accept()
{
  // Copy the settings into the 'settings' array
  _settings->clear();
  regsetting_t rs;
  for( int row=0; row<_tableWidgetSettings->rowCount(); ++row )
    {
      // Name
      rs.name = _tableWidgetSettings->item( row, 0 )->text().toStdString();

      // Value
      QString qs = _tableWidgetSettings->item( row, 1 )->text();
      bool ok;
      uint64_t val;
      if( qs[0] == '0' && (qs[1] == 'x' || qs[1] == 'X') )
        val = qs.toULong( &ok, 16 );
      else
        val = qs.toULong( &ok, 10 );
      rs.value = val;

      _settings->push_back( rs );
    }

  QDialog::accept();
}

// ----------------------------------------------------------------------------

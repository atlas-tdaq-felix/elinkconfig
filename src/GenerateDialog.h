#ifndef GENERATEDIALOG_H
#define GENERATEDIALOG_H

#include "ui_GenerateDialog.h"
#include <QCloseEvent>
#include <QDialog>
#include <QString>

#if REGMAP_VERSION < 0x500
#include "GbtConfig.h"
#else
#include "GbtConfig5.h"
#endif // REGMAP_VERSION
class FlxCard;

class GenerateDialog: public QDialog, Ui_GenerateDialog
{
  Q_OBJECT

 public:
  GenerateDialog( QWidget   *parent,
                  int        card_nr,
                  int        link_nr,
                  int        nchans,
                  GbtConfig *link_config,
                  std::vector<regsetting_t> *settings );
  ~GenerateDialog();

 public slots:
   void showDeveloperOptions   ( int checked );
   void changeAllLinksChecked  ( int checked );
   void changeLink             ( int index );
   void changeFlx              ( int index );
   void outputModeChanged      ( );
   void generateLinkConfig     ( );
   void generateEmulatorData   ( );
   void hideDoneLabel          ( );

 private:
   void generateGbtEmulatorData( );
   void generateFmEmulatorData ( );
   void setSpinBoxMaxChunkSize ( );
   void writeConfigToFile      ( uint32_t  lnk, GbtConfig *cfg );
   void writeConfigToFlx       ( uint32_t  lnk, GbtConfig *cfg );
   void writeEmuDataToFile     ( uint32_t *emudata,
                                 uint32_t  emusize,
                                 uint32_t  egroup,
                                 QString   ext );
   void writeEmuDataToFlx      ( uint32_t *emudata,
                                 uint32_t  emusize,
                                 uint32_t  egroup );
   void writeFmEmuDataToFile   ( uint64_t *emudata,
                                 uint64_t  emusize,
                                 QString   ext );
   void writeFmEmuDataToFlx    ( uint64_t *emudata,
                                 uint64_t  emusize );
   QString flxVersionString    ( );
   void readSettings           ( );
   void writeSettings          ( );
   void closeEvent             ( QCloseEvent *event );

 private:
   bool       _allLinks;
   int        _cardNr;
   int        _linkNr;
   uint64_t   _emuEnable;
   int        _dmaIndexInvalidCount;
   GbtConfig *_linkConfig;
   FlxCard   *_felixCard;
   std::vector<regsetting_t> *_regSettings;
};

#endif // GENERATEDIALOG_H

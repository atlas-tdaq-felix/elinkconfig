// Dummy FelixCard class definition
// for tests under Windows without FELIX hardware...

#ifdef WIN32
 #include <stdint.h>
#else
 #include </usr/include/stdint.h>
#endif // WIN32

#include "flxdefs.h"

// From registers.h
#define REG_MAP_VERSION_OFFSET                 0x00000
#define BOARD_ID_TIMESTAMP_OFFSET              0x00010
#define BOARD_ID_SVN_OFFSET                    0x00020
#define STATUS_LEDS_OFFSET                     0x00030
#define GENERIC_CONSTANTS_OFFSET               0x00040
#define NUM_OF_CHANNELS_OFFSET                 0x00050
#define CARD_TYPE_OFFSET                       0x00060
#define GBT_MAPPING_OFFSET                     0x00070
#define WIDE_MODE_OFFSET                       0x000D0
#define CR_TH_UPDATE_CTRL_OFFSET               0x01000
#define CR_FH_UPDATE_CTRL_OFFSET               0x01010
#define CR_TH_GBT00_EGROUP0_CTRL_OFFSET        0x01100
#define GBT_EMU_ENA_OFFSET                     0x04000
#define GBT_EMU_CONFIG_WE_ARRAY_OFFSET         0x04010
#define GBT_EMU_CONFIG_OFFSET                  0x04020
#define GBT_DNLNK_FO_SEL_OFFSET                0x05600
#define GBT_UPLNK_FO_SEL_OFFSET                0x05610

// Definitions for FelixCard registers and register contents
#define GBT_EGROUP_CTRL_I                      CR_TH_GBT00_EGROUP0_CTRL_OFFSET
#define GBT_EGROUP_CTRL_SIZE                   (0x10*(7+5))
#define GBT_EGROUP_CTRL_TOGBT_OFFS             (0x10*7)
#define EGROUP_CTRL_MODE_SHIFT                 15
#define EGROUP_CTRL_MAXCHUNK_SHIFT             31
#define GBT_CTRL_FROMGBT_UPDATE_I              CR_TH_UPDATE_CTRL_OFFSET
#define GBT_CTRL_TOGBT_UPDATE_I                CR_FH_UPDATE_CTRL_OFFSET
#define GBT_EMU_ENA_I                          GBT_EMU_ENA_OFFSET
#define GBT_EMU_CONFIG_I                       GBT_EMU_CONFIG_OFFSET
#define EMU_CONFIG_ADDR_SHIFT                  32
#define GBT_EMU_CONFIG_EGROUP_I                GBT_EMU_CONFIG_WE_ARRAY_OFFSET

class FelixCard
{
public:
  FelixCard()                  { }
  ~FelixCard()                 { }
  void setSlotNumber( int n )  { _slotNr = n; }
  int  open( int n )           { _slotNr = n; return 0; }
  int  close()                 { return 0; }
  int  getNumberOfCards()      { return 0; }
  static int numberOfCards()   { return 3; }
  void softReset()             { }
  uint64_t readBar2Register64( uint64_t offset )
  {
    if( offset == REG_MAP_VERSION_OFFSET    ) return 0x300; 
    if( offset == CARD_TYPE_OFFSET          ) return 709 + _slotNr; 
    if( offset == BOARD_ID_TIMESTAMP_OFFSET ) return 0x1531080000+_slotNr;
    if( offset == WIDE_MODE_OFFSET )          return 0;

    if( offset == GBT_EGROUP_CTRL_I+0x00    ) return 0x00000124aaaa8006;
    if( offset == GBT_EGROUP_CTRL_I+0x10    ) return 0x00000124aaaa8078;
    if( offset == GBT_EGROUP_CTRL_I+0x20    ) return 0x00000124aaaa8001;
    if( offset == GBT_EGROUP_CTRL_I+0x30    ) return 0x00000124aaaaff80;
    if( offset == GBT_EGROUP_CTRL_I+0x40    ) return 0x00000124aaaae628;
    if( offset == GBT_EGROUP_CTRL_I+0x50    ) return 0x00000124aaaa99d0;
    if( offset == GBT_EGROUP_CTRL_I+0x60    ) return 0x00000000555519d0;

    if( offset == GBT_EGROUP_CTRL_I+0x70    ) return 0x0000088888888001;
    if( offset == GBT_EGROUP_CTRL_I+0x80    ) return 0x000008888888ff80;
    if( offset == GBT_EGROUP_CTRL_I+0x90    ) return 0x0000111111116628;
    if( offset == GBT_EGROUP_CTRL_I+0xA0    ) return 0x00001999999999d0;
    if( offset == GBT_EGROUP_CTRL_I+0xB0    ) return 0x0000222222220006;

    if( offset >= GBT_EGROUP_CTRL_I &&
	offset<GBT_EGROUP_CTRL_I + FLX_GBT_LINKS*GBT_EGROUP_CTRL_SIZE )
      return 0;
    /*return (_slotNr<<16) + offset;*/
    return offset/0x10 + _slotNr;
  }
  uint64_t bar2Reg64( uint64_t offset )
  { /*return (_slotNr<<16) + offset;*/ return offset/0x10; }
  void     writeBar2Register64( uint64_t offset, uint64_t val )
  { offset = 0; val = 0; }
  void     setBar2Reg64( uint64_t offset, uint64_t val )
  { offset = 0; val = 0; }
 private:
  int _slotNr;
};

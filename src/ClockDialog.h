#ifndef CLOCKDIALOG_H
#define CLOCKDIALOG_H

#include "ui_ClockDialog.h"
#include <QDialog>

class ClockDialog: public QDialog, Ui_ClockDialog
{
  Q_OBJECT

 public:
  ClockDialog( QWidget *parent,
               bool    *local_clk,
               bool     pll_lock );
  ~ClockDialog();

 public slots:
   void accept();

 private:
   bool *_localClk;
};

#endif // CLOCKDIALOG_H

#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QPalette>

#include "FanOutSelectDialog.h"

// ----------------------------------------------------------------------------

FanOutSelectDialog::FanOutSelectDialog( QWidget  *parent,
                                        uint64_t  range,
                                        uint64_t *select_mask,
                                        bool     *locked,
                                        QString   title )
  : QDialog( parent ),
    _range( range ),
    _selectMask( select_mask ),
    _locked( locked )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );
  this->setWindowTitle( title );

  QLabel *lbl = new QLabel( "GBT:" );
  lbl->setMaximumWidth( 50 );
  this->horizontalLayout->addWidget( lbl );

  QPushButton *pb;
  for( uint32_t i=0; i<range; ++i )
    {
      pb = new QPushButton( QString::number(i), this );
      pb->setCheckable( true );
      pb->setMaximumWidth( 40 );

      if( (*select_mask) & ((uint64_t) 1<<i) )
        pb->setChecked( true );
      else
        pb->setChecked( false );

      //QPalette qp = QPalette( QColor(Qt::green) );//pb->palette();
      //qp.setColor( QPalette::Button, QColor(Qt::green) );
      pb->setAutoFillBackground( true );
      //pb->setPalette( qp );
      //pb->update();

      connect( pb, SIGNAL(clicked()), this, SLOT(updateButtons()) );

      _buttons.push_back( pb );
      this->horizontalLayout->addWidget( pb );
    }

  this->horizontalLayout->insertSpacing( -1, 30 );

  _cb = new QCheckBox( QString("Locked"), this );
  _cb->setToolTip( "Lock against changes by tools such as fdaq and fupload" );
  _cb->setChecked( *locked );
  this->horizontalLayout->addWidget( _cb );

  // Connect the buttons
  connect( _pushButtonAll, SIGNAL( clicked() ), this, SLOT( selectAll() ) );
  connect( _pushButtonNone, SIGNAL( clicked() ), this, SLOT( deselectAll() ) );
  connect( _pushButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( _pushButtonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );

  this->updateButtons();
}

// ----------------------------------------------------------------------------

FanOutSelectDialog::~FanOutSelectDialog()
{
}

// ----------------------------------------------------------------------------

void FanOutSelectDialog::updateButtons()
{
  QString txt;
  for( uint32_t i=0; i<_buttons.size(); ++i )
    {
      txt = _buttons[i]->text();
      if( _buttons[i]->isChecked() )
        {
          // If not present yet, add letter 'E' to the displayed GBT number
          if( txt[txt.size()-1] != QChar('E') )
            txt.append( 'E' );
        }
      else
        {
          // Remove letter 'E' if present
          if( txt[txt.size()-1] == QChar('E') )
            txt.truncate( txt.size()-1 );
        }
      _buttons[i]->setText( txt );
    }
}

// ----------------------------------------------------------------------------

void FanOutSelectDialog::selectAll()
{
  for( uint32_t i=0; i<_buttons.size(); ++i )
    _buttons[i]->setChecked( true );
  this->updateButtons();
}

// ----------------------------------------------------------------------------

void FanOutSelectDialog::deselectAll()
{
  for( uint32_t i=0; i<_buttons.size(); ++i )
    _buttons[i]->setChecked( false );
  this->updateButtons();
}

// ----------------------------------------------------------------------------

void FanOutSelectDialog::accept()
{
  // Establish which buttons were selected and return them in the mask
  *_selectMask = 0x0000;
  for( uint32_t i=0; i<_buttons.size(); ++i )
    {
      if( _buttons[i]->isChecked() )
        *_selectMask |= ((uint64_t) 1 << i);
    }
  *_locked = _cb->isChecked();

  QDialog::accept();
}

// ----------------------------------------------------------------------------

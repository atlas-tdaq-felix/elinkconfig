// Dummy FlxCard class definition
// for tests under Windows without FELIX hardware...

#ifdef WIN32
 #include <stdint.h>
 typedef uint64_t u_long;
 typedef uint32_t u_int;
 typedef uint8_t  u_char;
#else
 #include </usr/include/stdint.h>
#endif // WIN32

#include <iostream>
#include "flxdefs.h"
#include "regmap/regmap.h"

// From registers.h (in fel project)
#define EGROUP_CTRL_MODE_SHIFT      15
#define EGROUP_CTRL_MAXCHUNK_SHIFT  31
#define EMU_CONFIG_ADDR_SHIFT       32

// ----------------------------------------------------------------------------

class FlxCard
{
public:
  FlxCard()                          {
    int gbt;
    for( gbt=0; gbt<24; ++gbt ) {
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[0].TH = 0x00000124aaaa8006;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[1].TH = 0x00000124aaaa8078;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[2].TH = 0x00000124aaaa8001;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[3].TH = 0x00000124aaaaff80;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[4].TH = 0x00000124aaaae628;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[5].TH = 0x00000124aaaa99d0;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_TH[6].TH = 0x00000000555519d0;

      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_FH[0].FH = 0x0000088888888001;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_FH[1].FH = 0x000008888888ff80;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_FH[2].FH = 0x0000111111116628;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_FH[3].FH = 0x00001999999999d0;
      _bar2Regs.CR_GBT_CTRL[gbt].EGROUP_FH[4].FH = 0x0000222222220006;
    }
  }
  ~FlxCard()                         { }
  void       card_open( int n )      { _slotNr = n; _bar2Regs.NUM_OF_CHANNELS = 5+n; }
  void       card_close()            { }
  static int number_of_cards()       { return 3; }
  uint64_t   openBackDoor( int )     { return (uint64_t) &_bar2Regs; }
  uint64_t   cfg_get_reg( const char *offset )
  {
    if     ( strcmp(offset,REG_REG_MAP_VERSION)    == 0 ) return 0x300; 
    else if( strcmp(offset,REG_CARD_TYPE)          == 0 ) return 709 + _slotNr; 
    else if( strcmp(offset,REG_BOARD_ID_TIMESTAMP) == 0 ) return 0x1531080000+_slotNr;
    else if( strcmp(offset,REG_WIDE_MODE)          == 0 ) return 0;
    else
      /*return (_slotNr<<16) + offset;*/
      return _slotNr;
  }
  void cfg_set_reg( const char *, uint64_t val ) { val = 0; }
  void soft_reset() { }
 private:
  int _slotNr;
  flxcard_bar2_regs_t _bar2Regs;
};

// ----------------------------------------------------------------------------

class FlxException
{
 public:
  FlxException( uint32_t error_id, std::string error_text )
    : _errId( error_id ), _errMsg( error_text ) {}

  virtual ~FlxException() throw() {} 

  std::string what() { return _errMsg; }

 private:
  uint32_t    _errId;
  std::string _errMsg;
};

// ----------------------------------------------------------------------------

#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFont>
#include <QLabel>
#include <QMessageBox>
#include <QPalette>
#include <QSettings>
#include <QSignalMapper>
#include <QString>
#include <QTextStream>
#include <QTimer>

#include <unistd.h>

#include "felixtag.h"

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

#include "ElinkConfig.h"
#include "ReplicateDialog.h"
#include "FanOutSelectDialog.h"
#include "TimeoutDialog.h"
#include "ClockDialog.h"
#include "GenerateDialog.h"
#include "SettingsDialog.h"

#include <sstream>
#include <iostream>
#include <iomanip>
using namespace std;
#include "nlohmann/json.hpp"
using namespace nlohmann;
// NOTE: Ignore the deprecated-declarations warning of gcc13, see FLX-2310
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "yaml-cpp/yaml.h"
#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
// Versions

QString VERSION_STR( "v4.9.0  17-FEB-2025" );
// ElinkConfig.cpp/h:
// - Support for custom display of ITK STRIP firmware FromHost E-groups
//   (i.e mode text label and E-link background color).
// - Fix STRIP EC-link DMA assignment option.
//
//QString VERSION_STR( "v4.8.3  23-OCT-2024" );
// GenerateDialog.cpp:
// - Configure LPGBT_DATARATE and LPGBT_FEC registers matching lpGBT flavour.
// 
//QString VERSION_STR( "v4.8.2  02-JUL-2024" );
// GbtConfig5.h:
// - Added bool _ltiTtc.
// ElinkConfig.cpp (plus GUI):
// - Added checkbox to select FromHost LTI mode instead of GBT, per link
//   (for FULLmode firmware); NB: decided to mirror configuration on device #1,
//   as the register is available/effective on Endpoint0 only.

//QString VERSION_STR( "v4.8.1  01-JUL-2024" );
// EmuDataGenerator.cpp:
// - Take into account that minimum E-link width is 8- or 4-bit
//   for lpGBT 10G and 5G resp. when determining the max number of
//   chunks that can be stored in the emulator RAM, i.e. space for more chunks.

//QString VERSION_STR( "v4.8.0  22-MAY-2024" );
// ElinkConfig.cpp (plus GUI):
// - Add combobox to select lpGBT mode flavour (5/10G and FEC5/12).

//QString VERSION_STR( "v4.7.1  30-JUN-2023" );
// ElinkConfig.cpp, GenerateDialog.cpp (plus GUI):
// - Add RM5 DMA index selection for TTCtoHost e-link,
//   in configuration file stored as part of link 0 settings.
// - Some member variable name changes.

//QString VERSION_STR( "v4.7.0  17-MAY-2023" );
// ElinkConfig.cpp:
// - Show only standard E-link modes (not "Strip", etc), others as value.
// - Tooltips for E-link number and mode.
//QString VERSION_STR( "v4.7.0  03-APR-2023" );
// - Single E-links mode option 'TTC', now for RM5 GBT too.
//QString VERSION_STR( "v4.7.0  09-SEP-2022" );
// SettingsDialog.cpp: fix crash when calling addSetting().
//QString VERSION_STR( "v4.7.0  14-AUG-2022" );
// ElinkDialog.cpp, GenerateDialog.cpp, GbtConfig5.h:
// - Add RM5 TTC option and combobox: 4-bit setting per FromHost E-group
//   for E-links configured in 'TTC' mode.
// - Prepare for additional lpGBT modes (here: 5Gb-FEC5).
// - Fix incorrect FromHost E-path modes list in dropdown (now equal to ToHost).

//QString VERSION_STR( "v4.6.2  07-APR-2022" );
// ElinkDialog.cpp:
// - Add 'TTC-8' mode option to 2-bit FromHost E-links.
// - For STRIP firmware show EC mode fixed as "Endeavour" (RM5).
// - Fix DMA index configuration for FULL mode (was missing; for RM5).

//QString VERSION_STR( "v4.6.1  6-FEB-2022" );
// ElinkConfig.cpp:
// - RM4 default configuration file type now .jelc instead of .elc.
// - Optional extra FELIX register settings added to RM4 .jelc files
//   (appended to Link 0 settings); use 'Extra' button to add settings.
// - Added register settings and DMA indices also to (obsolete) RM5 .jelc file.

//QString VERSION_STR( "v4.6.0  20-JAN-2022" );
// RM5 version update
// ElinkConfig.cpp:
// - ToHost E-links display either mode or DMA index in their dropdown menu,
//   selectable by means of two new radiobuttons in the ToHost section.
// - DMA index options added for ToHost EC as well as IC.
// - TTCtoHost E-link number for RM5: link 24, egroup/epath 0 -> 0x600.
// - New RM5 .yelc format ("Format: 2"), with separate ToHost and FromHost
//   E-groups, which include a setting for the DMA index per ToHost E-link.
// GeneratorDialog.cpp
// - Support for DMA controller indices.
// GbtConfig5.h:
// - Support for DMA controller index per E-link, represented per E-group
//   for 8 possible E-paths by a 32-bit integer, i.e. a nibble per index.

//QString VERSION_STR( "v4.5.1  3-DEC-2021" );
// ElinkConfig.cpp:
// - Modifications and fixes in GUI logic.

//QString VERSION_STR( "v4.5.1  22-NOV-2021" );
// ElinkConfig.cpp:
// - Fix: lpGBT has four 8-bit wide FromHost E-groups.
// - Fix: string with number of bits per E-path for RM5, in epathBitsString().
// GenerateDialog.cpp/h:
// - Remove any reference to 'flip' (not used).

//QString VERSION_STR( "v4.5.1  11-NOV-2021" );
// ElinkConfig.cpp:
// - Fix: make sure to leave the TTC-options in FromHost direction available
//   after reading the firmware configuration from the device
//   (i.e. don't disable FromHost 2/4/8-bit widths, just certain E-link modes).
// - Take TOHOST_32 (32-bit E-links for lpGBT) capability bit into account.
// - LpGBT links can have 8-, 16- or 32-bit E-links.
// - Propagate firmware settings (i.e. width limitations) to the (E-)link
//   configuration copy in the software (ToHost direction).
// GenerateDialog.cpp:
// - Write .mem (RM5) or .coe (RM4) files.
// EmuDataGenerator.cpp/h, GbtConfig5.h:
// - Take 32-bit E-link into account
// - Special 64-bit (was 32-bit) parameter words with bit width and bit index
//   per E-path, for RM5 each is a byte (in RM4 a nibble)

//QString VERSION_STR( "v4.5.0  20-JUL-2021" );
// ElinkConfig.cpp:
// - Disable/(re)enable items in the E-link configuration GUI according to
//   the firmware configuration/capabilities as read from the card's registers
//   (when clicking on the 'Read Cfg' button).
// - E-group buttons: hide/show rather than disable/enable.
// - Clear file name input box when clicking 'Open', 'Save' or 'Read Cfg'.
// - Bug fix: in changeToHostEwidth() for lpGBT.
// - EC/IC(/AUX) E-link numbers are formed from the link number plus an index,
//   which for RM5 are read from a register.
// GenerateDialog.cpp:
// - Bug fix: show 'LSB first' checkbox for FULL mode too (was hidden).

//QString VERSION_STR( "v4.4.0  13-APR-2021" );
// ElinkConfig.cpp, GenerateDialog.cpp:
// - Support for additional FELIX register settings in .yelc configuration files
//   ("RegisterSettings" entries), for RM5.
// SettingsDialog.cpp/h/ui:
// - Dialog to add and/or remove additional register settings.

//QString VERSION_STR( "v4.3.0  23-MAR-2021" );
// ElinkConfig.cpp:
// - Modifications in support of lpGBT (under development).

//QString VERSION_STR( "v4.3.0  22-FEB-2021" );
// ElinkConfig.cpp, GenerateDialog.cpp:
// - Use GbtConfig5.h instead of GbtConfig.h for RM5.
// - Configuration files in YAML format for RM5.
// - Preparations for including lpGBT mode.
// GbtConfig5.h: added.

//QString VERSION_STR( "v4.2.1  3-FEB-2021" );
// ElinkConfig.cpp:
// - Added FromHost 8-bit E-link mode option 'TTC-7'.

//QString VERSION_STR( "v4.2.0  17-DEC-2020" );
// ElinkConfig.cpp:
// - Add 'Set TTC Clock' tickbox.
// - Save this setting in .elc and .jelc configuration files
//   (in .elc as added offset to link mode value, in .jelc as extra entry).
// GbtConfig.h:
// - Add _ttcClock member, plus set and get functions.

//QString VERSION_STR( "v4.1.2  15-OCT-2020" );
// ElinkConfig.cpp:
// - 'Read Cfg' pushbutton text changes to bold when a different FLX-device
//   is selected, and back to normal when its configuration has been read.
// TimeoutDialog.cpp:
// - Fix error in tooltip.

//QString VERSION_STR( "v4.1.1  15-JUL-2020" );
// EmuDataGenerator.cpp:
// - Custom 16-bit E-link emulator data matching the ATLAS NSW 640Mb/s E-links,
//   made up of two 320Mb/s E-links with data bits alternating between the two.

//QString VERSION_STR( "v4.1.0  25-JUN-2020" );
// - Fix bug in enable/disable TTCtoHost channel (in changeTtc2HostEnable()).
// - Fix bug in showing FromHost groups dependent on link mode.
// - 'Max Chunk' -> 'Truncation'.
// - Show checkbox for 2-bit HDLC truncation (size>=15 bytes) for GBT firmware.
//   (checked = truncation at 3584 bytes in 2-bit spinbox).

//QString VERSION_STR( "v4.0.1  27-APR-2020" );
// - GUI tweaks: mainly tooltips.
// - Hide Egroup info numbers unless 'Advanced' ticked.
// - Fixes for firmware mode (valid: 0 to 8)
// - Add firmware mode string to device dropdown.

//QString VERSION_STR( "v4.0.0  24-MAR-2020" );
// ElinkConfig.cpp/h/ui: Major update
// - Display E-groups of a link in columns.
// - Mixed width in an E-group not permitted.
// - E-group/E-path widgets generated programmatically
//   (see populateToHostPanel() and populateFromHostPanel()).
// - General renaming of FromGbt to ToHost and ToGbt to FromHost.

//QString VERSION_STR( "v3.7.2  26-NOV-2019" );
// GenerateDialog/EmuDataGenerator:
// - Add StreamID checkbox: first byte of emulator chunks becomes a StreamID;
//   counts up from 0.

//QString VERSION_STR( "v3.7.1  01-NOV-2019" );
// GenerateDialog:
// - Replace message-box when FM emu upload done by flashing 'DONE' label.
// - Increase max number of IDLES that can be selected, from 512 to 2048.

//QString VERSION_STR( "v3.7.0  30-OCT-2019" );
// GenerateDialog.cpp/h:
// - Get rid of 'bool rom' parameter in write(Fm)EmuDataToFile().
//QString VERSION_STR( "v3.7.0  01-OCT-2019" );
// ElinkDialog.cpp/ui:
// - Support for 'StreamID' bits in .JELC configuration files.
// - Include StreamID bits in reading and writing configuration.
// - Re-enable support for 'SCA-AUX' E-link for LTDB firmware.
// GbtConfig.h:
// - Support for 'StreamID' bits.

//QString VERSION_STR( "v3.6.0  08-AUG-2019" );
// ElinkDialog.cpp:
// - Support for writing and reading .JELC configuration files in JSON format.

//QString VERSION_STR( "v3.5.0  13-JUN-2019" );
// ElinkDialog.cpp:
// - Add 'TTC-5' mode option to 4-bit FromHost E-links and 'TTC-6' mode option
//   to 2-bit FromHost E-links (JIRA FLX-965).
// - Fix EC mode combobox enable/disable.

//QString VERSION_STR( "v3.4.1  12-APR-2019" );
// GeneratorDialog.cpp:
// - Issue a warning (pop-up) when nothing is enabled in GBT EMU link.
// EmuDataGenerator.cpp
// - Fix potential crash (buffer overflow) when inserting SoB/EoB K-chars.

//QString VERSION_STR( "v3.4.0  14-JAN-2019" );
// ElinkDialog.cpp, GeneratorDialog:
// - Added support for the LTDB firmware's SCA-AUX e-link
//   (to make the AUX tickboxes appear, click 'Read Cfg' on an LTDB FLX-card).

//QString VERSION_STR( "v3.3.2  5-NOV-2018" );
// GeneratorDialog, EmuDataGenerator:
// - Emulator RAM e-link data bit order depends on 'lsb_first' setting.

//QString VERSION_STR( "v3.3.1  13-SEP-2018" );
// - Number-of-channels register no longer div by 2 for FLX-712/711.
// GeneratorDialog, EmuDataGenerator:
// - Added 'CRC error' developer option.

//QString VERSION_STR( "v3.3.0  10-AUG-2018" );
// ElinkDialog.cpp/ui:
// - Add IC-channel to- and from-host checkboxes.

//QString VERSION_STR( "v3.2.3  19-JUN-2018" );
// GeneratorDialog.cpp/ui:
// - Add 'LSB-first' (developer) option, to configure 8b10b-word bit order
//   (partly controlled by SVN version, but that's RM3 legacy).

//QString VERSION_STR( "v3.2.1  21-MAY-2018" );
// ElinkConfig.cpp, EmuDataGenerator.cpp:
// - Bugs fixed, including some GUI-related ones.
// EmuDataGenerator.cpp, GeneratorDialog.cpp:
// - Change CRC polynomial for FULLMODE emulator data.
// Rename FLX_GBT_LINKS to FLX_LINKS.
// Updates in Replicate dialogs text strings.

//QString VERSION_STR( "v3.2.0  18-APR-2018" );
// ElinkDialog.cpp, GeneratorDialog.cpp, ReplicateDialog.cpp:
// - Add additional link 'EMU' to hold the configuration for the Emulator data.

//QString VERSION_STR( "v3.1.1  13-APR-2018" );
// ElinkDialog.cpp:
// - Show FIRWMARE_MODE 2 and 3 as: 'GBT (LTDB)' and 'GBT (FEI4)'.

//QString VERSION_STR( "v3.1.0  28-FEB-2018" );
// GenerateDialog.cpp:
// - Allow 'add BUSY' optionn for FULLMODE.
// - Increase maximum chunksize for FULLMODE to 10000.

//QString VERSION_STR( "v3.0.1  22-FEB-2018" );
// ElinkConfig.cpp/GenerateDialog.cpp:
// - Ported to Register Map 4.
// - Some constant/variable renaming.
// - Fixes for FULLMODE.
// GenerateDialog.cpp/GbtConfig.h:
// - FULLMODE emulator RAM depth increased from 1K to 8K
//   depending on SVN version.
// - FULLMODE emulator chunksize set to multiple of 4 and spinbox step to 4.
// - Display emulator chunk rate per elink.
// - Display total number of enabled elinks (to and from).
// TimeoutDialog.cpp:
// - Set minimum timeout value (depends on firmware mode: GBT or FULL)

//QString VERSION_STR( "v2.7.0  22-NOV-2017" );
// ElinkConfig/Dialog:
// - Move EC and TTC2Host channels into vertical button layout,
//   which involves a lot of modifications to the GUI interaction.
// - Disabling an E-link no longer automatically enables the lower-width ones.
// GenerateDialog.cpp:
// - Initially select same FLX-card selected in ElinkConfig dialog.

//QString VERSION_STR( "v2.6.2  30-OCT-2017" );
// GeneratorDialog, EmuDataGenerator, Encoder8b10b:
// - Added 'FEI4B Kchars' option.

//QString VERSION_STR( "v2.6.1  13-SEP-2017" );
// ElinkConfig/Dialog:
// - Change 'Link' type names.
// - 'Advanced' tickbox, hide/show max chunk info.
// EmuDataGenerator.cpp:
// - SOB/EOB inserted between (1+epath+egroup*8)-th and next chunk,
//   only IDLEs inbetween.
// - Omit-SOC/EOC option now also applies to FULLMODE emulator.

//QString VERSION_STR( "v2.6.0  24-AUG-2017" );
// ElinkConfig.cpp:
// - Improve EC/TTC user interface; TTC-to-Host channel only present on GBT #0,
//   now for FULL mode too.
// - Swap 'Replicate..' and 'Repl 2 All' buttons.
// - Fix bug in ElinkConfig::readFlxConfig() for TTC-to-host channel.
// EmuDataGenerator.cpp:
// - Added function crc20() for adding CRC to FULL mode emulator data (in EOP).
// ClockDialog:
// - Added 'PLL lock' indicator.

//QString VERSION_STR( "v2.5.5  13-JUN-2017" );
// GenerateDialog.cpp:
// - Changes for register map 3.7, concerning "TTC_TOHOST" register.

//QString VERSION_STR( "v2.5.4  08-JUN-2017" );
// GenerateDialog::writeConfigToFlx():
// - Take TH timeout and TH/FH reverse into account when writing configuration.
// ElinkConfig.cpp:
// - Display FLX E-link number instead of bit index in the enable/disable boxes
// TimeoutDialog.cpp:
// - Added TTC-to-Host time-out configuration.

//QString VERSION_STR( "v2.5.3  11-MAY-2017" );
// GenerateDialog/EmuDataGenerator:
// - Add developer options to optionally omit a (single) Start-of-Chunk and/or
//   End-of-Chunk in the emulator data, per elink.
// - Minor bugfix setting maximum number of idles;
//   increased max from 128 to 256.

//QString VERSION_STR( "v2.5.2  01-MAY-2017" );
// TimeoutDialog.cpp
// - Added timeout counter maximum parameter (read from an FLX-card register)

//QString VERSION_STR( "v2.5.1  29-MAR-2017" );
// ElinkConfig.cpp:
// - Read a 'static' configuration from the corresponding registers
//   and display 'STATIC' (label) in the GUI to indicate this.
// GenerateDialog:
// - Emulator data 'bit-reversed' option derived from FLX-card SVN number.
// - 'Advanced' options -> 'Developer' options.

//QString VERSION_STR( "v2.5.0  17-MAR-2017" );
// Add 'Clock Configuration' dialog.

//QString VERSION_STR( "v2.4.2  17-MAR-2017" );
// For FLX-711 divide number of channels by 2,
// because it presents itself as 2 cards.
// GenerateDialog.cpp, EmuDataGenerator.cpp/h:
// - Full-mode fixes.

//QString VERSION_STR( "v2.4.1  14-MAR-2017" );
// GenerateDialog.cpp, EmuDataGenerator.cpp/h:
// - Add generateFmDune() for ProtoDUNE-like data frames,
//   taken from the data array in protodune.h (new file).
// - Fix bug in full-mode .coe file generation.

//QString VERSION_STR( "v2.4.0  14-MAR-2017" );
// GenerateDialog.cpp:
// - Add 'Advanced' checkbox, showing or hiding a number of options
//   ('to file' radiobutton, 'Bit-reversed' and 'Add BUSY' checkboxes.
// - Fix some bugs in handling inaccessible FLX cards.
// - Add proper 'bit-reversed' option: 8b10b 10-bit words copied into emu-RAM
//   LSB-to-MSB instead of MSB-to-LSB (will become the default setting later on)
// - Add function to generate the full-mode .coe file.

//QString VERSION_STR( "v2.3.3  14-FEB-2017" );
// GenerateDialog.cpp:
// - Fix bug in setting EC_FROMHOST.ENABLE to 0; link was always enabled.

//QString VERSION_STR( "v2.3.2  1-FEB-2017" );
// GenerateDialog.cpp:
// - Save set number of idle chars between chunks in settings.
// - For single GBT link selection, select corresponding link configuration,
//   for all-links selection stick to GBT link #0 configuration.
// - writeSettings():
//   Partial prevention of "setNativeLocks failed" Qt message (bug).
// TimeoutDialog.cpp:
// - Time-out counter set by spinbox (instead of line-edit).

//QString VERSION_STR( "v2.3.1  20-JAN-2017" );
// ElinkConfig.cpp: add 'version tag' to displayed version string.

//QString VERSION_STR( "v2.3.0  21-DEC-2016" );
// GenerateDialog.ui/cpp, EmuDataGenerator.cpp:
// - Add configurable number of IDLE characters between chunks.
// - Hide the 'SoC=K28.7' checkbox.
// - Add a 'Bit-reversed' checkbox.
// ElinkConfig.cpp, TimeoutDialog.cpp:
// - Limit time-out value to [64..1023].

//QString VERSION( "v2.2.0  2-DEC-2016" );
// - Added capabilities and behaviour for 'FULL MODE' links.

//QString VERSION( "v2.1.0  31-OCT-2016" );
// - Add TimeoutDialog.cpp/h.
// - ELinkConfig.cpp:
//   - Added Timeout configuration button and associated functions.

//QString VERSION( "v2.0.3  19-OCT-2016" );
// - ELinkConfig.cpp:
//   - Add read/writeSettings().
//   - Add hostname to window title.
//   - Add warning about emulator data being based on GBT link 0 only
//     in generate/upload dialog.

//QString VERSION( "v2.0.0  9-SEP-2016" );
// - Add additional group in to-host (for EC link and virtual TTC-to-Host link)
//   and from-host (for EC link) direction, and map their configuration
//   registers to and from the GbtConfig class enable and encoding formats;
//   EC link located at 2-bit e-link epath #7 and
//   TTC-to-Host link located at 2-bit elink epath #3,
//   to match their elink numbers.
// - Fix missing entry for Qt resources compiler in CMakeLists.txt.

//QString VERSION( "v1.7.0  12-AUG-2016" );
// - Limit FLX_GBT_LINKS to 16 (at least temporary)
// - Use FLX_GBT_LINKS instead of hardcoded value where appropriate3

//QString VERSION( "v1.6.0  27-Jul-2016" );
// - ElinkConfig.cpp, GenerateDialog.cpp:
//   - Move to FlxCard API (instead of FelixCard).
// - ElinkConfig.cpp:
//   - In To-GBT/From-Host direction only 2-bit elinks can be HDLC encoded
//     -> disabled HDLC in combobox for other elinks.

//QString VERSION( "v1.5.0  30-Jun-2016" );
// - Added FanOutSelectDialog.cpp/h.
// - ElinkConfig.cpp:
//   - Add GBT To-Host and From-Host emulator (fan-out) select buttons,
//     and associated functions to/fromHostFanOutSelect(), popping up
//     a FanOutSelectDialog.
//   - Use bit 31 of the Fanout Select register(s) as a 'lock' for tools
//     like fel or fdaq.

//QString VERSION( "v1.4.1  03-Jun-2016" );
// - GenerateDialog.cpp/ui, EmuDataGenerator.cpp, Encode8b10.cpp:
//   - Parameter to select between K28.7 and K28.1 for Start-of-Chunk K-char.

//QString VERSION( "v1.4.0  27-May-2016" );
// - GenerateDialog.cpp:
//   - Fix bug in writing To-GBT mode bits.
// - ElinkConfig.cpp:
//   - Add 'disable' buttons to disable a complete E-group.
//   - Show GBT frame bit index in checkboxes (instead of EPROC index).

//QString VERSION( "v1.3.4  06-Jan-2016" );
// Moved to new memory map and generated register name definitions.
// A few bug fixes in code and GUI.

//QString VERSION( "v1.3.3  10-Sep-2015" );
// - GenerateDialog.cpp:
//   - Chunk size spinbox max dependent on maximum chunk size settings
//     for the selected GBT link number.
//   - After configuring a FELIX card generate a soft reset on the board.
// - Bugfix in EmuDataGenerator.cpp.
// - Subdialogs: make them properly modal (missing Qt::Dialog in window flags).

//QString VERSION( "v1.3.2  31-Aug-2015" );
// - Radiobuttons for GBT wide/normal mode.
// - Display board type number (e.g. '709') in FELIX-card combobox.

//QString VERSION( "v1.3.1  25-Aug-2015" );
// - Add option to set different data patterns in emulator data
// - Initialise emulator data chunk counter to 0, not 1.

//QString VERSION( "v1.3.0  24-Aug-2015" );
// - Add option in GUI to read/download E-link configuration from one of the
//   FELIX cards present in the system, and start editing the configuration
//   from there; added function ElinkConfig::readFelixConfig() for that.
// - Create DummyFelixCard.h containing the dummy FelixCard class for testing
//   under Windows.
// - Minor simplification plus bugfix in ElinkConfig::changeEgroupEnables().

//QString VERSION( "v1.2.0  17-Aug-2015" );
// In GenerateDialog.cpp:
// Add upload of configuration and emulator data to FELIX cards,
// as well as writing of files compatible with central-router VHDL.

//QString VERSION( "v1.1.0  02-Jul-2015" );
// Ongoing development..

//QString VERSION( "v1.0.0  12-Mar-2015" );
// First version

// For ITk STRIP firmware: FromHost E-link names/modes
const std::vector<QString> STRIP_ELINK_STR =
  { "L-cfg", "L-cmd", "L-trkl", "R-cfg", "R-cmd" };
const std::vector<QString> STRIP_ELINK_STR_SWAPPED =
  { "L*cfg", "L*cmd", "L*trkl", "R*cfg", "R*cmd" };
const QColor STRIP_ELINK_COLOR = QColor( "#F4A460" ); // 'sandybrown'

// ----------------------------------------------------------------------------

ElinkConfig::ElinkConfig()
  : QDialog(),
    _gbtNr( 0 ),
    _egroupNrToHost( 0 ),
    _egroupNrFromHost( 0 ),
    _auxVisible( false ),
    _itkStripLcbR3l1ElinkSwap( 0 ),
    _cfgDir( "." ),
    _stripsImage( 0 )
{
  // Disable EMU link FromHost e-links (ToHost used for configuration)
  for( int egroup=0; egroup<FLX_FROMHOST_GROUPS; ++egroup )
    _gbtConfig[FLX_LINKS].setEnablesFromHost( egroup, 0 );

  // Initialize firmware configuration as 'unread' (i.e. all features enabled)
  this->initFwParameters();

  this->setupUi(this);
  this->setWindowFlags( Qt::WindowMinimizeButtonHint |
                        Qt::WindowMaximizeButtonHint |
                        Qt::WindowCloseButtonHint );
  QString qs( VERSION_STR );
  qs += " (tag: " + QString(FELIX_TAG) + ")";
  _labelVersion->setText( qs );
  char name[64];
  if( gethostname( name, sizeof(name) ) == 0 )
    {
      QString qs = this->windowTitle();
      qs += QString( " @ " ) + QString( name );
      this->setWindowTitle( qs );
    }

  this->populateToHostPanel();
  this->populateFromHostPanel();

  // lpGBT mode combobox
  _comboLpGbtMode->addItem( "10G-FEC5" );
  _comboLpGbtMode->addItem( "5G-FEC5" );
  _comboLpGbtMode->addItem( "10G-FEC12" );
  _comboLpGbtMode->addItem( "5G-FEC12" );
  connect( _comboLpGbtMode, SIGNAL(currentIndexChanged(int)),
           this, SLOT( changeLinkMode() ) );

  // LTI-TTC checkbox
  connect( _checkBoxLtiTtc, SIGNAL(stateChanged(int)),
           this, SLOT(changeFromHostLtiTtc()) );

  // EC/IC/AUX/TTCtoHost checkboxes
  connect( _checkBoxEcToHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeEcToHostEnable()) );
  connect( _checkBoxEcFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeEcFromHostEnable()) );
  connect( _checkBoxIcToHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeIcToHostEnable()) );
  connect( _checkBoxIcFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeIcFromHostEnable()) );
  connect( _checkBoxAuxToHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeAuxToHostEnable()) );
  connect( _checkBoxAuxFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeAuxFromHostEnable()) );
  connect( _checkBoxTtc2Host, SIGNAL(stateChanged(int)),
           this, SLOT(changeTtc2HostEnable()) );
  connect( _checkBoxTtcClock, SIGNAL(stateChanged(int)),
           this, SLOT(changeTtcClock()) );

  // EC comboboxes
  _comboEcToHostMode->addItem( "direct" );
  _comboEcToHostMode->addItem( "8b10b" );
  _comboEcToHostMode->addItem( "HDLC" );
  _comboEcFromHostMode->addItem( "direct" );
  _comboEcFromHostMode->addItem( "8b10b" );
  _comboEcFromHostMode->addItem( "HDLC" );
  connect( _comboEcToHostMode, SIGNAL(currentIndexChanged(int)),
           this, SLOT(changeEcToHostMode()) );
  connect( _comboEcFromHostMode, SIGNAL(currentIndexChanged(int)),
           this, SLOT(changeEcFromHostMode()) );

  // IC combobox (for DMA index only)
  connect( _comboIcToHostDmaIndex, SIGNAL(currentIndexChanged(int)),
           this, SLOT(changeIcToHostDmaIndex()) );

  // TTC2Host combobox (for DMA index only)
  connect( _comboTtc2HostDmaIndex, SIGNAL(currentIndexChanged(int)),
           this, SLOT(changeTtc2HostDmaIndex()) );

  // Group the ToHost E-group pushbuttons
  _egroupToHostButtons.push_back( _pushButtonEgroup0 );
  _egroupToHostButtons.push_back( _pushButtonEgroup1 );
  _egroupToHostButtons.push_back( _pushButtonEgroup2 );
  _egroupToHostButtons.push_back( _pushButtonEgroup3 );
  _egroupToHostButtons.push_back( _pushButtonEgroup4 );
  _egroupToHostButtons.push_back( _pushButtonEgroup5 );
  _egroupToHostButtons.push_back( _pushButtonEgroup6 );
  // Use a signal mapper
  _egroupToHostMapper = new QSignalMapper( this );
  for( uint32_t i=0; i<_egroupToHostButtons.size(); ++i )
    {
      connect( _egroupToHostButtons[i], SIGNAL(clicked()),
               _egroupToHostMapper, SLOT(map()) );
      _egroupToHostMapper->setMapping( _egroupToHostButtons[i], i );
    }
  connect( _egroupToHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeEgroupToHost(int)) );

  // Group the FromHost E-group pushbuttons
  _egroupFromHostButtons.push_back( _pushButtonEgroup0_1 );
  _egroupFromHostButtons.push_back( _pushButtonEgroup1_1 );
  _egroupFromHostButtons.push_back( _pushButtonEgroup2_1 );
  _egroupFromHostButtons.push_back( _pushButtonEgroup3_1 );
  _egroupFromHostButtons.push_back( _pushButtonEgroup4_1 );
  // Use a signal mapper
  _egroupFromHostMapper = new QSignalMapper( this );
  for( uint32_t i=0; i<_egroupFromHostButtons.size(); ++i )
    {
      connect( _egroupFromHostButtons[i], SIGNAL(clicked()),
               _egroupFromHostMapper, SLOT(map()) );
      _egroupFromHostMapper->setMapping( _egroupFromHostButtons[i], i );
    }
  connect( _egroupFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeEgroupFromHost(int)) );

  // Group the max chunk length spinboxes
  _spins.push_back( _spinBox2BitMax );
  _spins.push_back( _spinBox4BitMax );
  _spins.push_back( _spinBox8BitMax );
  _spins.push_back( _spinBox16BitMax );
  // Use a signal mapper
  _spinMapper = new QSignalMapper( this );
  for( uint32_t i=0; i<_spins.size(); ++i )
    {
      connect( _spins[i], SIGNAL(valueChanged(int)),
               _spinMapper, SLOT(map()) );
      _spinMapper->setMapping( _spins[i], i );
    }
  connect( _spinMapper, SIGNAL(mapped(int)),
           this, SLOT(changeMaxChunk(int)) );
  connect( _checkBoxHdlcTruncation, SIGNAL(stateChanged(int)),
           this, SLOT(changeHdlcTruncation()) );

  // Group the streamid-bit checkboxes
  _streamChecks.push_back( _checkBoxEpath0 );
  _streamChecks.push_back( _checkBoxEpath1 );
  _streamChecks.push_back( _checkBoxEpath2 );
  _streamChecks.push_back( _checkBoxEpath3 );
  _streamChecks.push_back( _checkBoxEpath4 );
  _streamChecks.push_back( _checkBoxEpath5 );
  _streamChecks.push_back( _checkBoxEpath6 );
  _streamChecks.push_back( _checkBoxEpath7 );
  // Use a signal mapper
  _streamBitMapper = new QSignalMapper( this );
  for( uint32_t i=0; i<_streamChecks.size(); ++i )
    {
      connect( _streamChecks[i], SIGNAL(stateChanged(int)),
               _streamBitMapper, SLOT(map()) );
      _streamBitMapper->setMapping( _streamChecks[i], i );
    }
  connect( _streamBitMapper, SIGNAL(mapped(int)),
           this, SLOT(changeStreamBit(int)) );

  // Connect various widgets
  connect( _buttonGroupLinkMode, SIGNAL( buttonClicked(int) ),
           this, SLOT( changeLinkMode() ) );
  connect( _buttonGroupToHostDisplay, SIGNAL( buttonClicked(int) ),
           this, SLOT( changeToHostDisplay() ) );

  connect( _spinBoxLinkNr, SIGNAL( valueChanged(int) ),
           this, SLOT( changeLink(int) ) );

  connect( _pushButtonReplicateLink, SIGNAL( clicked() ),
           this, SLOT( replicateLink() ) );
  connect( _pushButtonReplicateLinkToAll, SIGNAL( clicked() ),
           this, SLOT( replicateLinkToAll() ) );

  connect( _pushButtonReplicateEgroupToHost, SIGNAL( clicked() ),
           this, SLOT( replicateEgroupToHost() ) );
  connect( _pushButtonReplicateEgroupFromHost, SIGNAL( clicked() ),
           this, SLOT( replicateEgroupFromHost() ) );
  connect( _pushButtonReplicateEgroupToHostToAll, SIGNAL( clicked() ),
           this, SLOT( replicateEgroupToHostToAll() ) );
  connect( _pushButtonReplicateEgroupFromHostToAll, SIGNAL( clicked() ),
           this, SLOT( replicateEgroupFromHostToAll() ) );
  connect( _pushButtonDisableEgroupToHost, SIGNAL( clicked() ),
           this, SLOT( disableEgroupToHost() ) );
  connect( _pushButtonDisableEgroupFromHost, SIGNAL( clicked() ),
           this, SLOT( disableEgroupFromHost() ) );
  connect( _pushButtonEnableEgroupToHost, SIGNAL( clicked() ),
           this, SLOT( enableEgroupToHost() ) );
  connect( _pushButtonEnableEgroupFromHost, SIGNAL( clicked() ),
           this, SLOT( enableEgroupFromHost() ) );

  connect( _pushButtonGenerate, SIGNAL( clicked() ),
           this, SLOT( showGenerateDialog() ) );

  connect( _pushButtonRead, SIGNAL( clicked() ),
           this, SLOT( readLinkConfig() ) );
  connect( _pushButtonToHostFanOutSelect, SIGNAL( clicked() ),
           this, SLOT( toHostFanOutSelect() ) );
  connect( _pushButtonFromHostFanOutSelect, SIGNAL( clicked() ),
           this, SLOT( fromHostFanOutSelect() ) );
  connect( _pushButtonTimeoutConfig, SIGNAL( clicked() ),
           this, SLOT( timeoutConfig() ) );
  connect( _pushButtonClockConfig, SIGNAL( clicked() ),
           this, SLOT( clockConfig() ) );
  connect( _pushButtonOpen, SIGNAL( clicked() ),
           this, SLOT( openConfigFile() ) );
  connect( _pushButtonSave, SIGNAL( clicked() ),
           this, SLOT( saveConfigFile() ) );
  connect( _pushButtonSettings, SIGNAL( clicked() ),
           this, SLOT( showSettingsDialog() ) );
  connect( _pushButtonQuit, SIGNAL( clicked() ),
           this, SLOT( close() ) );

  connect( _checkBoxAdvanced, SIGNAL(stateChanged(int)),
           this, SLOT(showAdvancedOptions(int)) );

  connect( _checkBoxStreamId, SIGNAL(stateChanged(int)),
           this, SLOT(showStreamIdOptions(int)) );

  // Determine the number of FELIX cards present and
  // configure the card selection combobox accordingly
  int nr_of_devs = FlxCard::number_of_devices();
  _comboBoxFelix->clear();
  if( nr_of_devs > 0 )
    {
      FlxCard  felix;
      uint64_t type;
      QString  fw, qs;
      for( int i=0; i<nr_of_devs; ++i )
        {
          try {
            felix.card_open( i, LOCK_NONE );
            type = felix.card_type();
            fw = QString::fromStdString( felix.firmware_type_string() );
            qs = QString("%1 (%2, %3)").arg( i ).
              arg( type, 3, 10, QChar('0') ).arg( fw );
            felix.card_close();
            _comboBoxFelix->addItem( qs );
          }
          catch( FlxException &ex ) {
            std::cout << "### FlxCard open: " << ex.what() << std::endl;
            qs = QString::number(i);
            _comboBoxFelix->addItem( qs );
            continue;
          }
        }
    }
  else
    {
      // Hide widgets associated with FELIX card configuration download
      _labelFelix->hide();
      _comboBoxFelix->hide();
      _pushButtonRead->hide();
      _pushButtonToHostFanOutSelect->hide();
      _pushButtonFromHostFanOutSelect->hide();
      _pushButtonTimeoutConfig->hide();
      _pushButtonClockConfig->hide();
    }

  connect( _comboBoxFelix, SIGNAL(currentIndexChanged(int)),
           this, SLOT(selectedDeviceChanged(int)) );

  this->showAdvancedOptions( _checkBoxAdvanced->isChecked() );
  this->showStreamIdOptions( _checkBoxStreamId->isChecked() );
  _labelFullMode->hide();
  //_labelStatic->hide();
  //_labelKitty->hide();
  _labelEmu->hide();
#if REGMAP_VERSION < 0x500
  _labelRM5->hide();
  _radioButtonToHostEmode->hide();
  _radioButtonToHostDmaIndex->hide();
#else
  _labelRM5->show();
#endif // REGMAP_VERSION
  _labelMessage->hide();

  _frameAuxToHost->hide();
  _frameAuxFromHost->hide();
  _auxVisible = false;

  _spinBoxLinkNr->setSpecialValueText( "EMU" );

  this->readSettings();

  // Update display
  this->changeLinkMode();
}

// ----------------------------------------------------------------------------

ElinkConfig::~ElinkConfig()
{
}

// ----------------------------------------------------------------------------

void ElinkConfig::populateToHostPanel()
{
  _ewidthToHostMapper = new QSignalMapper( this );
  //connect( _ewidthToHostMapper, &QSignalMapper::mapped,
  //         this, &ElinkConfig::changeToHostEwidth );  WHY NOT??
  connect( _ewidthToHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeToHostEwidth(int)) );

  _enableToHostMapper = new QSignalMapper( this );
  connect( _enableToHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeToHostEnable(int)) );

  _modeToHostMapper = new QSignalMapper( this );
  connect( _modeToHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeToHostMode(int)) );

  for( int grp=0; grp<7; ++grp )
    {
      // Create widgets containing E-link config widgets for E-groups
      QFrame *egroupFrame = new QFrame( this );
      QVBoxLayout *egroupLayout = new QVBoxLayout( egroupFrame );
      egroupLayout->setSpacing( 2 );
      egroupLayout->setContentsMargins( 1,0,1,0 );

      QLabel *egroupLabel = new QLabel( QString("Egroup %1").arg(grp),
                                        egroupFrame );
      egroupLabel->setAlignment( Qt::AlignHCenter );
      //QFont f = egroupLabel->font();
      //f.setBold( true );
      //egroupLabel->setFont( f );
      QPalette p = egroupLabel->palette();
      p.setColor( QPalette::WindowText, QColor("blue") );
      egroupLabel->setPalette( p );
      egroupLayout->addWidget( egroupLabel );

      QComboBox *ewidthCombo = new QComboBox( egroupFrame );
      ewidthCombo->setToolTip( "Elink width" );
      egroupLayout->addWidget( ewidthCombo );
      ewidthCombo->addItem( " 2-bit" );
      ewidthCombo->addItem( " 4-bit" );
      ewidthCombo->addItem( " 8-bit" );
      ewidthCombo->addItem( " 16-bit" );
      QFont f = ewidthCombo->font();
      f.setBold( true );
      ewidthCombo->setFont( f );
      p = ewidthCombo->palette();
      p.setColor( QPalette::Active, QPalette::ButtonText, QColor("blue") );
      ewidthCombo->setPalette( p );
      connect( ewidthCombo, SIGNAL(currentIndexChanged(int)),
               _ewidthToHostMapper, SLOT(map()) );
      _ewidthToHostMapper->setMapping( ewidthCombo, grp );

      _hLayoutToHost->insertWidget( 0+grp, egroupFrame );

      // The widgets for the E-paths

      egroupWidget_t egroupWidget;
      egroupWidget.id          = grp;
      egroupWidget.enabled     = true;
      egroupWidget.frame       = egroupFrame;
      egroupWidget.label       = egroupLabel;
      egroupWidget.ewidthCombo = ewidthCombo;
      egroupWidget.ttcOptCombo = 0;

      epathWidget_t epathWidget;
      for( int i=0; i<8; ++i )
        {
          QFrame *epathFrame = new QFrame( egroupFrame );
          egroupLayout->insertWidget( 2, epathFrame );
          //epathFrame->setAutoFillBackground( true );
          epathFrame->setFrameShape( QFrame::WinPanel );
          epathFrame->setFrameShadow( QFrame::Raised );

          QVBoxLayout *epathLayout = new QVBoxLayout( epathFrame );
          epathLayout->setSpacing( 2 );
          epathLayout->setContentsMargins( 3,2,3,2 );

          epathLayout->addStretch();

          QCheckBox *epathCheck = new QCheckBox( epathFrame );
          epathCheck->setToolTip( "Elink number" );
          f = epathCheck->font();
          f.setBold( true );
          epathCheck->setFont( f );
          connect( epathCheck, SIGNAL(stateChanged(int)),
                   _enableToHostMapper, SLOT(map()) );
          _enableToHostMapper->setMapping( epathCheck, grp*8 + i );
          epathLayout->addWidget( epathCheck );

          QComboBox *epathCombo = new QComboBox( epathFrame );
          epathCombo->setToolTip( "Elink mode" );
          epathLayout->addWidget( epathCombo );
          epathCombo->addItem( "direct" );
          epathCombo->addItem( "8b10b" );
          epathCombo->addItem( "HDLC" );
          connect( epathCombo, SIGNAL(currentIndexChanged(int)),
                   _modeToHostMapper, SLOT(map()) );
          _modeToHostMapper->setMapping( epathCombo, grp*8 + i );

          QLabel *epathLabel = new QLabel( QString("Epath %1").arg(i),
                                           epathFrame );
          QFont f = epathLabel->font();
          f.setPointSize( f.pointSize()-1 );
          epathLabel->setFont( f );
          epathLabel->setAlignment( Qt::AlignHCenter );
          epathLayout->addWidget( epathLabel );

          epathLayout->addStretch();

          epathWidget.frame       = epathFrame;
          epathWidget.enableCheck = epathCheck;
          epathWidget.modeCombo   = epathCombo;
          epathWidget.label       = epathLabel;
          egroupWidget.epaths.push_back( epathWidget );
        }

      _egroupToHostWidgets.push_back( egroupWidget );
    }
  _hLayoutToHost->insertStretch( 7 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::populateFromHostPanel()
{
  _ewidthFromHostMapper = new QSignalMapper( this );
  connect( _ewidthFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostEwidth(int)) );

  _enableFromHostMapper = new QSignalMapper( this );
  connect( _enableFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostEnable(int)) );

  _modeFromHostMapper = new QSignalMapper( this );
  connect( _modeFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostMode(int)) );

  _ttcOptionMapper = new QSignalMapper( this );
  connect( _ttcOptionMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostTtcOption(int)) );

  for( int grp=0; grp<5; ++grp )
    {
      // Create widgets containing E-link config widgets for E-groups
      QFrame *egroupFrame = new QFrame( this );
      QVBoxLayout *egroupLayout = new QVBoxLayout( egroupFrame );
      egroupLayout->setSpacing( 2 );
      egroupLayout->setContentsMargins( 1,0,1,0 );

      QLabel *egroupLabel = new QLabel( QString("Egroup %1").arg(grp),
                                        egroupFrame );
      egroupLabel->setAlignment( Qt::AlignHCenter );
      QPalette p = egroupLabel->palette();
      p.setColor( QPalette::WindowText, QColor("blue") );
      egroupLabel->setPalette( p );
      egroupLayout->addWidget( egroupLabel );

      QComboBox *ewidthCombo = new QComboBox( egroupFrame );
      ewidthCombo->setToolTip( "Elink width" );
      egroupLayout->addWidget( ewidthCombo );
      ewidthCombo->addItem( " 2-bit" );
      ewidthCombo->addItem( " 4-bit" );
      ewidthCombo->addItem( " 8-bit" );
      ewidthCombo->addItem( " 16-bit" );
      QFont f = ewidthCombo->font();
      f.setBold( true );
      ewidthCombo->setFont( f );
      p = ewidthCombo->palette();
      p.setColor( QPalette::Active, QPalette::ButtonText, QColor("blue") );
      ewidthCombo->setPalette( p );
      connect( ewidthCombo, SIGNAL(currentIndexChanged(int)),
               _ewidthFromHostMapper, SLOT(map()) );
      _ewidthFromHostMapper->setMapping( ewidthCombo, grp );

      QComboBox *ttcOptionCombo = new QComboBox( egroupFrame );
      ttcOptionCombo->setToolTip( "TTC option" );
      egroupLayout->addWidget( ttcOptionCombo );
      for( int i=0; i<16; ++i )
        ttcOptionCombo->addItem( QString("TTC %1").arg(i) );
      connect( ttcOptionCombo, SIGNAL(currentIndexChanged(int)),
               _ttcOptionMapper, SLOT(map()) );
      _ttcOptionMapper->setMapping( ttcOptionCombo, grp );
#if REGMAP_VERSION < 0x500
      ttcOptionCombo->hide();
#endif // REGMAP_VERSION

      _hLayoutFromHost->insertWidget( 0+grp, egroupFrame );

      // The widgets for the E-paths

      egroupWidget_t egroupWidget;
      egroupWidget.id          = grp;
      egroupWidget.enabled     = true;
      egroupWidget.frame       = egroupFrame;
      egroupWidget.label       = egroupLabel;
      egroupWidget.ewidthCombo = ewidthCombo;
      egroupWidget.ttcOptCombo = ttcOptionCombo;

      epathWidget_t epathWidget;
      for( int i=0; i<8; ++i )
        {
          QFrame *epathFrame = new QFrame( egroupFrame );
          egroupLayout->insertWidget( 3, epathFrame );
          //epathFrame->setAutoFillBackground( true );
          epathFrame->setFrameShape( QFrame::WinPanel );
          epathFrame->setFrameShadow( QFrame::Raised );

          QVBoxLayout *epathLayout = new QVBoxLayout( epathFrame );
          epathLayout->setSpacing( 2 );
          epathLayout->setContentsMargins( 3,2,3,2 );

          epathLayout->addStretch();

          QCheckBox *epathCheck = new QCheckBox( epathFrame );
          epathCheck->setToolTip( "Elink number" );
          f = epathCheck->font();
          f.setBold( true );
          epathCheck->setFont( f );
          connect( epathCheck, SIGNAL(stateChanged(int)),
                   _enableFromHostMapper, SLOT(map()) );
          _enableFromHostMapper->setMapping( epathCheck, grp*8 + i );
          epathLayout->addWidget( epathCheck );

          // Combobox list will depend on the E-link width
          QComboBox *epathCombo = new QComboBox( epathFrame );
          epathCombo->setToolTip( "Elink mode" );
          epathLayout->addWidget( epathCombo );
          epathCombo->addItem( "direct" );
          epathCombo->addItem( "8b10b" );
          epathCombo->addItem( "HDLC" );
          connect( epathCombo, SIGNAL(currentIndexChanged(int)),
                   _modeFromHostMapper, SLOT(map()) );
          _modeFromHostMapper->setMapping( epathCombo, grp*8 + i );

          QLabel *epathLabel = new QLabel( QString("Epath %1").arg(i),
                                           epathFrame );
          QFont f = epathLabel->font();
          f.setPointSize( f.pointSize()-1 );
          epathLabel->setFont( f );
          epathLabel->setAlignment( Qt::AlignHCenter );
          epathLayout->addWidget( epathLabel );

          epathLayout->addStretch();

          epathWidget.frame       = epathFrame;
          epathWidget.enableCheck = epathCheck;
          epathWidget.modeCombo   = epathCombo;
          epathWidget.label       = epathLabel;
          egroupWidget.epaths.push_back( epathWidget );
        }

      _egroupFromHostWidgets.push_back( egroupWidget );
    }
  _hLayoutFromHost->insertStretch( 5 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::configureEpathColors( egroupWidget_t &widget,
                                        int             linkmode )
{
  // Change the E-group widgets' color dependent on the selected E-link width
#if REGMAP_VERSION >= 0x500
  bool lpgbt10 = (linkmode == LINKMODE_LPGBT10_F5 ||
                  linkmode == LINKMODE_LPGBT10_F12);
  bool lpgbt5  = (linkmode == LINKMODE_LPGBT5_F5 ||
                  linkmode == LINKMODE_LPGBT5_F12);
#endif // REGMAP_VERSION
  int width_code = widget.ewidthCombo->currentIndex();
  int i = 0;
  for( epathWidget_t e: widget.epaths )
    {
      QPalette qp = e.frame->palette();
      if( width_code == 0 )
        {
          // 2-bit
          e.frame->show();
          qp.setColor( QPalette::Window, QColor(255,255,127) );
          if( _fwMode == FIRMW_STRIP )
            {
              // Use the combo text to distinguish STRIP firmware swapped
              // (names with '*') and non-swapped E-groups/links (names with '-')
              QString str = e.modeCombo->itemText( e.modeCombo->currentIndex() );
              if( str.contains("R-") || str.contains("L*") )
                qp.setColor( QPalette::Window, STRIP_ELINK_COLOR );
            }
        }
      else if( width_code == 1 )
        {
          // 4-bit
#if REGMAP_VERSION < 0x500
          if( (i & 1) == 0 )
#else
          if( (lpgbt5 && i < 4) ||
              (!lpgbt5 && (i & 1) == 0) )
#endif // REGMAP_VERSION
            e.frame->show();
          else
            e.frame->hide();
          qp.setColor( QPalette::Window, QColor(255,170,255) );
        }
      else if( width_code == 2 )
        {
          // 8-bit
#if REGMAP_VERSION < 0x500
          if( i == 1 || i == 5 )
#else
          if( (lpgbt10 && i < 4) ||
              (lpgbt5 && i < 4 && (i & 1) == 0) ||
              (!lpgbt10 && !lpgbt5 && (i & 3) == 0) )
#endif // REGMAP_VERSION
            e.frame->show();
          else
            e.frame->hide();
          qp.setColor( QPalette::Window, QColor(170,255,255) );
        }
      else if( width_code == 3 )
        {
          // 16-bit
#if REGMAP_VERSION < 0x500
          if( i == 3 )
#else
          if( (i & 7) == 0 || (lpgbt10 && i == 2) )
#endif // REGMAP_VERSION
            e.frame->show();
          else
            e.frame->hide();
          qp.setColor( QPalette::Window, QColor(170,255,127) );
        }
      else if( width_code == 4 )
        {
          // 32-bit
          if( (i & 7) == 0 )
            e.frame->show();
          else
            e.frame->hide();
          qp.setColor( QPalette::Window, QColor(170,127,255) );
        }
      e.frame->setPalette( qp );
      ++i;
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayEgroupToHost( int group )
{
  // Display the selected 'ToHost' Egroup config of the selected GBT link
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   linkmode = cfg->linkMode();
  uint32_t   enables  = cfg->enablesToHost( group );
  uint64_t   modes    = cfg->modesToHost( group );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t mode_word  = GbtConfig::epath4BitModeWord( enables, modes );
  int      width_code;
  uint32_t dma_indices = 0;
#else
  uint32_t mode_word   = modes;
  uint32_t width_code  = cfg->widthToHost( group );
  uint32_t dma_indices = cfg->dmaIndices( group );
#endif // REGMAP_VERSION

  if( _egroupToHostWidgets[group].enabled == false )
    {
      _egroupToHostWidgets[group].frame->hide();
      return;
    }
  _egroupToHostWidgets[group].frame->show();

  // Configure the check- and comboboxes
  if( linkmode != LINKMODE_FULL )
    {
      int epath_enables = 0;
      QComboBox *cb = _egroupToHostWidgets[group].ewidthCombo;
#if REGMAP_VERSION < 0x500
      if( enables & 0x07F80 )
        {
          cb->setCurrentIndex( 0 ); // 2-bit
          epath_enables = (enables & 0x07F80) >> 7;
        }
      else if( enables & 0x00078 )
        {
          cb->setCurrentIndex( 1 ); // 4-bit
          if( enables & 0x00040 ) epath_enables |= 0x40;
          if( enables & 0x00020 ) epath_enables |= 0x10;
          if( enables & 0x00010 ) epath_enables |= 0x04;
          if( enables & 0x00008 ) epath_enables |= 0x01;
        }
      else if( enables & 0x00006 )
        {
          cb->setCurrentIndex( 2 ); // 8-bit
          if( enables & 0x00004 ) epath_enables |= 0x20;
          if( enables & 0x00002 ) epath_enables |= 0x02;
        }
      else if( enables & 0x00001 )
        {
          cb->setCurrentIndex( 3 ); // 16-bit
          if( enables & 0x00001 ) epath_enables |= 0x08;
        }
      else
        {
          // Keep the current width setting
        }
      width_code = cb->currentIndex();
#else
      epath_enables = enables;
      if( (int) width_code < cb->count() )
        cb->setCurrentIndex( width_code );
      else
        cb->setCurrentIndex( -1 );
#endif // REGMAP_VERSION

      // Next operations without side effects, so with disconnect()
      disconnect( _modeToHostMapper, SIGNAL(mapped(int)), 0, 0 );
      disconnect( _enableToHostMapper, SIGNAL(mapped(int)), 0, 0 );

      // Configure the contents of the 'mode' comboboxes
      if( _radioButtonToHostEmode->isChecked() )
        {
          uint32_t ewidth = width_code;
          for( epathWidget_t e: _egroupToHostWidgets[group].epaths )
            {
              e.modeCombo->clear();
              e.modeCombo->addItem( "direct" );
              e.modeCombo->addItem( "8b10b" );
              e.modeCombo->addItem( "HDLC" );
              if( linkmode == LINKMODE_LPGBT10_F5 ||
                  linkmode == LINKMODE_LPGBT10_F12 ||
                  linkmode == LINKMODE_LPGBT5_F5 ||
                  linkmode == LINKMODE_LPGBT5_F12 )
                {
                  e.modeCombo->addItem( "m3" ); //"TTC" );
                  e.modeCombo->addItem( "m4" ); //"Strips" );
                  e.modeCombo->addItem( "m5" ); //"Pixel" );
                  e.modeCombo->addItem( "m6" ); //"Endeavour" );
                }

              // Disable direct mode?
              this->setComboItemEnabled( e.modeCombo, 0, // direct mode
                                         _fwDirectModeIncluded );

              // Enable HDLC (index=2) only for 2-bit E-links
              if( _fwEgroupIncludes[group].TOHOST_HDLC == 1 )
                this->setComboItemEnabled( e.modeCombo, 2, (ewidth == 0) );
              else
                this->setComboItemEnabled( e.modeCombo, 2, false );

              // 8b10b mode disabled?
              if( (ewidth == 0 && _fwEgroupIncludes[group].TOHOST_02 == 0) ||
                  (ewidth == 1 && _fwEgroupIncludes[group].TOHOST_04 == 0) ||
                  (ewidth == 2 && _fwEgroupIncludes[group].TOHOST_08 == 0) ||
                  (ewidth == 3 && _fwEgroupIncludes[group].TOHOST_16 == 0) )
                this->setComboItemEnabled( e.modeCombo, 1, false );
              else
                this->setComboItemEnabled( e.modeCombo, 1, true );

              /*
              int index = e.modeCombo->findText( "Aurora" );
              if( linkmode == LINKMODE_LPGBT10_F5 ||
                  linkmode == LINKMODE_LPGBT10_F12 )
                {
                  if( index < 0 )
                    // Add "Aurora" protocol
                    e.modeCombo->addItem( "Aurora" );
                }
              else
                {
                  if( index >= 0 )
                    // Remove "Aurora" protocol
                    e.modeCombo->removeItem( index );
                }
              */
            }
        }
      else
        {
          for( epathWidget_t e: _egroupToHostWidgets[group].epaths )
            {
              e.modeCombo->clear();
              for( uint64_t i=0; i<_fwDmaToHostDescriptorCount; ++i )
                e.modeCombo->addItem( QString( "DMA %1" ).arg( i ) );
            }
        }

      // Configure enabled/disabled E-paths
      int path = 0;
      int gbt = _gbtNr;
      if( _gbtNr == FLX_LINKS ) // EMU link
        gbt = 0;
      int e_index, elinknr;
      for( epathWidget_t e: _egroupToHostWidgets[group].epaths )
        {
          if( linkmode == LINKMODE_GBT ||
              linkmode == LINKMODE_GBTWIDE ||
              linkmode == LINKMODE_FULL )
            e_index = ((group << BLOCK_EGROUP_SHIFT) | path);
          else
            e_index = ((group << BLOCK_EGROUP_SHIFT_LPGBT) | path);
          elinknr = (gbt << BLOCK_LNK_SHIFT) | e_index;
          e.enableCheck->setText( QString( "%1" ).
                                  arg(elinknr, 3, 16, QChar('0')) );

          if( epath_enables & (1 << path) )
            {
              e.enableCheck->setChecked( true );
              e.frame->setAutoFillBackground( true );
              if( _radioButtonToHostEmode->isChecked() )
                e.modeCombo->setCurrentIndex( (mode_word >> (path*4)) & 0xF );
              else
                e.modeCombo->setCurrentIndex( (dma_indices >> (path*4)) & 0xF );

              if( _checkBoxAdvanced->isChecked() )
                {
                  // Use font to indicate E-link alignment status
                  //QPalette p = e.enableCheck->palette();
                  QFont f = e.enableCheck->font();
                  if( _gbtNr != FLX_LINKS &&
                      (_fwElinksAligned[gbt] & ((uint64_t)1 << e_index)) != 0 )
                    //p.setColor( QPalette::WindowText, QColor("magenta") );
                    f.setUnderline( true );
                  else
                    f.setUnderline( false );
                  e.enableCheck->setFont( f );
                }
            }
          else
            {
              e.enableCheck->setChecked( false );
              e.frame->setAutoFillBackground( false );
              e.modeCombo->setCurrentIndex( -1 );

              // Use font to indicate E-link alignment status
              QFont f = e.enableCheck->font();
              f.setUnderline( false );
              e.enableCheck->setFont( f );
            }

          e.enableCheck->setEnabled( true );
          ++path;
        }

      // Reconnect
      connect( _modeToHostMapper, SIGNAL(mapped(int)),
               this, SLOT(changeToHostMode(int)) );
      connect( _enableToHostMapper, SIGNAL(mapped(int)),
               this, SLOT(changeToHostEnable(int)) );

      this->configureEpathColors( _egroupToHostWidgets[group], linkmode );
    }
  else if( group == 0 ) // LINKMODE_FULL
    {
      // Next operations without side effects, so with disconnect()
      disconnect( _modeToHostMapper, SIGNAL(mapped(int)), 0, 0 );

      // FULL mode enable checkbox
      epathWidget_t e = _egroupToHostWidgets[group].epaths[0];
      e.enableCheck->setChecked( enables & FULL_ENABLED );  // FULL mode
      e.frame->setAutoFillBackground( enables & FULL_ENABLED );
      int elinknr = _gbtNr << BLOCK_LNK_SHIFT;
      if( _gbtNr != FLX_LINKS )
        e.enableCheck->setText( QString( "%1" ).
                                arg(elinknr, 3, 16, QChar('0')) );
      else
        // EMU link
        e.enableCheck->setText( "EMU" );

      // Configure the contents of the 'mode' comboboxes
      if( _radioButtonToHostDmaIndex->isChecked() &&
          _gbtNr != FLX_LINKS ) // Don't show for EMU link
        {
          e.modeCombo->clear();
          for( uint64_t i=0; i<_fwDmaToHostDescriptorCount; ++i )
            e.modeCombo->addItem( QString( "DMA %1" ).arg( i ) );
          e.modeCombo->show();

          int path = 0;
          if( enables & FULL_ENABLED )
            e.modeCombo->setCurrentIndex( (dma_indices >> (path*4)) & 0xF );
          else
            e.modeCombo->setCurrentIndex( -1 );
        }
      else
        {
          e.modeCombo->hide();
        }

      // Reconnect
      connect( _modeToHostMapper, SIGNAL(mapped(int)),
               this, SLOT(changeToHostMode(int)) );
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayEgroupFromHost( int group )
{
  // Display the selected 'FromHost' Egroup config of the selected GBT link
  GbtConfig *cfg       = &_gbtConfig[_gbtNr];
  uint32_t   linkmode  = cfg->linkMode();
  uint32_t   enables   = cfg->enablesFromHost( group );
  uint64_t   modes     = cfg->modesFromHost( group );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t   mode_word = GbtConfig::epath4BitModeWord( enables, modes );
  int        width_code;
  bool       lti_ttc   = false;
#else
  uint32_t   mode_word  = modes;
  uint32_t   width_code = cfg->widthFromHost( group );
  uint32_t   ttc_option = cfg->ttcOptionFromHost( group );
  bool       lti_ttc    = (linkmode == LINKMODE_FULL && cfg->ltiTtc());
#endif // REGMAP_VERSION

  if( _egroupFromHostWidgets[group].enabled == false ||
      lti_ttc ||            // FromHost is in LTI-mode
      _gbtNr == FLX_LINKS ) // EMU link
    {
      _egroupFromHostWidgets[group].frame->hide();
      return;
    }
  _egroupFromHostWidgets[group].frame->show();

  // Configure the check- and comboboxes
  int epath_enables = 0;
  QComboBox *cb = _egroupFromHostWidgets[group].ewidthCombo;
#if REGMAP_VERSION < 0x500
  if( enables & 0x07F80 )
    {
      cb->setCurrentIndex( 0 );
      epath_enables = (enables & 0x07F80) >> 7;
    }
  else if( enables & 0x00078 )
    {
      cb->setCurrentIndex( 1 );
      if( enables & 0x00040 ) epath_enables |= 0x40;
      if( enables & 0x00020 ) epath_enables |= 0x10;
      if( enables & 0x00010 ) epath_enables |= 0x04;
      if( enables & 0x00008 ) epath_enables |= 0x01;
    }
  else if( enables & 0x00006 )
    {
      cb->setCurrentIndex( 2 );
      if( enables & 0x00004 ) epath_enables |= 0x20;
      if( enables & 0x00002 ) epath_enables |= 0x02;
    }
  else if( enables & 0x00001 )
    {
      cb->setCurrentIndex( 3 );
      if( enables & 0x00001 ) epath_enables |= 0x08;
    }
  else
    {
      // Keep the current width setting
    }
  width_code = cb->currentIndex();
#else
  epath_enables = enables;
  if( (int) width_code < cb->count() )
    cb->setCurrentIndex( width_code );
  else
    cb->setCurrentIndex( -1 );
  cb->setVisible( _fwMode != FIRMW_STRIP ); // For STRIP FromHost

  cb = _egroupFromHostWidgets[group].ttcOptCombo;
  if( (int) ttc_option < cb->count() )
    cb->setCurrentIndex( ttc_option );
  else
    cb->setCurrentIndex( -1 );
  cb->setVisible( _fwMode != FIRMW_STRIP ); // For STRIP FromHost
#endif // REGMAP_VERSION

  // Next operations without side effects, so with disconnect()
  disconnect( _modeFromHostMapper, SIGNAL(mapped(int)), 0, 0 );
  disconnect( _enableFromHostMapper, SIGNAL(mapped(int)), 0, 0 );

  // Configure the contents of the 'mode' comboboxes and restore selected modes
  int path = 0;
  uint32_t ewidth = width_code;
  for( epathWidget_t e: _egroupFromHostWidgets[group].epaths )
    {
      e.modeCombo->clear();
      e.modeCombo->addItem( "direct" );
      e.modeCombo->addItem( "8b10b" );
      e.modeCombo->addItem( "HDLC" );
      if( linkmode == LINKMODE_GBT ||
          linkmode == LINKMODE_GBTWIDE ||
          linkmode == LINKMODE_FULL )
        {
#if REGMAP_VERSION < 0x500
          if( ewidth == 0 )      // 2-bit
            {
              e.modeCombo->addItem( "TTC-0" );
              e.modeCombo->addItem( "TTC-6" );
              e.modeCombo->addItem( "TTC-8" );
            }
          else if( ewidth == 1 ) // 4-bit
            {
              e.modeCombo->addItem( "TTC-1" );
              e.modeCombo->addItem( "TTC-2" );
              e.modeCombo->addItem( "TTC-5" );
            }
          else if( ewidth == 2 ) // 8-bit
            {
              e.modeCombo->addItem( "TTC-3" );
              e.modeCombo->addItem( "TTC-4" );
              e.modeCombo->addItem( "FEI4" );  // FEI4B-specific (7 May 2018)
              e.modeCombo->addItem( "TTC-7" ); // Added 3 Feb 2021
            }
          else if( ewidth == 3 ) // 16-bit
            {
            }
#else
          e.modeCombo->addItem( "TTC" );
#endif // REGMAP_VERSION
        }
      else
        {
          e.modeCombo->addItem( "TTC" );
          e.modeCombo->addItem( "m4" ); //"Strips" );
          e.modeCombo->addItem( "m5" ); //"Pixel" );
          e.modeCombo->addItem( "m6" ); //"Endeavour" );
        }

      if( e.enableCheck->isChecked() )
        e.modeCombo->setCurrentIndex( (mode_word>>(path*4)) & 0xF );
      else
        e.modeCombo->setCurrentIndex( -1 );
      ++path;
    }

  // Disable certain modes on the basis of the firmware configuration
  for( epathWidget_t e: _egroupFromHostWidgets[group].epaths )
    {
      // Disable direct mode?
      this->setComboItemEnabled( e.modeCombo, 0, // direct mode
                                 _fwDirectModeIncluded );

      // Enable HDLC (index=2) only for 2-bit E-links
      if( _fwEgroupIncludes[group].FROMHOST_HDLC == 1 )
        this->setComboItemEnabled( e.modeCombo, 2, (ewidth == 0) );
      else
        this->setComboItemEnabled( e.modeCombo, 2, false );

      // 8b10b mode disabled?
      if( (ewidth == 0 && _fwEgroupIncludes[group].FROMHOST_02 == 0) ||
          (ewidth == 1 && _fwEgroupIncludes[group].FROMHOST_04 == 0) ||
          (ewidth == 2 && _fwEgroupIncludes[group].FROMHOST_08 == 0) )
        this->setComboItemEnabled( e.modeCombo, 1, false );
      else
        this->setComboItemEnabled( e.modeCombo, 1, true );
    }

  // Configure enabled/disabled E-paths
  path = 0;
  int gbt = _gbtNr;
  if( _gbtNr == FLX_LINKS ) // EMU link
    gbt = 0;
  int elinknr;
  for( epathWidget_t e: _egroupFromHostWidgets[group].epaths )
    {
      if( linkmode == LINKMODE_GBT ||
          linkmode == LINKMODE_GBTWIDE ||
          linkmode == LINKMODE_FULL )
        elinknr = ((gbt << BLOCK_LNK_SHIFT) |
                   (group << BLOCK_EGROUP_SHIFT) | path);
      else if( _fwMode == FIRMW_STRIP )
        // NB: lpGBT STRIP firmware has 5 FromHost paths/E-links
        elinknr = ((gbt << BLOCK_LNK_SHIFT) |
                   ((group * 5) + path));
      else
        // lpGBT-type firmware
        elinknr = ((gbt << BLOCK_LNK_SHIFT) |
                   (group << BLOCK_EGROUP_SHIFT_LPGBT) | path);
      e.enableCheck->setText( QString( "%1" ).
                              arg(elinknr, 3, 16, QChar('0')) );
      if( epath_enables & (1 << path) )
        {
          e.enableCheck->setChecked( true );
          e.frame->setAutoFillBackground( true );
          e.modeCombo->setCurrentIndex( (mode_word >> (path*4)) & 0xF );

          if( _fwMode == FIRMW_STRIP )
            {
              // STRIP firmware FromHost E-link mode text gets name depending
              // on 'swapped' or 'non-swapped' state
              if( (_itkStripLcbR3l1ElinkSwap & (1L<<((_gbtNr<<2) + group))) != 0 )
                e.modeCombo->setItemText( e.modeCombo->currentIndex(),
                                          STRIP_ELINK_STR_SWAPPED[path] );
              else
                e.modeCombo->setItemText( e.modeCombo->currentIndex(),
                                          STRIP_ELINK_STR[path] );
            }
          e.label->setVisible( _fwMode != FIRMW_STRIP);
        }
      else
        {
          e.enableCheck->setChecked( false );
          e.frame->setAutoFillBackground( false );
          e.modeCombo->setCurrentIndex( -1 );
          e.label->setVisible( _fwMode != FIRMW_STRIP);
        }
      e.enableCheck->setEnabled( true );
      ++path;
    }

  // Reconnect
  connect( _modeFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostMode(int)) );
  connect( _enableFromHostMapper, SIGNAL(mapped(int)),
           this, SLOT(changeFromHostEnable(int)) );

  this->configureEpathColors( _egroupFromHostWidgets[group] );

  // lpGBT: FromHost E-groups are only 8 bits (rather than 16)
  if( _fwMode == FIRMW_STRIP )
    {
      // NB: STRIP firmware has 5 FromHost paths/(virtual) E-links
      for( size_t i=5; i<_egroupFromHostWidgets[group].epaths.size(); ++i )
        _egroupFromHostWidgets[group].epaths[i].frame->hide();
    }
  else if( cfg->linkMode() == LINKMODE_LPGBT10_F5 ||
           cfg->linkMode() == LINKMODE_LPGBT5_F5 ||
           cfg->linkMode() == LINKMODE_LPGBT10_F12 ||
           cfg->linkMode() == LINKMODE_LPGBT5_F12 )
    {
      for( size_t i=4; i<_egroupFromHostWidgets[group].epaths.size(); ++i )
        _egroupFromHostWidgets[group].epaths[i].frame->hide();
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayEgroupToHostInfo( int group )
{
  // Update the information shown in the labels above the ToHost panel
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   enables  = cfg->enablesToHost( group );
  uint32_t   linkmode = cfg->linkMode();
  uint32_t   streamid = cfg->streamIdBits( group );
  uint64_t   modes    = cfg->modesToHost( group );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t   mode_word = GbtConfig::epath4BitModeWord( enables, modes );
  int        width_in_bits = -1; // Dummy
#else
  uint32_t   mode_word     = modes;
  uint32_t   width_code    = cfg->widthToHost( group );
  int        width_in_bits = GbtConfig::widthCodeToBits( width_code );
#endif // REGMAP_VERSION

  _labelToHostEgroup->setText( QString( "%1:" ).arg( group ) );

  // Update the displayed enable-bit mask word
  if( linkmode == LINKMODE_FULL )
    _labelToHostEnableMask->setText( QString("0x%1").
                                     arg(enables,5,16,QChar('0')) );
  else
    _labelToHostEnableMask->setText( QString("0x%1").
                                     arg(enables & 0xFFFF,4,16,QChar('0')) );

  // Update the displayed E-path mode word
  _labelToHostModeMask->setText( QString("0x%1").
                                 arg(mode_word,8,16,QChar('0')) );

  // Update the number-of-bits string
  QString qs = this->epathBitsString( enables, width_in_bits );
  _labelToHostBits->setText( qs );

  // Update stream-ID checkboxes
  disconnect( _streamBitMapper, SIGNAL(mapped(int)), 0, 0 );
  for( unsigned int i=0; i<_streamChecks.size(); ++i )
    _streamChecks[i]->setChecked( ((streamid & (1<<i)) != 0) );
  connect( _streamBitMapper, SIGNAL(mapped(int)),
           this, SLOT(changeStreamBit(int)) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayEgroupFromHostInfo( int group )
{
  // Update the information shown in the labels above the ToHost panel
  GbtConfig *cfg     = &_gbtConfig[_gbtNr];
  uint32_t   enables = cfg->enablesFromHost( group );
  uint64_t   modes   = cfg->modesFromHost( group );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t   mode_word = GbtConfig::epath4BitModeWord( enables, modes );
  int        width_in_bits = -1; // Dummy
#else
  uint32_t   mode_word     = modes;
  uint32_t   width_code    = cfg->widthFromHost( group );
  int        width_in_bits = GbtConfig::widthCodeToBits( width_code );
#endif // REGMAP_VERSION

  _labelFromHostEgroup->setText( QString( "%1:" ).arg( group ) );

  // Update the displayed enable-bit mask word
  _labelFromHostEnableMask->setText( QString("0x%1").
                                     arg(enables & 0xFFFF,4,16,QChar('0')) );

  // Update the displayed E-path mode word
  _labelFromHostModeMask->setText( QString("0x%1").
                                   arg(mode_word,8,16,QChar('0')) );

  // Update the number-of-bits string
  QString qs = this->epathBitsString( enables, width_in_bits );
  _labelFromHostBits->setText( qs );
}

// ----------------------------------------------------------------------------

QString ElinkConfig::epathBitsString( uint32_t enables,
                                      uint32_t width_in_bits )
{
#if REGMAP_VERSION < 0x500
  // Get a word indicating the number of bits per E-path:
  // a nibble per E-path indicating the number of bits minus 1
  uint32_t nbits_word = GbtConfig::epathBitsWord( enables );
  width_in_bits = width_in_bits;

  // Compile a string describing the number of bits per E-path/FIFO,
  // in order of E-paths (zero means E-path/FIFO is not in use)

  // Go through the nibbles in order (low-to-high) compiling the string
  QString  qs;
  uint32_t nbits;
  for( int i=0; i<FLX_ELINK_PATHS; ++i )
    {
      nbits = (nbits_word >> (i*4)) & 0xF;
      if( !qs.isEmpty() ) qs += '+';
      if( nbits != 0 )
        qs += QString::number( nbits + 1 );
      else
        qs += '0';
    }
  //qs += QString(" (indices %1)").arg( GbtConfig_t::epathBitIndexWord(enables),
  //                                  8, 16, QChar('0') );
#else
  // Get a word indicating the number of bits per E-path
  // a byte per E-path indicating the number of bits
  uint64_t nbits_word = GbtConfig::epathBitsWord( enables, width_in_bits );

  // Compile a string describing the number of bits per E-path/FIFO,
  // in order of E-paths (zero means E-path/FIFO is not in use)

  // Go through the bytes in order (low-to-high) compiling the string
  QString  qs;
  uint64_t nbits;
  for( int i=0; i<FLX_ELINK_PATHS; ++i )
    {
      nbits = (nbits_word >> (i*8)) & 0xFF;
      if( !qs.isEmpty() ) qs += '+';
      if( nbits != 0 )
        qs += QString::number( nbits );
      else
        qs += '0';
    }
#endif // REGMAP_VERSION

  return qs;
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayToHostConfig()
{
  // Map various 'ToHost' configuration stuff of the selected link
  // onto the GUI, except for the Egroups
  // (done in displayEgroupToHost() and displayEgroupToHostInfo())
  GbtConfig *cfg        = &_gbtConfig[_gbtNr];
  uint32_t   chunksizes = cfg->maxChunkWord();
  uint32_t   linkmode   = cfg->linkMode();

  // DEBUG
  //QMessageBox::warning( this, "displayToHost",
  //                      QString::number(group) );

  // Configure the E-group pushbuttons
  if( linkmode == LINKMODE_GBT )
    {
      _pushButtonEgroup0->show();
      _pushButtonEgroup1->show();
      _pushButtonEgroup2->show();
      _pushButtonEgroup3->show();
      _pushButtonEgroup4->show();
      _pushButtonEgroup5->hide();
      _pushButtonEgroup6->hide();

      _pushButtonReplicateEgroupToHost->setEnabled( true );
      _pushButtonReplicateEgroupToHostToAll->setEnabled( true );
    }
  else if( linkmode == LINKMODE_GBTWIDE )
    {
      _pushButtonEgroup0->show();
      _pushButtonEgroup1->show();
      _pushButtonEgroup2->show();
      _pushButtonEgroup3->show();
      _pushButtonEgroup4->show();
      _pushButtonEgroup5->show();
      _pushButtonEgroup6->show();

      _pushButtonReplicateEgroupToHost->setEnabled( true );
      _pushButtonReplicateEgroupToHostToAll->setEnabled( true );
    }
  else if( linkmode == LINKMODE_LPGBT10_F5 ||
           linkmode == LINKMODE_LPGBT5_F5 ||
           linkmode == LINKMODE_LPGBT10_F12 ||
           linkmode == LINKMODE_LPGBT5_F12 )
    {
      _pushButtonEgroup0->show();
      _pushButtonEgroup1->show();
      _pushButtonEgroup2->show();
      _pushButtonEgroup3->show();
      _pushButtonEgroup4->show();
      _pushButtonEgroup5->show();
      if( linkmode == LINKMODE_LPGBT10_F5 ||
          linkmode == LINKMODE_LPGBT5_F5 )
        _pushButtonEgroup6->show();
      else
        _pushButtonEgroup6->hide();

      _pushButtonReplicateEgroupToHost->setEnabled( true );
      _pushButtonReplicateEgroupToHostToAll->setEnabled( true );
    }
  else
    {
      // FULL mode
      _pushButtonEgroup0->hide();
      _pushButtonEgroup1->hide();
      _pushButtonEgroup2->hide();
      _pushButtonEgroup3->hide();
      _pushButtonEgroup4->hide();
      _pushButtonEgroup5->hide();
      _pushButtonEgroup6->hide();

      _pushButtonReplicateEgroupToHost->setEnabled( false );
      _pushButtonReplicateEgroupToHostToAll->setEnabled( false );
    }

  // FULL mode enables
  /* (NOW DONE IN displayEgroupToHost(), 7 Apr 2022)
  if( linkmode == LINKMODE_FULL )
    {
      uint32_t enables = cfg->enablesToHost( 0 );
      QCheckBox *cb = _egroupToHostWidgets[0].epaths[0].enableCheck;
      QFrame    *f  = _egroupToHostWidgets[0].epaths[0].frame;
      cb->setChecked( enables & FULL_ENABLED );  // FULL mode
      f->setAutoFillBackground( enables & FULL_ENABLED );

      //enables = cfg->enablesToHost( TOHOST_MINI_GROUP );
      //_checkBoxTtc2Host->setChecked( enables & TTC2H_ENABLED ); // TTCtoHost
      //enables = cfg->enablesToHost( 0 ); // Restore 'enables'
    }
  */
  // Configure the maximum chunk size spinboxes
  for( uint32_t i=0; i<4; ++i )
    _spins[i]->setValue( ((chunksizes >> (i*3)) & 0x7)*FLX_MAXCHUNK_UNIT );

  // Some other link mode dependent configuration
  epathWidget_t e = _egroupToHostWidgets[0].epaths[0];
  if( linkmode == LINKMODE_FULL )
    {
      // FULL mode
      _radioButtonFullMode->setChecked( true );
      //_labelFullMode->show();
      /*{ (NOW DONE IN displayEgroupToHost(), 7 Apr 2022)
        //e.enableCheck->setText( "Enable" );
        // (### To be added rather to displayEgroupFromHost()?)
        int elinknr = _gbtNr << BLOCK_LNK_SHIFT;
        if( _gbtNr != FLX_LINKS )
          e.enableCheck->setText( QString( "%1" ).
                                  arg(elinknr, 3, 16, QChar('0')) );
        else
          // EMU link
          e.enableCheck->setText( "---" );
      }*/
      e.label->setText( "FULLMODE" );
      e.modeCombo->hide();
      QPalette qp = e.frame->palette();
      qp.setColor( QPalette::Window, QColor("orange") );
      e.frame->setPalette( qp );

      _egroupToHostWidgets[0].label->hide();
      _egroupToHostWidgets[0].ewidthCombo->hide();

      // Hide all ToHost Egroup0 Epaths except 0
      int i = 0;
      for( epathWidget_t e: _egroupToHostWidgets[0].epaths )
        {
          if( i != 0 )
            e.frame->hide();
          else
            e.frame->show();
          ++i;
        }

    }
  else
    {
      // (### Is this if-statement redundant?)
      if( linkmode == LINKMODE_GBTWIDE )
        // GBT WIDE mode
        _radioButtonWideMode->setChecked( true );
      else if( linkmode == LINKMODE_GBT )
        // GBT mode
        _radioButtonNormalMode->setChecked( true );
      else if( linkmode == LINKMODE_LPGBT10_F5 )
        // lpGBT 10.24Gbps FEC5
        _radioButtonLpGbtMode->setChecked( true );
      else if( linkmode == LINKMODE_LPGBT5_F5 )
        // lpGBT 5.12Gbps FEC5
        _radioButtonLpGbtMode->setChecked( true );
      else if( linkmode == LINKMODE_LPGBT10_F12 )
        // lpGBT 10.24Gbps FEC12
        _radioButtonLpGbtMode->setChecked( true );
      else if( linkmode == LINKMODE_LPGBT5_F12 )
        // lpGBT 5.12Gbps FEC12
        _radioButtonLpGbtMode->setChecked( true );

      // Restore stuff for non-FULLMODE
      //_labelFullMode->hide();
      e.modeCombo->show();
      e.label->setText( "Epath 0" );

      _egroupToHostWidgets[0].label->show();
      _egroupToHostWidgets[0].ewidthCombo->show();
    }

  // GUI customization for the To-Host EC+IC+AUX+TTCtoHost group: mode dependent
  if( linkmode == LINKMODE_FULL )
    {
      _frameEcToHost->hide();
      _frameIcToHost->hide();
    }
  else
    {
      _frameEcToHost->show();
      _frameIcToHost->show();

      // Elink numbers for EC, IC, AUX
      int gbt = _gbtNr;
      if( _gbtNr == FLX_LINKS ) gbt = 0; // EMU link
      int elinknr = (gbt << BLOCK_LNK_SHIFT);
      _checkBoxEcToHost->setText( QString( "EC (%1)" ).
                                  arg(elinknr | _ecToHostIndex,
                                      2, 16, QChar('0')) );
      _checkBoxIcToHost->setText( QString( "IC (%1)" ).
                                  arg(elinknr | _icToHostIndex,
                                      2, 16, QChar('0')) );
      _checkBoxAuxToHost->setText( QString( "AUX (%1)" ).
                                   arg(elinknr | _auxToHostIndex,
                                       2, 16, QChar('0')) );

      // The EC/IC/AUX/TTCtoHost checkboxes
      uint32_t enables = cfg->enablesToHost( TOHOST_MINI_GROUP );
      disconnect( _checkBoxEcToHost, SIGNAL(stateChanged(int)), 0, 0 );
      disconnect( _checkBoxIcToHost, SIGNAL(stateChanged(int)), 0, 0 );
      disconnect( _checkBoxAuxToHost, SIGNAL(stateChanged(int)), 0, 0 );
//#if REGMAP_VERSION < 0x500
      _checkBoxEcToHost->setChecked( enables & EC_ENABLED );
      _checkBoxIcToHost->setChecked( enables & IC_ENABLED );
      _checkBoxAuxToHost->setChecked( enables & AUX_ENABLED );
      _frameEcToHost->setAutoFillBackground( enables & EC_ENABLED );
      _frameIcToHost->setAutoFillBackground( enables & IC_ENABLED );
      _frameAuxToHost->setAutoFillBackground( enables & AUX_ENABLED );
//#else TO-BE-DEFINED
//    _checkBoxEcToHost->setChecked( enables & 0x0001 );
//    _checkBoxIcToHost->setChecked( enables & 0x0002 );
//    _checkBoxAuxToHost->setChecked( enables & 0x0004 );
//    _frameEcToHost->setAutoFillBackground( enables & 0x0001 );
//    _frameIcToHost->setAutoFillBackground( enables & 0x0002 );
//    _frameAuxToHost->setAutoFillBackground( enables & 0x0004 );
//#endif // REGMAP_VERSION
      // Reconnect
      connect( _checkBoxEcToHost, SIGNAL(stateChanged(int)),
               this, SLOT(changeEcToHostEnable()) );
      connect( _checkBoxIcToHost, SIGNAL(stateChanged(int)),
               this, SLOT(changeIcToHostEnable()) );
      connect( _checkBoxAuxToHost, SIGNAL(stateChanged(int)),
               this, SLOT(changeAuxToHostEnable()) );

      //_checkBoxTtc2Host->setChecked( enables & TTC2H_ENABLED );
      //_frameTtc2Host->setAutoFillBackground( enables & TTC2H_ENABLED );

      // Use font color to indicate E-link alignment status
      if( _gbtNr != FLX_LINKS )
        {
          QFont f = _checkBoxEcToHost->font();
          if( _checkBoxAdvanced->isChecked() &&
              (_fwElinksAligned[gbt] & ((uint64_t)1 << _ecToHostIndex)) != 0 )
            f.setUnderline( true );
          else
            f.setUnderline( false );
          _checkBoxEcToHost->setFont( f );

          f = _checkBoxIcToHost->font();
          if( _checkBoxAdvanced->isChecked() &&
              (_fwElinksAligned[gbt] & ((uint64_t)1 << _icToHostIndex)) != 0 )
            f.setUnderline( true );
          else
            f.setUnderline( false );
          _checkBoxIcToHost->setFont( f );

          f = _checkBoxAuxToHost->font();
          if( _checkBoxAdvanced->isChecked() &&
              (_fwElinksAligned[gbt] & ((uint64_t)1 << _auxToHostIndex)) != 0 )
            f.setUnderline( true );
          else
            f.setUnderline( false );
          _checkBoxAuxToHost->setFont( f );
        }

      // The EC mode combobox
      disconnect( _comboEcToHostMode, SIGNAL(currentIndexChanged(int)), 0, 0 );
      _comboEcToHostMode->clear();
      if( _radioButtonToHostEmode->isChecked() )
        {
          _comboEcToHostMode->addItem( "direct" );
          _comboEcToHostMode->addItem( "8b10b" );
          if( _fwMode != FIRMW_STRIP )
            _comboEcToHostMode->addItem( "HDLC" );
          else
            _comboEcToHostMode->addItem( "Endeavour" );

          // Disable direct mode ?
          if( _radioButtonToHostEmode->isChecked() )
            this->setComboItemEnabled( _comboEcToHostMode, 0,
                                       _fwDirectModeIncluded );

          if( _fwMode == FIRMW_LTDB )
            this->setComboItemEnabled( _comboEcToHostMode, 1, false ); // 8b10b

          if( _checkBoxEcToHost->isChecked() )
            {
              uint64_t modes = cfg->modesToHost( TOHOST_MINI_GROUP );
//#if REGMAP_VERSION < 0x500
              _comboEcToHostMode->setCurrentIndex( (modes >>
                                                    (EC_ENABLED_BIT*4))&0xFULL );
//#else
//            _comboEcToHostMode->setCurrentIndex( modes & 0xFULL );
//#endif // REGMAP_VERSION
            }
          else
            {
              _comboEcToHostMode->setCurrentIndex( -1 );
            }
        }
      else
        {
          for( uint64_t i=0; i<_fwDmaToHostDescriptorCount; ++i )
            _comboEcToHostMode->addItem( QString( "DMA %1" ).arg( i ) );

          if( _checkBoxEcToHost->isChecked() )
            {
#if REGMAP_VERSION >= 0x500
              uint32_t dma_indices = cfg->dmaIndices( TOHOST_MINI_GROUP );
              _comboEcToHostMode->setCurrentIndex( (dma_indices>>(7*4)) & 0xF );
#endif // REGMAP_VERSION
            }
          else
            {
              _comboEcToHostMode->setCurrentIndex( -1 );
            }
        }

      if( _fwMode == FIRMW_STRIP )
        _comboEcToHostMode->setEnabled( _radioButtonToHostDmaIndex->isChecked() );
      else
        _comboEcToHostMode->setEnabled( _checkBoxEcToHost->isChecked() );
      //QMessageBox::warning( this, "ENABLE",
      //                      QString("%1").arg((modes>>(EC_ENABLED_BIT*4)) & 0xFULL) );
      // Reconnect
      connect( _comboEcToHostMode, SIGNAL(currentIndexChanged(int)),
               this, SLOT(changeEcToHostMode()) );
    }

  // The IC and TTC2Host mode combobox (only visible when 'DMA index' display selected)
  // (but ToHost IC not visible in FULL mode)
  if( _radioButtonToHostDmaIndex->isChecked() )
    {
      // IC
      disconnect( _comboIcToHostDmaIndex, SIGNAL(currentIndexChanged(int)), 0,0 );

      _comboIcToHostDmaIndex->clear();
      for( uint64_t i=0; i<_fwDmaToHostDescriptorCount; ++i )
        _comboIcToHostDmaIndex->addItem( QString( "DMA %1" ).arg( i ) );

      if( _checkBoxIcToHost->isChecked() )
        {
#if REGMAP_VERSION >= 0x500
          uint32_t dma_indices = cfg->dmaIndices( TOHOST_MINI_GROUP );
          _comboIcToHostDmaIndex->setCurrentIndex( (dma_indices>>(6*4)) & 0xF );
#endif // REGMAP_VERSION
        }
      else
        {
          _comboIcToHostDmaIndex->setCurrentIndex( -1 );
        }

      // Reconnect
      connect( _comboIcToHostDmaIndex, SIGNAL(currentIndexChanged(int)),
               this, SLOT(changeIcToHostDmaIndex()) );
      _comboIcToHostDmaIndex->show();

      // TTC2Host
      disconnect( _comboTtc2HostDmaIndex, SIGNAL(currentIndexChanged(int)), 0,0 );

      _comboTtc2HostDmaIndex->clear();
      for( uint64_t i=0; i<_fwDmaToHostDescriptorCount; ++i )
        _comboTtc2HostDmaIndex->addItem( QString( "DMA %1" ).arg( i ) );

      if( _checkBoxTtc2Host->isChecked() )
        {
#if REGMAP_VERSION >= 0x500
          uint32_t dma_indices = _gbtConfig[0].dmaIndices( TOHOST_MINI_GROUP );
          _comboTtc2HostDmaIndex->setCurrentIndex( (dma_indices>>(0*4)) & 0xF );
#endif // REGMAP_VERSION
        }
      else
        {
          _comboTtc2HostDmaIndex->setCurrentIndex( -1 );
        }
      // Reconnect
      connect( _comboTtc2HostDmaIndex, SIGNAL(currentIndexChanged(int)),
               this, SLOT(changeTtc2HostDmaIndex()) );

      if( _checkBoxTtc2Host->isChecked() )
        _comboTtc2HostDmaIndex->show();
      else
        _comboTtc2HostDmaIndex->hide();
    }
  else
    {
      _comboIcToHostDmaIndex->hide(); // Only visible when displaying DMA index
      _comboTtc2HostDmaIndex->hide(); // Only visible when displaying DMA index
    }

  // Link dependent GUI customization for the ToHost EC+IC+AUX+TTCtoHost group
  //if( _gbtNr == 0 )
  if( true )
    {
      //_frameTtc2Host->show();

      // Display Elink number for TTCtoHost
#if REGMAP_VERSION < 0x500
      int elinknr = (((_spinBoxLinkNr->maximum()+1) << BLOCK_LNK_SHIFT) |
                     (7 << BLOCK_EGROUP_SHIFT) | 3);
#else
      int elinknr = (24 << BLOCK_LNK_SHIFT); // E-link 0x600
#endif // REGMAP_VERSION
      _checkBoxTtc2Host->setText( QString( "TTC-to-Host (%1)" ).
                                  arg(elinknr, 2, 16, QChar('0')) );
    }
  else
    {
      //_frameTtc2Host->hide();
    }

  // EMU link ToHost dependent customization
  if( _gbtNr == FLX_LINKS )
    {
      // EMU link: hide EC/IC/AUX stuff
      _frameEcToHost->hide();
      _frameIcToHost->hide();
      if( !_frameAuxToHost->isHidden() )
        {
          _frameAuxToHost->hide();
          _auxVisible = true;
        }
      // EMU link: hide StreamID stuff
      _labelStreamId->hide();
      for( QCheckBox *cb: _streamChecks )
        cb->hide();
    }
  else
    {
      // Other links: show EC/IC stuff
      if( linkmode != LINKMODE_FULL )
        {
          _frameEcToHost->show();
          _frameIcToHost->show();
          if( _auxVisible )
            _frameAuxToHost->show();
        }
      // Show StreamID stuff ?
      if( _checkBoxStreamId->isChecked() )
        {
          _labelStreamId->show();
          for( QCheckBox *cb: _streamChecks )
            cb->show();
        }
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::displayFromHostConfig()
{
  // Map various 'FromHost' configuration stuff of the selected link
  // onto the GUI, except for the Egroups
  // (done in displayEgroupFromHost() and displayEgroupFromHostInfo())
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   linkmode = cfg->linkMode();

  if( linkmode == LINKMODE_GBT ||
      linkmode == LINKMODE_FULL )
    {
      _pushButtonEgroup2_1->show();
      _pushButtonEgroup3_1->show();
      _pushButtonEgroup4_1->show();
    }
  else if( linkmode == LINKMODE_GBTWIDE )
    {
      _pushButtonEgroup2_1->show();
      _pushButtonEgroup3_1->hide();
      _pushButtonEgroup4_1->hide();
    }
  else if( linkmode == LINKMODE_LPGBT10_F5 ||
           linkmode == LINKMODE_LPGBT5_F5 ||
           linkmode == LINKMODE_LPGBT10_F12 ||
           linkmode == LINKMODE_LPGBT5_F12 )
    {
      _pushButtonEgroup2_1->show();
      _pushButtonEgroup3_1->show();
      _pushButtonEgroup4_1->hide();
    }

  // GUI customization for the FromHost EC+IC+AUX group

  // Elink numbers for EC, IC, AUX
  int gbt = _gbtNr;
  if( _gbtNr == FLX_LINKS ) gbt = 0; // EMU link
  int elinknr = (gbt << BLOCK_LNK_SHIFT);
  _checkBoxEcFromHost->setText( QString( "EC (%1)" ).
                                arg(elinknr | _ecFromHostIndex,
                                    2, 16, QChar('0')) );
  _checkBoxIcFromHost->setText( QString( "IC (%1)" ).
                                arg(elinknr | _icFromHostIndex,
                                    2, 16, QChar('0')) );
  _checkBoxAuxFromHost->setText( QString( "AUX (%1)" ).
                                 arg(elinknr | _auxFromHostIndex,
                                     2, 16, QChar('0')) );

  // The EC/IC/AUX checkboxes
  uint32_t enables = cfg->enablesFromHost( FROMHOST_MINI_GROUP );
  disconnect( _checkBoxEcFromHost, SIGNAL(stateChanged(int)), 0, 0 );
  disconnect( _checkBoxIcFromHost, SIGNAL(stateChanged(int)), 0, 0 );
  disconnect( _checkBoxAuxFromHost, SIGNAL(stateChanged(int)), 0, 0 );
//#if REGMAP_VERSION < 0x500
  _checkBoxEcFromHost->setChecked( enables & EC_ENABLED );
  _checkBoxIcFromHost->setChecked( enables & IC_ENABLED );
  _checkBoxAuxFromHost->setChecked( enables & AUX_ENABLED );
  _frameEcFromHost->setAutoFillBackground( enables & EC_ENABLED );
  _frameIcFromHost->setAutoFillBackground( enables & IC_ENABLED );
  _frameAuxFromHost->setAutoFillBackground( enables & AUX_ENABLED );
//#else TO-BE-DEFINED
//  _checkBoxEcFromHost->setChecked( enables & 0x0001 );
//  _checkBoxIcFromHost->setChecked( enables & 0x0002 );
//  _checkBoxAuxFromHost->setChecked( enables & 0x0004 );
//  _frameEcFromHost->setAutoFillBackground( enables & 0x0001 );
//  _frameIcFromHost->setAutoFillBackground( enables & 0x0002 );
//  _frameAuxFromHost->setAutoFillBackground( enables & 0x0004 );
//#endif // REGMAP_VERSION
  // Reconnect
  connect( _checkBoxEcFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeEcFromHostEnable()) );
  connect( _checkBoxIcFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeIcFromHostEnable()) );
  connect( _checkBoxAuxFromHost, SIGNAL(stateChanged(int)),
           this, SLOT(changeAuxFromHostEnable()) );

  // The EC mode combobox
  uint64_t modes = cfg->modesFromHost( FROMHOST_MINI_GROUP );
  disconnect( _comboEcFromHostMode, SIGNAL(currentIndexChanged(int)), 0, 0 );
  if( _checkBoxEcFromHost->isChecked() )
//#if REGMAP_VERSION < 0x500
    _comboEcFromHostMode->setCurrentIndex((modes>>(EC_ENABLED_BIT*4)) & 0xFULL);
//#else
//    _comboEcFromHostMode->setCurrentIndex( modes& 0xFULL );
//#endif // REGMAP_VERSION
  else
    _comboEcFromHostMode->setCurrentIndex( -1 );
  if( _fwMode != FIRMW_STRIP || _radioButtonToHostDmaIndex->isChecked() )
    _comboEcFromHostMode->setEnabled( _checkBoxEcFromHost->isChecked() );
  // Reconnect
  connect( _comboEcFromHostMode, SIGNAL(currentIndexChanged(int)),
           this, SLOT(changeEcFromHostMode()) );

  // EMU link and LTI-TTC FromHost dependent customization
#if REGMAP_VERSION < 0x500
  bool lti_ttc = false;
#else
  bool lti_ttc = (linkmode == LINKMODE_FULL && cfg->ltiTtc());
#endif // REGMAP_VERSION
  if( _gbtNr == FLX_LINKS || lti_ttc )
    {
      // EMU link or LTI-TTC mode: hide EC/IC/AUX stuff
      _frameEcFromHost->hide();
      _frameIcFromHost->hide();
      if( !_frameAuxFromHost->isHidden() )
        {
          _frameAuxFromHost->hide();
          _auxVisible = true;
        }
    }
  else
    {
      // Other links: show EC/IC stuff
      if( linkmode != LINKMODE_FULL )
        {
          if( _auxVisible )
            _frameAuxFromHost->show();
        }
      _frameEcFromHost->show();
      _frameIcFromHost->show();
    }

  // LTI-TTC related GUI configuration
  if( linkmode == LINKMODE_FULL && _gbtNr != FLX_LINKS )
    {
#if REGMAP_VERSION < 0x500
      _frameLtiTtc->hide();
#else
      _frameLtiTtc->show();
      _frameLtiTtc->setAutoFillBackground( cfg->ltiTtc() );
      _checkBoxLtiTtc->setChecked( cfg->ltiTtc() );
      _pushButtonReplicateEgroupFromHost->setEnabled( !cfg->ltiTtc() );
      _pushButtonReplicateEgroupFromHostToAll->setEnabled( !cfg->ltiTtc() );
      _pushButtonEgroup0_1->setEnabled( !cfg->ltiTtc() );
      _pushButtonEgroup1_1->setEnabled( !cfg->ltiTtc() );
      _pushButtonEgroup2_1->setEnabled( !cfg->ltiTtc() );
      _pushButtonEgroup3_1->setEnabled( !cfg->ltiTtc() );
      _pushButtonEgroup4_1->setEnabled( !cfg->ltiTtc() );
#endif // REGMAP_VERSION
    }
  else
    {
      _frameLtiTtc->hide();
      _pushButtonReplicateEgroupFromHost->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonReplicateEgroupFromHostToAll->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonEgroup0_1->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonEgroup1_1->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonEgroup2_1->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonEgroup3_1->setEnabled( _gbtNr != FLX_LINKS );
      _pushButtonEgroup4_1->setEnabled( _gbtNr != FLX_LINKS );
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::applyEgroupConfiguration()
{
  // Map configuration to ToHost display panel
  this->displayToHostConfig();
  for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
    this->displayEgroupToHost( grp );
  this->displayEgroupToHostInfo( _egroupNrToHost );

  // Map configuration to FromHost display panel
  this->displayFromHostConfig();
  for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
    this->displayEgroupFromHost( grp );
  this->displayEgroupFromHostInfo( _egroupNrFromHost );
}

// ----------------------------------------------------------------------------

void ElinkConfig::selectedDeviceChanged( int index )
{
  // Indicate the selected FLX device's configuration 'read' status
  QFont f = _pushButtonRead->font();
  if( index >= 0 )
    f.setBold( true );  // Not yet read out
  else
    f.setBold( false ); // Read out
  _pushButtonRead->setFont( f );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeToHostEwidth( int group )
{
  // User selected a new E-link width to display for a particular E-group

  // Update the enables-mask according to this (new) onscreen E-path setting
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   linkmode = cfg->linkMode();
  uint32_t   enables  = cfg->enablesToHost( group );
  // The currently selected E-link width:
  int ewidth   = _egroupToHostWidgets[group].ewidthCombo->currentIndex();
  int disables = 0;
  if( linkmode != LINKMODE_FULL )
    {
#if REGMAP_VERSION < 0x500
      if( ewidth == 0 )      // 2-bit
        disables = 0x0007F;
      else if( ewidth == 1 ) // 4-bit
        disables = 0x07F87;
      else if( ewidth == 2 ) // 8-bit
        disables = 0x07FF9;
      else if( ewidth == 3 ) // 16-bit
        disables = 0x07FFE;
#else
      if( ewidth == 0 )      // 2-bit
        disables = 0x00;
      else if( ewidth == 1 ) // 4-bit
        {
          if( linkmode == LINKMODE_LPGBT5_F5 ||
              linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xF0;
          else
            disables = 0xAA;
        }
      else if( ewidth == 2 ) // 8-bit
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            disables = 0xF0;
          else if( linkmode == LINKMODE_LPGBT5_F5 ||
                   linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xFA;
          else
            disables = 0xEE;
        }
      else if( ewidth == 3 ) // 16-bit
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            disables = 0xFA;
          else if( linkmode == LINKMODE_LPGBT5_F5 ||
                   linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xFE;
          else
            disables = 0xFE;
        }
      else if( ewidth == 4 ) // 32-bit
        {
          disables = 0xFE;
        }

      // Apply width setting to GBT configuration
      cfg->setWidthToHost( group, ewidth );

      // For widths unequal to 2-bit change HDLC into non-HDLC, i.e. 8b10b
      uint64_t modes = cfg->modesToHost( group );
      if( ewidth != 0 )
        {
          for( int i=0; i<FLX_ELINK_PATHS; ++i )
            if( ((modes >> (i*4)) & 0xF) == 2 ) // HDLC
              {
                modes &= ~(0xF << (i*4));
                modes |= (1 << (i*4)); // 8b10b
              }
          cfg->setModesToHost( group, modes );
        }
      else if( _fwEgroupIncludes[group].TOHOST_02 == 0 )
        {
          // 2-bit, but only HDLC supported: change mode if necessary
          for( int i=0; i<FLX_ELINK_PATHS; ++i )
            if( ((modes >> (i*4)) & 0xF) == 1 ) // 8b10b
              {
                modes &= ~(0xF << (i*4));
                modes |= (2 << (i*4)); // HDLC
              }
          cfg->setModesToHost( group, modes );
        }
#endif // REGMAP_VERSION

      // Disable E-links in the E-group with a different width
      // (as this is not possible/allowed in GBT configurations)
      enables &= ((~disables) & 0x17FFF);
      cfg->setEnablesToHost( group, enables );
    }

  // Clear E-links aligned status for this link
  _fwElinksAligned[_gbtNr] = 0;

  // Update the GUI with the current link's settings
  // (for this E-group's width selection)
  this->displayEgroupToHost( group );

  // Make this the currently selected E-group
  this->changeEgroupToHost( group );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeFromHostEwidth( int group )
{
  // User selected a new E-link width to display for a particular E-group

  // Update the enables-mask according to this (new) onscreen E-path setting
  GbtConfig *cfg     = &_gbtConfig[_gbtNr];
  uint32_t   enables = cfg->enablesFromHost( group );

  // The currently selected E-link width:
  int ewidth   = _egroupFromHostWidgets[group].ewidthCombo->currentIndex();
  int disables = 0;
#if REGMAP_VERSION < 0x500
  if( ewidth == 0 )      // 2-bit
    disables = 0x0007F;
  else if( ewidth == 1 ) // 4-bit
    disables = 0x07F87;
  else if( ewidth == 2 ) // 8-bit
    disables = 0x07FF9;
  else if( ewidth == 3 ) // 16-bit
    disables = 0x07FFE;
#else
  if( ewidth == 0 )      // 2-bit
    disables = 0x00;
  else if( ewidth == 1 ) // 4-bit
    disables = 0xAA;
  else if( ewidth == 2 ) // 8-bit
    disables = 0xEE;
  else if( ewidth == 3 ) // 16-bit
    disables = 0xFE;

  uint32_t linkmode = cfg->linkMode();
  if( linkmode == LINKMODE_LPGBT10_F5 ||
      linkmode == LINKMODE_LPGBT5_F5 ||
      linkmode == LINKMODE_LPGBT10_F12 ||
      linkmode == LINKMODE_LPGBT5_F12 )
    // FromHost E-groups are only 8 bits wide, i.e. 4 paths
    // NB: STRIP firmware has 5 FromHost paths/(virtual) E-links, hence 0xE0
    //disables |= 0xF0;
    disables |= 0xE0;

  // Apply width setting to GBT configuration
  cfg->setWidthFromHost( group, ewidth );

  // For widths unequal to 2-bit change HDLC into non-HDLC, i.e. 8b10b
  uint64_t modes = cfg->modesFromHost( group );
  if( ewidth != 0 )
    {
      for( int i=0; i<FLX_ELINK_PATHS; ++i )
        if( ((modes >> (i*4)) & 0xF) == 2 ) // HDLC
          {
            modes &= ~(0xF << (i*4));
            modes |= (1 << (i*4)); // 8b10b
          }
      cfg->setModesFromHost( group, modes );
    }
  else if( _fwEgroupIncludes[group].FROMHOST_02 == 0 )
    {
      // 2-bit, but only HDLC supported: change mode if necessary
      for( int i=0; i<FLX_ELINK_PATHS; ++i )
        if( ((modes >> (i*4)) & 0xF) == 1 ) // 8b10b
          {
            modes &= ~(0xF << (i*4));
            modes |= (2 << (i*4)); // HDLC
          }
      cfg->setModesFromHost( group, modes );
    }
#endif // REGMAP_VERSION

  // Disable E-links in the E-group with a different width
  // (as this is not possible/allowed in GBT configurations)
  enables &= ((~disables) & 0x17FFF);
  cfg->setEnablesFromHost( group, enables );

  // Update the GUI with the current link's settings
  // (for this E-group's width selection)
  this->displayEgroupFromHost( group );

  // Make this the currently selected E-group
  this->changeEgroupFromHost( group );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeFromHostTtcOption( int group )
{
#if REGMAP_VERSION >= 0x500
  // User selected a new TTC option value for a particular E-group

  // Update the TTC option value
  GbtConfig *cfg = &_gbtConfig[_gbtNr];
  int ttc_option = _egroupFromHostWidgets[group].ttcOptCombo->currentIndex();
  cfg->setTtcOptionFromHost( group, ttc_option );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeToHostEnable( int index )
{
  // User clicked to enable or disable an E-link (actually E-path)
  // in a particular E-group
  int grp  = index/8;
  int path = (index & 7);

  uint32_t linkmode = _gbtConfig[_gbtNr].linkMode();
  uint32_t enables  = _gbtConfig[_gbtNr].enablesToHost( grp );
  uint64_t modes    = _gbtConfig[_gbtNr].modesToHost( grp );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t mode_word = GbtConfig::epath4BitModeWord( enables, modes );
  uint32_t dma_indices = 0;
#else
  uint32_t mode_word   = modes;
  uint32_t dma_indices = _gbtConfig[_gbtNr].dmaIndices( grp );
#endif // REGMAP_VERSION

  epathWidget_t e = _egroupToHostWidgets[grp].epaths[path];

  // Clear E-links aligned status for this link
  _fwElinksAligned[_gbtNr] = 0;
  QFont f = e.enableCheck->font();
  f.setUnderline( false );
  e.enableCheck->setFont( f );

  // Update display of the link mode of this E-path (i.e. show or hide)
  if( e.enableCheck->isChecked() )
    {
      //e.modeCombo->setCurrentIndex( (mode_word>>(path*4)) & 0xF );

      // Adjust selected mode if necessary, i.e. when not available in firmware
      // (reflected by a disabled combobox item)
      int index;
      if( _radioButtonToHostEmode->isChecked() )
        index = (mode_word >> (path*4)) & 0xF;
      else
        index = (dma_indices >> (path*4)) & 0xF;
      if( !this->comboItemEnabled( e.modeCombo, index ) )
        {
          // Set to the first available mode
          for( int i=0; i<e.modeCombo->count(); ++i )
            if( this->comboItemEnabled( e.modeCombo, i ) )
              {
                e.modeCombo->setCurrentIndex( i );
                break;
              }
        }
      else
        {
          e.modeCombo->setCurrentIndex( index );
        }
    }
  else
    {
      // No selection shown
      e.modeCombo->setCurrentIndex( -1 );
    }

  // Update color highlighting
  e.frame->setAutoFillBackground( e.enableCheck->isChecked() );

  // Update the enables-mask according to this (new) onscreen E-path setting
  if( linkmode != LINKMODE_FULL )
    {
      int bitmask  = 0;
      int disables = 0;
#if REGMAP_VERSION < 0x500
      // The currently selected E-link width:
      int ewidth = _egroupToHostWidgets[grp].ewidthCombo->currentIndex();
      if( ewidth == 0 ) // 2-bit
        {
          bitmask = 1 << (path + 7);
          disables = 0x0007F;
        }
      else if( ewidth == 1 ) // 4-bit
        {
          if( path == 6 ) bitmask = 0x00040;
          if( path == 4 ) bitmask = 0x00020;
          if( path == 2 ) bitmask = 0x00010;
          if( path == 0 ) bitmask = 0x00008;
          disables = 0x07F87;
        }
      else if( ewidth == 2 ) // 8-bit
        {
          if( path == 5 ) bitmask = 0x00004;
          if( path == 1 ) bitmask = 0x00002;
          disables = 0x07FF9;
        }
      else if( ewidth == 3 ) // 16-bit
        {
          if( path == 3 ) bitmask = 0x00001;
          disables = 0x07FFE;
        }
#else
      int ewidth = _gbtConfig[_gbtNr].widthToHost( grp );
      bitmask = (1 << path);
      if( ewidth == 0 )      // 2-bit
        disables = 0x00;
      else if( ewidth == 1 ) // 4-bit
        {
          if( linkmode == LINKMODE_LPGBT5_F5 ||
              linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xF0;
          else
            disables = 0xAA;
        }
      else if( ewidth == 2 ) // 8-bit
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            disables = 0xF0;
          else if( linkmode == LINKMODE_LPGBT5_F5 ||
                   linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xFA;
          else
            disables = 0xEE;
        }
      else if( ewidth == 3 ) // 16-bit
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            disables = 0xFA;
          else if( linkmode == LINKMODE_LPGBT5_F5 ||
                   linkmode == LINKMODE_LPGBT5_F12 )
            disables = 0xFE;
          else
            disables = 0xFE;
        }
      else if( ewidth == 4 ) // 32-bit
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            disables = 0xFE;
          else
            disables = 0xFF;
        }
#endif // REGMAP_VERSION

      // Enable or disable the E-link in question
      if( e.enableCheck->isChecked() )
        enables |= bitmask;
      else
        enables &= ((~bitmask) & 0x17FFF);

      // Disable E-links in the E-group with a different width
      // (as this is not possible/allowed in GBT configurations)
      enables &= ((~disables) & 0x17FFF);
    }
  else
    {
      // FULL mode
      if( path == 0 && e.enableCheck->isChecked() )
        enables |= FULL_ENABLED;
      else
        enables &= 0x0FFFF;
      // No need to disable other E-links because in FULLmode
      // only bit 16 (0x10000, FULL_ENABLED) in the enables word is considered
    }
  _gbtConfig[_gbtNr].setEnablesToHost( grp, enables );

  // Make this the currently selected E-group
  this->changeEgroupToHost( grp );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeFromHostEnable( int index )
{
  // User clicked to enable or disable an E-link (actually E-path)
  // in a particular E-group
  int grp  = index/8;
  int path = (index & 7);

  uint32_t enables = _gbtConfig[_gbtNr].enablesFromHost( grp );
  uint64_t modes   = _gbtConfig[_gbtNr].modesFromHost( grp );
#if REGMAP_VERSION < 0x500
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t mode_word = GbtConfig::epath4BitModeWord( enables, modes );
#else
  uint32_t mode_word = modes;
#endif // REGMAP_VERSION

  epathWidget_t e = _egroupFromHostWidgets[grp].epaths[path];

  // Update display of the link mode of this E-path (i.e. show or hide)
  if( e.enableCheck->isChecked() )
    {
      //e.modeCombo->setCurrentIndex( (mode_word>>(path*4)) & 0xF );

      // Adjust selected mode if necessary, i.e. when not available in firmware
      // (reflected by a disabled combobox item)
      int mode_i = (mode_word >> (path*4)) & 0xF;
      if( !this->comboItemEnabled( e.modeCombo, mode_i ) )
        {
          // Set to the first available mode
          for( int i=0; i<e.modeCombo->count(); ++i )
            if( this->comboItemEnabled( e.modeCombo, i ) )
              {
                e.modeCombo->setCurrentIndex( i );
                this->changeFromHostMode( index );
                break;
              }
        }
      else
        {
          e.modeCombo->setCurrentIndex( mode_i );

          if( _fwMode == FIRMW_STRIP )
            {
              // Special: select E-link color according to 'swapped' state
              if( (_itkStripLcbR3l1ElinkSwap & (1L<<((_gbtNr<<4) + grp))) != 0 )
                e.modeCombo->setItemText( e.modeCombo->currentIndex(),
                                          STRIP_ELINK_STR_SWAPPED[path] );
              else
                e.modeCombo->setItemText( e.modeCombo->currentIndex(),
                                          STRIP_ELINK_STR[path] );

              QString str = e.modeCombo->itemText( e.modeCombo->currentIndex() );
              if( str.contains("R-") || str.contains("L*") )
                {
                  // Use the combo text to distinguish STRIP firmware swapped
                  // (names with '*') and non-swapped E-links (names with '-')
                  // and change E-link color accordingly
                  QPalette qp = e.frame->palette();
                  qp.setColor( QPalette::Window, STRIP_ELINK_COLOR );
                  e.frame->setPalette( qp );
                }
            }
        }
    }
  else
    {
      // No selection shown
      e.modeCombo->setCurrentIndex( -1 );
    }

  // Update color highlighting
  e.frame->setAutoFillBackground( e.enableCheck->isChecked() );

  // Update the enables-mask according to this (new) onscreen E-path setting

  int bitmask  = 0;
  int disables = 0;
#if REGMAP_VERSION < 0x500
  // The currently selected E-link width:
  int ewidth   = _egroupFromHostWidgets[grp].ewidthCombo->currentIndex();
  if( ewidth == 0 ) // 2-bit
    {
      bitmask = 1 << (path + 7);
      disables = 0x0007F;
    }
  else if( ewidth == 1 ) // 4-bit
    {
      if( path == 6 ) bitmask = 0x00040;
      if( path == 4 ) bitmask = 0x00020;
      if( path == 2 ) bitmask = 0x00010;
      if( path == 0 ) bitmask = 0x00008;
      disables = 0x07F87;
    }
  else if( ewidth == 2 ) // 8-bit
    {
      if( path == 5 ) bitmask = 0x00004;
      if( path == 1 ) bitmask = 0x00002;
      disables = 0x07FF9;
    }
  else if( ewidth == 3 ) // 16-bit
    {
      if( path == 3 ) bitmask = 0x00001;
      disables = 0x07FFE;
    }
#else
  int ewidth = _gbtConfig[_gbtNr].widthFromHost( grp );
  bitmask = (1 << path);
  if( ewidth == 0 )      // 2-bit
    disables = 0x00;
  else if( ewidth == 1 ) // 4-bit
    disables = 0xAA;
  else if( ewidth == 2 ) // 8-bit
    disables = 0xEE;
  else if( ewidth == 3 ) // 16-bit
    disables = 0xFE;
#endif // REGMAP_VERSION

  // Enable or disable the E-link in question
  if( e.enableCheck->isChecked() )
    enables |= bitmask;
  else
    enables &= ((~bitmask) & 0x17FFF);

  // Disable E-links in the E-group with a different width
  // (as this is not possible/allowed in GBT configurations)
  enables &= ((~disables) & 0x17FFF);

  _gbtConfig[_gbtNr].setEnablesFromHost( grp, enables );

  // Make this the currently selected E-group
  this->changeEgroupFromHost( grp );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeToHostMode( int index )
{
  // User selected a mode (or DMA index) for an E-link (actually E-path)
  // in a particular E-group
  int grp  = index/8;
  int path = (index & 7);
  epathWidget_t e = _egroupToHostWidgets[grp].epaths[path];

  // Don't set mode/index when nothing is selected
  if( e.modeCombo->currentIndex() == -1 ) return;

  // Clear E-links aligned status for the link
  _fwElinksAligned[_gbtNr] = 0;
  QFont f = e.enableCheck->font();
  f.setUnderline( false );
  e.enableCheck->setFont( f );

  // Store the selected mode or DMA index
  int m = e.modeCombo->currentIndex() & 0xF;
  if( _radioButtonToHostEmode->isChecked() )
    {
      uint64_t modes = _gbtConfig[_gbtNr].modesToHost( grp );
#if REGMAP_VERSION < 0x500
      uint32_t enables = _gbtConfig[_gbtNr].enablesToHost( grp );
      // Convert from mode per EPROC to (4-bit) mode per E-path
      uint32_t mode_word = GbtConfig::epath4BitModeWord( enables, modes );
      mode_word &= ~((uint64_t) 0xF << (path*4));
      mode_word |=  ((uint64_t) m   << (path*4));
      // Convert from (4-bit) mode per E-path back to mode per EPROC
      modes = GbtConfig::epath4BitModeWordToModes( mode_word );
#else
      modes &= ~((uint64_t) 0xF << (path*4));
      modes |=  ((uint64_t) m   << (path*4));
#endif // REGMAP_VERSION
      _gbtConfig[_gbtNr].setModesToHost( grp, modes );
    }
  else
    {
#if REGMAP_VERSION >= 0x500
      uint32_t dma_indices = _gbtConfig[_gbtNr].dmaIndices( grp );
      dma_indices &= ~((uint64_t) 0xF << (path*4));
      dma_indices |=  ((uint64_t) m   << (path*4));
      _gbtConfig[_gbtNr].setDmaIndices( grp, dma_indices );
#endif // REGMAP_VERSION
    }

  // Make this the currently selected E-group
  this->changeEgroupToHost( grp );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeFromHostMode( int index )
{
  // User selected a mode for an E-link (actually E-path)
  // in a particular E-group
  int grp  = index/8;
  int path = (index & 7);
  epathWidget_t e = _egroupFromHostWidgets[grp].epaths[path];

  // Don't set mode when nothing is selected
  if( e.modeCombo->currentIndex() == -1 ) return;

  // Store the selected mode
  int m = e.modeCombo->currentIndex() & 0xF;
  uint64_t modes = _gbtConfig[_gbtNr].modesFromHost( grp );
#if REGMAP_VERSION < 0x500
  uint32_t enables = _gbtConfig[_gbtNr].enablesFromHost( grp );
  // Convert from mode per EPROC to (4-bit) mode per E-path
  uint32_t mode_word = GbtConfig::epath4BitModeWord( enables, modes );
  mode_word &= ~((uint64_t) 0xF << (path*4)); // Clear mode
  mode_word |=  ((uint64_t) m   << (path*4)); // Set mode
  // Convert from mode per E-path back to mode per EPROC
  modes = GbtConfig::epath4BitModeWordToModes( mode_word );
#else
  modes &= ~((uint64_t) 0xF << (path*4)); // Clear mode
  modes |=  ((uint64_t) m   << (path*4)); // Set mode
#endif // REGMAP_VERSION
  _gbtConfig[_gbtNr].setModesFromHost( grp, modes );

  // Make this the currently selected E-group
  this->changeEgroupFromHost( grp );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEgroupToHost( int index )
{
  _egroupNrToHost = index;
  this->displayEgroupToHostInfo( _egroupNrToHost );
  _egroupToHostButtons[index]->setChecked( true );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEgroupFromHost( int index )
{
  _egroupNrFromHost = index;
  this->displayEgroupFromHostInfo( _egroupNrFromHost );
  _egroupFromHostButtons[index]->setChecked( true );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEcToHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesToHost( TOHOST_MINI_GROUP );
  if( _checkBoxEcToHost->isChecked() )
    enables |= EC_ENABLED;
  else
    enables &= ~EC_ENABLED;
  _gbtConfig[_gbtNr].setEnablesToHost( TOHOST_MINI_GROUP, enables );

  // Clear E-links aligned status for the link
  _fwElinksAligned[_gbtNr] = 0;

  // Update display
  this->displayToHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEcFromHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesFromHost( FROMHOST_MINI_GROUP );
  if( _checkBoxEcFromHost->isChecked() )
    enables |= EC_ENABLED;
  else
    enables &= ~EC_ENABLED;
  _gbtConfig[_gbtNr].setEnablesFromHost( FROMHOST_MINI_GROUP, enables );

  // Update display
  this->displayFromHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEcToHostMode()
{
  int cb = _comboEcToHostMode->currentIndex();
  if( cb == -1 ) return; // Prevent setting mode when nothing is selected
  if( _radioButtonToHostEmode->isChecked() )
    {
      uint64_t modes = _gbtConfig[_gbtNr].modesToHost( TOHOST_MINI_GROUP );
      modes &= ~((uint64_t)0xF << (EC_ENABLED_BIT*4));
      modes |=  ((uint64_t) cb << (EC_ENABLED_BIT*4));
      _gbtConfig[_gbtNr].setModesToHost( TOHOST_MINI_GROUP, modes );
    }
  else
    {
#if REGMAP_VERSION >= 0x500
      uint32_t dma_indices = _gbtConfig[_gbtNr].dmaIndices( TOHOST_MINI_GROUP );
      dma_indices &= ~((uint64_t)0xF << (7*4));
      dma_indices |=  ((uint64_t) cb << (7*4));
      _gbtConfig[_gbtNr].setDmaIndices( TOHOST_MINI_GROUP, dma_indices );
#endif // REGMAP_VERSION
    }

  // Clear E-links aligned status for the link
  _fwElinksAligned[_gbtNr] = 0;

  // Update display
  this->displayToHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeEcFromHostMode()
{
  int cb = _comboEcFromHostMode->currentIndex();
  if( cb == -1 ) return;  // Prevent setting mode when nothing is selected
  uint64_t modes = _gbtConfig[_gbtNr].modesFromHost( FROMHOST_MINI_GROUP );
  modes &= ~((uint64_t)0xF << (EC_ENABLED_BIT*4));
  modes |=  ((uint64_t) cb << (EC_ENABLED_BIT*4));
  _gbtConfig[_gbtNr].setModesFromHost( FROMHOST_MINI_GROUP, modes );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeIcToHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesToHost( TOHOST_MINI_GROUP );
  if( _checkBoxIcToHost->isChecked() )
    enables |= IC_ENABLED;
  else
    enables &= ~IC_ENABLED;
  _gbtConfig[_gbtNr].setEnablesToHost( TOHOST_MINI_GROUP, enables );

  // Clear E-links aligned status for the link
  _fwElinksAligned[_gbtNr] = 0;

  // Update display
  this->displayToHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeIcFromHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesFromHost( FROMHOST_MINI_GROUP );
  if( _checkBoxIcFromHost->isChecked() )
    enables |= IC_ENABLED;
  else
    enables &= ~IC_ENABLED;
  _gbtConfig[_gbtNr].setEnablesFromHost( FROMHOST_MINI_GROUP, enables );

  // Update display
  this->displayFromHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeIcToHostDmaIndex()
{
  int cb = _comboIcToHostDmaIndex->currentIndex();
  if( cb == -1 ) return; // Prevent setting mode when nothing is selected
  if( _radioButtonToHostDmaIndex->isChecked() )
    {
#if REGMAP_VERSION >= 0x500
      uint32_t dma_indices = _gbtConfig[_gbtNr].dmaIndices( TOHOST_MINI_GROUP );
      dma_indices &= ~((uint64_t)0xF << (6*4));
      dma_indices |=  ((uint64_t) cb << (6*4));
      _gbtConfig[_gbtNr].setDmaIndices( TOHOST_MINI_GROUP, dma_indices );
#endif // REGMAP_VERSION
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeAuxToHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesToHost( TOHOST_MINI_GROUP );
  if( _checkBoxAuxToHost->isChecked() )
    enables |= AUX_ENABLED;
  else
    enables &= ~AUX_ENABLED;
  _gbtConfig[_gbtNr].setEnablesToHost( TOHOST_MINI_GROUP, enables );

  // Update display
  this->displayToHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeAuxFromHostEnable()
{
  uint32_t enables = _gbtConfig[_gbtNr].enablesFromHost( FROMHOST_MINI_GROUP );
  if( _checkBoxAuxFromHost->isChecked() )
    enables |= AUX_ENABLED;
  else
    enables &= ~AUX_ENABLED;
  _gbtConfig[_gbtNr].setEnablesFromHost( FROMHOST_MINI_GROUP, enables );

  // Update display
  this->displayFromHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeTtc2HostEnable()
{
  uint32_t enables = _gbtConfig[0].enablesToHost( TOHOST_MINI_GROUP );
  if( _checkBoxTtc2Host->isChecked() )
    enables |= TTC2H_ENABLED;
  else
    enables &= ~TTC2H_ENABLED;
  _gbtConfig[0].setEnablesToHost( TOHOST_MINI_GROUP, enables );

  // Update display
  _frameTtc2Host->setAutoFillBackground( enables & TTC2H_ENABLED );

  // DMA index combo
  if( _radioButtonToHostDmaIndex->isChecked() &&
      _checkBoxTtc2Host->isChecked() )
    {
#if REGMAP_VERSION >= 0x500
      uint32_t dma_indices = _gbtConfig[0].dmaIndices( TOHOST_MINI_GROUP );
      _comboTtc2HostDmaIndex->setCurrentIndex( dma_indices & 0xF );
#endif // REGMAP_VERSION
      _comboTtc2HostDmaIndex->show();
    }
  else
    {
      _comboTtc2HostDmaIndex->hide();
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeTtc2HostDmaIndex()
{
  int cb = _comboTtc2HostDmaIndex->currentIndex();
  if( cb == -1 ) return; // Prevent setting mode when nothing is selected
  if( _radioButtonToHostDmaIndex->isChecked() )
    {
#if REGMAP_VERSION >= 0x500
      uint32_t dma_indices = _gbtConfig[0].dmaIndices( TOHOST_MINI_GROUP );
      dma_indices &= ~((uint64_t)0xF << (0*4));
      dma_indices |=  ((uint64_t) cb << (0*4));
      _gbtConfig[0].setDmaIndices( TOHOST_MINI_GROUP, dma_indices );
#endif // REGMAP_VERSION
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeTtcClock()
{
  _gbtConfig[0].setTtcClock( _checkBoxTtcClock->isChecked() );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeFromHostLtiTtc()
{
#if REGMAP_VERSION >= 0x500
  _gbtConfig[_gbtNr].setLtiTtc( _checkBoxLtiTtc->isChecked() );
#endif // REGMAP_VERSION

  // Update display
  for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
    displayEgroupFromHost( grp );
  this->displayFromHostConfig();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeLink( int index )
{
  if( index == -1 ) // Index (invisible) used to indicate the Emulator config
    {
      _gbtNr = FLX_LINKS; // Index where we store the Emulator configuration
      _labelEmu->show();

      _labelToHost->setText( "ToHost" );
      _labelFromHost->setText( "FromHost" );

      // For EMU link: hide all FromHost E-group widgets:
      // done in applyEgroupConfiguration()->displayEgroupFromHost()
    }
  else
    {
      _gbtNr = index;
      _labelEmu->hide();

      _labelToHost->setText( QString("ToHost Link %1").arg( index ) );
      _labelFromHost->setText( QString("FromHost Link %1").arg( index ) );

      // For non-EMU link: show FromHost E-group widgets again:
      // done in applyEgroupConfiguration()->displayEgroupFromHost()
    }

  this->applyEgroupConfiguration();
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeLinkMode()
{
  // Initialize firmware configuration as 'unread' (i.e. all features enabled)
  this->initFwParameters();

  // Update display
  this->applyLinkMode();

  // Re-enable any Egroup widget combobox items disabled
  // because of the previously set/read firmware configuration
  this->reEnableGuiItems();

  // Indicate the currently selected FLX device's configuration is 'unread'
  this->selectedDeviceChanged( 0 );

  // Make E-group 0 the currently selected E-group
  this->changeEgroupToHost( 0 );
  this->changeEgroupFromHost( 0 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::applyLinkMode( bool init )
{
  // Applies to all GBT links, including EMU

  if( init )
    {
      // Initialize firmware type when changing link mode
      if( _radioButtonNormalMode->isChecked() )
        _fwMode = FIRMW_GBT;
      else if( _radioButtonWideMode->isChecked() )
        _fwMode = FIRMW_GBT;
      else if( _radioButtonFullMode->isChecked() )
        _fwMode = FIRMW_FULL;
      else if( _radioButtonLpGbtMode->isChecked() )
        _fwMode = FIRMW_LPGBT;
      else
        _fwMode = FIRMW_GBT;

      // All 24 links for LTDB only, otherwise 12
      if( _fwMode == FIRMW_LTDB )
        _spinBoxLinkNr->setMaximum( FLX_LINKS - 1 );
      else
        _spinBoxLinkNr->setMaximum( FLX_LINKS/2 - 1 );

      // Initialize default EC/IC e-link numbers
      if( _radioButtonLpGbtMode->isChecked() )
        {
          _ecToHostIndex    = 28;
          _ecFromHostIndex  = 16;
          _icToHostIndex    = 29;
          _icFromHostIndex  = 17;
          _auxToHostIndex   = 30;
          _auxFromHostIndex = 18;
        }
      else
        {
#if REGMAP_VERSION < 0x500
          _ecToHostIndex    = 0x3F;
          _ecFromHostIndex  = 0x3F;
          _icToHostIndex    = 0x3E;
          _icFromHostIndex  = 0x3E;
          _auxToHostIndex   = 0x3D;
          _auxFromHostIndex = 0x3D;
#else
          _ecToHostIndex    = 40;
          _ecFromHostIndex  = 40;
          _icToHostIndex    = 41;
          _icFromHostIndex  = 41;
          _auxToHostIndex   = 42;
          _auxFromHostIndex = 42;
#endif // REGMAP_VERSION
        }
    }

  // Display tweaks according to firmware type
  if( _fwMode == FIRMW_LTDB )
    _radioButtonNormalMode->setText( "GBT (LTDB)" );
  else if( _fwMode == FIRMW_FEI4 )
    _radioButtonNormalMode->setText( "GBT (FEI4)" );
  else
    _radioButtonNormalMode->setText( "GBT" );
  if( _fwMode == FIRMW_LTDB )
    {
      _frameAuxToHost->show();
      _frameAuxFromHost->show();
    }
  else
    {
      _frameAuxToHost->hide();
      _frameAuxFromHost->hide();
      _auxVisible = false;
    }

#if REGMAP_VERSION >= 0x500
  // Show E-link specific buttons
  _radioButtonToHostEmode->show();
  _radioButtonToHostDmaIndex->show();
#endif // REGMAP_VERSION

  // Link mode
  if( _radioButtonNormalMode->isChecked() )
    {
      // GBT
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        _gbtConfig[gbt].setLinkMode( LINKMODE_GBT );

      for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
        _egroupToHostWidgets[grp].enabled = (grp < 5);

      for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
        _egroupFromHostWidgets[grp].enabled = (grp < 5);

      _comboLpGbtMode->hide();
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT" );
    }
  else if( _radioButtonWideMode->isChecked() )
    {
      // GBT Wide
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        _gbtConfig[gbt].setLinkMode( LINKMODE_GBTWIDE );

      for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
        _egroupToHostWidgets[grp].enabled = (grp < 7);

      for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
        _egroupFromHostWidgets[grp].enabled = (grp < 3);

      _comboLpGbtMode->hide();
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT" );
    }
  else if( _radioButtonFullMode->isChecked() )
    {
      // FULL mode
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        {
          _gbtConfig[gbt].setLinkMode( LINKMODE_FULL );
          // Don't change current configuration,
          // so the following is outcommented:
          //uint32_t enables = _gbtConfig[gbt].enablesToHost( 0 );
          //enables |= FULL_ENABLED; // FULL mode link default enabled
          //_gbtConfig[gbt].setEnablesToHost( 0, enables );
          //enables = 0x00000; // TTC-to-Host link default disabled
          //_gbtConfig[gbt].setEnablesToHost( TOHOST_MINI_GROUP, enables );
        }

      this->changeEgroupToHost( 0 );

      // Hide all ToHost Egroups except 0
      for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
        _egroupToHostWidgets[grp].enabled = (grp < 1);

      for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
        _egroupFromHostWidgets[grp].enabled = (grp < 5);

      // Hide E-link specific buttons
      //_radioButtonToHostEmode->hide();
      //_radioButtonToHostDmaIndex->hide();

      _comboLpGbtMode->hide();
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT" );
    }
  else if( _radioButtonLpGbtMode->isChecked() )
    {
      _comboLpGbtMode->show();

      // Select an lpGBT type
      int linkmode;
      int lpgbt_mode_index = _comboLpGbtMode->currentIndex();
      if( lpgbt_mode_index == 0 )
        linkmode = LINKMODE_LPGBT10_F5;
      else if( lpgbt_mode_index == 1 )
        linkmode = LINKMODE_LPGBT5_F5;
      else if( lpgbt_mode_index == 2 )
        linkmode = LINKMODE_LPGBT10_F12;
      else if( lpgbt_mode_index == 3 )
        linkmode = LINKMODE_LPGBT5_F12;
      else
        linkmode = LINKMODE_LPGBT10_F5;
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        _gbtConfig[gbt].setLinkMode( linkmode );

      // Update lpGBT tooltip
      if( linkmode == LINKMODE_LPGBT10_F5 )
        _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10.24Gbps-FEC5" );
      else if( linkmode == LINKMODE_LPGBT5_F5 )
        _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5.12Gbps-FEC5" );
      else if( linkmode == LINKMODE_LPGBT10_F12 )
        _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10.24Gbps-FEC12" );
      else if( linkmode == LINKMODE_LPGBT5_F12 )
        _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5.12Gbps-FEC12" );

      // 7 groups for FEC5 uplink frame structure, 6 for FEC12
      for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
        if( linkmode == LINKMODE_LPGBT10_F5 ||
            linkmode == LINKMODE_LPGBT5_F5 )
          _egroupToHostWidgets[grp].enabled = (grp < 7);
        else
          _egroupToHostWidgets[grp].enabled = (grp < 6);

      // 4 groups (of 8 bits each) for downlink frames
      for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
        _egroupFromHostWidgets[grp].enabled = (grp < 4);

      // Modify ToHost E-link width selection
      for( egroupWidget_t widget : _egroupToHostWidgets )
        {
          QComboBox *cb = widget.ewidthCombo;
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 )
            {
              this->setComboItemEnabled( cb, 0, false ); // 2-bit
              this->setComboItemEnabled( cb, 1, false ); // 4-bit
              if( cb->count() < 5 )
                cb->addItem( " 32-bit" );
              this->setComboItemEnabled( cb, 4, true ); // 32-bit
            }
          else if( linkmode == LINKMODE_LPGBT5_F5 ||
                   linkmode == LINKMODE_LPGBT5_F12 )
            {
              this->setComboItemEnabled( cb, 0, false ); // 2-bit
              this->setComboItemEnabled( cb, 1, true );  // 4-bit
              if( cb->count() > 4 )
                cb->removeItem( 4 ); // 32-bit
            }
        }

      // Modify FromHost E-link width selection
      for( egroupWidget_t widget : _egroupFromHostWidgets )
        {
          QComboBox *cb = widget.ewidthCombo;
          cb->removeItem( 3 ); // 16-bit
        }
    }

  if( !_radioButtonLpGbtMode->isChecked() )
    {
      // Adjust to non-lpGBT-specific items

      // Remove ToHost 32-bit width option (if present) for non-lpGBT,
      // reenable 2-bit and 4-bit widths
      for( egroupWidget_t widget : _egroupToHostWidgets )
        {
          QComboBox *cb = widget.ewidthCombo;
          cb->removeItem( 4 );

          this->setComboItemEnabled( cb, 0, true ); // 2-bit
          this->setComboItemEnabled( cb, 1, true ); // 4-bit
        }

      // Reinsert 16-bit FromHost width if required
      for( egroupWidget_t widget : _egroupFromHostWidgets )
        {
          QComboBox *cb = widget.ewidthCombo;
          if( cb->count() < 4 )
            cb->addItem( " 16-bit" );
        }
    }

  // TTCtoHost virtual E-link
  uint32_t enables = _gbtConfig[0].enablesToHost( TOHOST_MINI_GROUP );
  _checkBoxTtc2Host->setChecked( enables & TTC2H_ENABLED );
  _frameTtc2Host->setAutoFillBackground( enables & TTC2H_ENABLED );

  _checkBoxTtcClock->setChecked( _gbtConfig[0].ttcClock() );

  this->showAdvancedOptions( _checkBoxAdvanced->isChecked() );

  if( _radioButtonFullMode->isChecked() )
    {
      labelMaxChunk->hide();
      _checkBoxHdlcTruncation->hide();
    }
  else
    {
      labelMaxChunk->show();
      _checkBoxHdlcTruncation->show();
    }

  if( _stripsImage )
    _stripsImage->hide();

  this->applyEgroupConfiguration();

  // Make E-group 0 the currently selected E-group
  this->changeEgroupToHost( 0 );
  this->changeEgroupFromHost( 0 );

  QCoreApplication::processEvents();
  this->resize( 0, this->size().height() ); // Squeeze window horizontally
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeMaxChunk( int index )
{
  GbtConfig *cfg = &_gbtConfig[_gbtNr];
  cfg->setMaxChunkSize( index, _spins[index]->value() );
  if( index == 0 )
    _checkBoxHdlcTruncation->setChecked( _spins[index]->value() > 0 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeHdlcTruncation()
{
  GbtConfig *cfg = &_gbtConfig[_gbtNr];
  if( _checkBoxHdlcTruncation->isChecked() )
    {
      if( _spins[0]->value() == 0 )
        {
          cfg->setMaxChunkWord( 3584 );
          _spins[0]->setValue( 3584 );
        }
    }
  else
    {
      cfg->setMaxChunkWord( 0 );
      for( QSpinBox *s: _spins )
        s->setValue( 0 );
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeStreamBit( int index )
{
  GbtConfig *cfg = &_gbtConfig[_gbtNr];
  uint32_t streamid = cfg->streamIdBits( _egroupNrToHost );
  if( _streamChecks[index]->isChecked() )
    streamid |= (1 << index);
  else
    streamid &= ~(1 << index);
  cfg->setStreamIdBits( _egroupNrToHost, streamid );
}

// ----------------------------------------------------------------------------

void ElinkConfig::changeToHostDisplay()
{
  // Change ToHost E-group display from mode to DMA or vice versa

  // This is the ToHost part of the applyEgroupConfiguration() call:
  // Map configuration to ToHost display panel
  this->displayToHostConfig();
  for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
    this->displayEgroupToHost( grp );
  this->displayEgroupToHostInfo( _egroupNrToHost );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateEgroupToHost()
{
  // Do not include the EC+IC+TTC group
  if( _egroupNrToHost == TOHOST_MINI_GROUP )
    return;

  uint32_t selection_mask;
  QString title = QString("Replicate ToHost Egroup %1 "
                          "to Egroups:").arg(_egroupNrToHost);
  int groups = FLX_TOHOST_GROUPS-1;
  if( _gbtConfig[_gbtNr].linkMode() == LINKMODE_GBT )
    groups -= 2; // No Egroups 5+6
  ReplicateDialog diag( this, _egroupNrToHost, groups,
                        &selection_mask, title );

  if( diag.exec() == QDialog::Rejected || selection_mask == 0 )
    return;

  // Do not include the EC+IC+TTC group
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   enables  = cfg->enablesToHost( _egroupNrToHost );
#if REGMAP_VERSION >= 0x500
  uint32_t   width_code  = cfg->widthToHost( _egroupNrToHost );
  uint32_t   dma_indices = cfg->dmaIndices( _egroupNrToHost );
#endif // REGMAP_VERSION
  uint64_t   modes    = cfg->modesToHost( _egroupNrToHost );
  int        grp, cnt = 0;
  for( grp=0; grp<FLX_TOHOST_GROUPS-1; ++grp )
    if( (selection_mask & (1<<grp)) != 0 && grp != _egroupNrToHost )
      {
        cfg->setEnablesToHost( grp, enables );
        if( _radioButtonToHostEmode->isChecked() )
          cfg->setModesToHost( grp, modes );
#if REGMAP_VERSION >= 0x500
        else
          cfg->setDmaIndices( grp, dma_indices );
        cfg->setWidthToHost( grp, width_code );
#endif // REGMAP_VERSION
        ++cnt;
      }

  // Update display
  for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
    this->displayEgroupToHost( grp );
  QCoreApplication::processEvents();

  this->showMessage( QString( "ToHost: replicated Egroup %1 "
                              "to %2 selected Egroups" ).
                     arg( _egroupNrToHost ).arg( cnt ) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateEgroupToHostToAll()
{
  // Do not include the EC+IC+TTC group
  if( _egroupNrToHost == TOHOST_MINI_GROUP )
    return;

  // Do not include the EC+IC+TTC group
  GbtConfig *cfg     = &_gbtConfig[_gbtNr];
  uint32_t   enables = cfg->enablesToHost( _egroupNrToHost );
  uint64_t   modes   = cfg->modesToHost( _egroupNrToHost );
  int        groups  = FLX_TOHOST_GROUPS-1;
#if REGMAP_VERSION >= 0x500
  uint32_t   width_code  = cfg->widthToHost( _egroupNrToHost );
  uint32_t   dma_indices = cfg->dmaIndices( _egroupNrToHost );
  // In RM5 using egroup > 4 may interfere with EC/IC enables (e.g. felink)
  if( cfg->linkMode() != LINKMODE_LPGBT10_F5 &&
      cfg->linkMode() != LINKMODE_LPGBT5_F5 &&
      cfg->linkMode() != LINKMODE_LPGBT10_F12 &&
      cfg->linkMode() != LINKMODE_LPGBT5_F12 )
    groups = 5;
#endif // REGMAP_VERSION
  int        grp, cnt = 0;
  for( grp=0; grp<groups; ++grp )
    if( grp != _egroupNrToHost )
      {
        cfg->setEnablesToHost( grp, enables );
        if( _radioButtonToHostEmode->isChecked() )
          cfg->setModesToHost( grp, modes );
#if REGMAP_VERSION >= 0x500
        else
          cfg->setDmaIndices( grp, dma_indices );
        cfg->setWidthToHost( grp, width_code );
#endif // REGMAP_VERSION
        ++cnt;
      }

  // Update display
  for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
    this->displayEgroupToHost( grp );
  QCoreApplication::processEvents();

  this->showMessage( QString( "ToHost: replicated Egroup %1 "
                              "to all Egroups" ).
                     arg( _egroupNrToHost ) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::disableEgroupToHost()
{
  egroupWidget_t w = _egroupToHostWidgets[_egroupNrToHost];
  for( epathWidget_t e: w.epaths )
    e.enableCheck->setChecked( false );
}

// ----------------------------------------------------------------------------

void ElinkConfig::enableEgroupToHost()
{
  egroupWidget_t w = _egroupToHostWidgets[_egroupNrToHost];
  for( epathWidget_t e: w.epaths )
    e.enableCheck->setChecked( e.frame->isVisible() );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateEgroupFromHost()
{
  // Do not include the EC group
  if( _egroupNrFromHost == FROMHOST_MINI_GROUP )
    return;

  uint32_t selection_mask;
  QString title = QString("Replicate FromHost Egroup %1 "
                          "to Egroups:" ).arg( _egroupNrFromHost );
  int groups = FLX_FROMHOST_GROUPS-1;
  if( _gbtConfig[_gbtNr].linkMode() == LINKMODE_LPGBT10_F5 ||
      _gbtConfig[_gbtNr].linkMode() == LINKMODE_LPGBT5_F5 ||
      _gbtConfig[_gbtNr].linkMode() == LINKMODE_LPGBT10_F12 ||
      _gbtConfig[_gbtNr].linkMode() == LINKMODE_LPGBT5_F12 )
    groups -= 1; // No Egroup 4
  ReplicateDialog diag( this, _egroupNrFromHost, groups,
                        &selection_mask, title );

  if( diag.exec() == QDialog::Rejected || selection_mask == 0 )
    return;

  // Do not include the EC group
  GbtConfig *cfg      = &_gbtConfig[_gbtNr];
  uint32_t   enables  = cfg->enablesFromHost( _egroupNrFromHost );
  uint64_t   modes    = cfg->modesFromHost( _egroupNrFromHost );
#if REGMAP_VERSION >= 0x500
  uint32_t   width_code = cfg->widthFromHost( _egroupNrFromHost );
  uint32_t   ttc_option = cfg->ttcOptionFromHost( _egroupNrFromHost );
#endif // REGMAP_VERSION
  int        grp, cnt = 0;
  for( grp=0; grp<FLX_FROMHOST_GROUPS-1; ++grp )
    if( (selection_mask & (1<<grp)) != 0 && grp != _egroupNrFromHost )
      {
        cfg->setEnablesFromHost( grp, enables );
        cfg->setModesFromHost( grp, modes );
#if REGMAP_VERSION >= 0x500
        cfg->setWidthFromHost( grp, width_code );
        cfg->setTtcOptionFromHost( grp, ttc_option );
#endif // REGMAP_VERSION
        ++cnt;
      }

  // Update display
  for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
    displayEgroupFromHost( grp );
  QCoreApplication::processEvents();

  this->showMessage( QString( "FromHost: replicated Egroup %1 "
                              "to %2 selected Egroups" ).
                     arg( _egroupNrFromHost ).arg( cnt ) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateEgroupFromHostToAll()
{
  // Do not include the EC group
  if( _egroupNrFromHost == FROMHOST_MINI_GROUP )
    return;

  // Do not include the EC group
  GbtConfig *cfg     = &_gbtConfig[_gbtNr];
  uint32_t   enables = cfg->enablesFromHost( _egroupNrFromHost );
  uint64_t   modes   = cfg->modesFromHost( _egroupNrFromHost );
  int        groups  = FLX_FROMHOST_GROUPS-1;
#if REGMAP_VERSION >= 0x500
  uint32_t   width_code = cfg->widthFromHost( _egroupNrFromHost );
  uint32_t   ttc_option = cfg->ttcOptionFromHost( _egroupNrFromHost );
  // In RM5 using egroup > 4 may interfere with EC/IC enables (e.g. felink)
  if( cfg->linkMode() == LINKMODE_LPGBT10_F5 ||
      cfg->linkMode() == LINKMODE_LPGBT5_F5 ||
      cfg->linkMode() == LINKMODE_LPGBT10_F12 ||
      cfg->linkMode() == LINKMODE_LPGBT5_F12 )
    groups = 4;
#endif // REGMAP_VERSION
  int grp, cnt = 0;
  for( grp=0; grp<groups; ++grp )
    if( grp != _egroupNrFromHost )
      {
        cfg->setEnablesFromHost( grp, enables );
        cfg->setModesFromHost( grp, modes );
#if REGMAP_VERSION >= 0x500
        cfg->setWidthFromHost( grp, width_code );
        cfg->setTtcOptionFromHost( grp, ttc_option );
#endif // REGMAP_VERSION
        ++cnt;
      }

  // Update display
  for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
    displayEgroupFromHost( grp );
  QCoreApplication::processEvents();

  this->showMessage( QString( "FromHost: replicated Egroup %1 "
                              "to all Egroups" ).
                     arg( _egroupNrFromHost ) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::disableEgroupFromHost()
{
  egroupWidget_t w = _egroupFromHostWidgets[_egroupNrFromHost];
  for( epathWidget_t e: w.epaths )
    e.enableCheck->setChecked( false );
}

// ----------------------------------------------------------------------------

void ElinkConfig::enableEgroupFromHost()
{
  egroupWidget_t w = _egroupFromHostWidgets[_egroupNrFromHost];
  for( epathWidget_t e: w.epaths )
    e.enableCheck->setChecked( e.frame->isVisible() );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateLink()
{
  uint32_t selection_mask;
  QString title;
  if( _gbtNr == FLX_LINKS )
    title = QString("Replicate Link EMU config to links:");
  else
    title = QString("Replicate Link %1 config to links:").arg(_gbtNr);
  // One link extra representing the 'EMU' link
  ReplicateDialog diag( this, _gbtNr, FLX_LINKS+1, &selection_mask, title );
  if( diag.exec() == QDialog::Rejected || selection_mask == 0 )
    return;

  //GbtConfig *cfg = &_gbtConfig[_gbtNr];
  int lnk, cnt = 0;
  // One link extra representing the 'EMU' link
  for( lnk=0; lnk<=FLX_LINKS; ++lnk )
    if( (selection_mask & (1<<lnk)) != 0 && lnk != _gbtNr )
      {
        _gbtConfig[lnk] = _gbtConfig[_gbtNr];
        ++cnt;
        // Don't copy TTC2Host enable bit...
        if( lnk != 0 )
          {
            // Clear bit representing enabled TTC2Host channel
            uint32_t enables;
            enables = _gbtConfig[lnk].enablesToHost( TOHOST_MINI_GROUP );
            enables &= ~TTC2H_ENABLED;
            _gbtConfig[lnk].setEnablesToHost( TOHOST_MINI_GROUP, enables );
          }
      }

  //QMessageBox::information( this, "Replicate GBT link",
  //                          QString("Replicated GBT %1 config "
  //                                  "to %2 selected links").
  //                          arg( _gbtNr ).arg( cnt ) );
  this->showMessage( QString("Replicated GBT %1 config "
                             "to %2 selected links").
                     arg( _gbtNr ).arg( cnt ) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::replicateLinkToAll()
{
  //GbtConfig *cfg = &_gbtConfig[_gbtNr];
  int lnk, cnt = 0;
  // One link extra representing the 'EMU' link
  for( lnk=0; lnk<=FLX_LINKS; ++lnk )
    if( lnk != _gbtNr )
      {
        _gbtConfig[lnk] = _gbtConfig[_gbtNr];
        ++cnt;
        // Don't copy TTC2Host enable bit; there's only one per device
        if( lnk != 0 )
          {
            // Clear bit representing enabled TTC2Host channel
            uint32_t enables;
            enables = _gbtConfig[lnk].enablesToHost( TOHOST_MINI_GROUP );
            enables &= ~TTC2H_ENABLED;
            _gbtConfig[lnk].setEnablesToHost( TOHOST_MINI_GROUP, enables );
          }
      }

  QString qs;
  if( _gbtNr == FLX_LINKS )
    qs = QString("Replicated Link EMU config "
                 "to %1 other Links").arg( cnt );
  else
    qs = QString("Replicated Link %1 config "
                 "to %2 other Links").arg( _gbtNr ).arg( cnt );
  //QMessageBox::information( this, "Replicate Link", qs );
  this->showMessage( qs );
}

// ----------------------------------------------------------------------------

void ElinkConfig::showAdvancedOptions( int checked )
{
  // Show/hide the advanced/developer options
  if( checked > 0 )
    {
      //labelMaxChunk->show();
      if( _radioButtonFullMode->isChecked() )
        {
          label2bit->hide();
          label4bit->hide();
          label8bit->hide();
          label16bit->hide();
          _spinBox2BitMax->hide();
          _spinBox4BitMax->hide();
          _spinBox8BitMax->hide();
          _spinBox16BitMax->hide();
        }
      else
        {
          // Commented 9 Feb 2021: don't show spinboxes anymore
          //label2bit->show();
          //label4bit->show();
          //label8bit->show();
          //label16bit->show();
          //_spinBox2BitMax->show();
          //_spinBox4BitMax->show();
          //_spinBox8BitMax->show();
          //_spinBox16BitMax->show();
        }

      //_radioButtonWideMode->show();
      //_pushButtonEgroup5->show();
      //_pushButtonEgroup6->show();

      //_radioButtonLpGbtMode->show();

      _frameGroupInfo->show();
    }
  else
    {
      //labelMaxChunk->hide();
      label2bit->hide();
      label4bit->hide();
      label8bit->hide();
      label16bit->hide();
      _spinBox2BitMax->hide();
      _spinBox4BitMax->hide();
      _spinBox8BitMax->hide();
      _spinBox16BitMax->hide();

      _radioButtonWideMode->hide();
      _pushButtonEgroup5->hide();
      _pushButtonEgroup6->hide();

#if REGMAP_VERSION < 0x500
      _radioButtonLpGbtMode->hide();
#endif // REGMAP_VERSION

      _frameGroupInfo->hide();
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::showStreamIdOptions( int checked )
{
  // Show/hide the StreamId options
  if( checked > 0 && _gbtNr != FLX_LINKS )
    {
      _labelStreamId->show();
      for( QCheckBox *cb: _streamChecks )
        cb->show();
    }
  else
    {
      _labelStreamId->hide();
      for( QCheckBox *cb: _streamChecks )
        cb->hide();
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::readLinkConfig()
{
  // Read the current configuration from the selected FELIX device
  int device_nr = _comboBoxFelix->currentIndex();
  FlxCard felix;
  flxcard_bar2_regs_t *bar2;
  try {
    felix.card_open( device_nr, LOCK_NONE );
    bar2 = (flxcard_bar2_regs_t *) felix.bar2Address();
  }
  catch( FlxException &ex ) {
    QString qs( "Failed to open FLX-device:\n" );
    qs += QString::fromStdString( ex.what() );
    QMessageBox::warning( this, "Read FLX-device configuration", qs );
    return;
  }

  // Clear the file name line edit
  _lineEditFileName->setText( "" );

  // Number of channels
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  //if( bar2->CARD_TYPE == 711 || bar2->CARD_TYPE == 712 ) chans /= 2;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;

  // Link mode
  uint32_t linkmode;
  bool lpgbt_type = felix.lpgbt_type();
  if( felix.fullmode_type() )
    {
      linkmode = LINKMODE_FULL;
    }
  else if( lpgbt_type )
    {
      if( felix.lpgbt_5gbps() )
        {
          if( felix.lpgbt_fec12() )
            linkmode = LINKMODE_LPGBT5_F12;
          else
            linkmode = LINKMODE_LPGBT5_F5;
        }
      else
        {
          if( felix.lpgbt_fec12() )
            linkmode = LINKMODE_LPGBT10_F12;
          else
            linkmode = LINKMODE_LPGBT10_F5;
        }
    }
  else
    {
      uint64_t wm = bar2->WIDE_MODE;
      if( wm == 1 )
        linkmode = LINKMODE_GBTWIDE;
      else
        linkmode = LINKMODE_GBT;
    }
  for( uint64_t gbt=0; gbt<chans; ++gbt )
    _gbtConfig[gbt].setLinkMode( linkmode );

  // Firmware configuration
  _fwMode                     = bar2->FIRMWARE_MODE;
  _fwDmaToHostDescriptorCount = bar2->GENERIC_CONSTANTS.DESCRIPTORS - 1;
  _fwDirectModeIncluded       = (bar2->CR_GENERICS.DIRECT_MODE_INCLUDED == 1);
  for( uint64_t grp=0; grp<7; ++grp )
    _fwEgroupIncludes[grp] = bar2->INCLUDE_EGROUPS[grp].INCLUDE_EGROUP;

#if REGMAP_VERSION >= 0x500
  // Get snapshot of the Elink alignment status
  for( uint64_t link=0; link<FLX_LINKS; ++link )
    _fwElinksAligned[link] =
      bar2->DECODING_LINK_STATUS_ARR[link].DECODING_LINK_ALIGNED;

  // Get EC and IC E-link numbers to use from the firmware
  _ecToHostIndex    = bar2->AXI_STREAMS_TOHOST.EC_INDEX;
  _icToHostIndex    = bar2->AXI_STREAMS_TOHOST.IC_INDEX;
  _auxToHostIndex   = _icToHostIndex + 1;
  _ecFromHostIndex  = bar2->AXI_STREAMS_FROMHOST.EC_INDEX;
  _icFromHostIndex  = bar2->AXI_STREAMS_FROMHOST.IC_INDEX;
  _auxFromHostIndex = _icFromHostIndex + 1;

  if( _fwMode == FIRMW_STRIP )
    // Read the STRIP firmware FromHost LCB/R3L1 E-group swap register
    _itkStripLcbR3l1ElinkSwap = bar2->ITKSTRIP_LCB_R3L1_ELINK_SWAP;
#endif // REGMAP_VERSION

  uint64_t gbt, egroup;
  uint32_t enables, truncation;
  uint64_t modes;
  // To Host:
  // ========
  for( gbt=0; gbt<chans; ++gbt )
    {
      truncation = 0;
      if( gbt < FLX_LINKS/2 )
      for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
        {
#if REGMAP_VERSION < 0x500
          flxcard_cr_tohost_egroup_ctrl_t config =
                     bar2->CR_GBT_CTRL[gbt].EGROUP_TOHOST[egroup].TOHOST;
          uint64_t modebits;
          enables  = (uint32_t) config.EPROC_ENA;
          modebits = config.PATH_ENCODING; // 2 bits per E-path!
          truncation = (uint32_t) config.MAX_CHUNK_LEN;
          if( egroup == 0 )
            _gbtConfig[gbt].setMaxChunkWord( truncation );

          // Translate 'mode' word from register to local format 'modes' word,
          // i.e. 4 bits per E-PROC
          //modes = GbtConfig::epath2BitModeWordToModes( enables, modebits );
          modes = GbtConfig::epath2BitModeWordToModes( modebits );

          // FULL mode?
          if( egroup == 0 && linkmode == LINKMODE_FULL )
            if( (bar2->CR_FM_PATH_ENA & (1<<gbt)) != 0 )
              enables |= FULL_ENABLED;
#else
          flxcard_decoding_egroup_ctrl_t config =
            bar2->DECODING_EGROUP_CTRL_GEN[gbt].DECODING_EGROUP[egroup].EGROUP;
          uint32_t width;
          // There are only 8 enable bits: the width setting determines
          // what the E-links width is (they are the same within an E-group)
          enables = (uint32_t) config.EPATH_ENA;
          width   = (uint32_t) config.EPATH_WIDTH;
          modes   = config.PATH_ENCODING; // 4 bits per E-path!
          // Collect HDLC truncation bits
          truncation |= (config.ENABLE_TRUNCATION << (egroup+3));

          // FULL mode?
          if( egroup == 0 && linkmode == LINKMODE_FULL )
            if( (bar2->DECODING_EGROUP_CTRL_GEN[gbt].DECODING_EGROUP[0].EGROUP.EPATH_ENA & 1) != 0 )
              enables |= FULL_ENABLED;

          _gbtConfig[gbt].setWidthToHost( egroup, width );
#endif // REGMAP_VERSION

          // Store in our local configuration
          _gbtConfig[gbt].setEnablesToHost( egroup, enables );
          _gbtConfig[gbt].setModesToHost( egroup, modes );
        }

      // and the EC+IC+AUX+TTCtoHost egroup
      enables = 0x0000;

#if REGMAP_VERSION < 0x500
      flxcard_ec_tohost_t *ec_tohost =
                     &bar2->MINI_EGROUP_CTRL[gbt].EC_TOHOST;

      // To-host EC enabled/disabled
      if( ec_tohost->ENABLE )
        enables |= EC_ENABLED;

      // To-host EC encoding:
      // translate 'mode' word from register to local format 'modes' word,
      // i.e. 4 bits per E-PROC
      uint64_t modebits;
      modebits = ec_tohost->ENCODING << (7*2);
      modes = GbtConfig::epath2BitModeWordToModes( modebits );

      // To-host IC enabled/disabled
      if( ec_tohost->IC_ENABLE )
        enables |= IC_ENABLED;

      // To-host AUX enabled/disabled
      if( ec_tohost->SCA_AUX_ENABLE && _fwMode == FIRMW_LTDB )
        enables |= AUX_ENABLED;

      // TTC-to-Host enabled/disabled
      if( gbt == 0 && bar2->CR_TTC_TOHOST.ENABLE )
        enables |= TTC2H_ENABLED;
#else
      flxcard_mini_egroup_tohost_t *mini_egroup_tohost =
        &bar2->MINI_EGROUP_TOHOST_GEN[gbt].MINI_EGROUP_TOHOST;

      // To-host EC enabled/disabled
      if( mini_egroup_tohost->EC_ENABLE )
        enables |= EC_ENABLED;

      // To-host EC encoding: 4 bits
      modes = (uint64_t) mini_egroup_tohost->EC_ENCODING << (EC_ENABLED_BIT*4);

      // To-host IC enabled/disabled
      if( mini_egroup_tohost->IC_ENABLE )
        enables |= IC_ENABLED;

      // To-host AUX enabled/disabled
      if( mini_egroup_tohost->AUX_ENABLE && _fwMode == FIRMW_LTDB )
        enables |= AUX_ENABLED;

      // TTC-to-Host enabled/disabled
      if( gbt == 0 && bar2->TTC_TOHOST_ENABLE )
        enables |= TTC2H_ENABLED;

      // Collect HDLC truncation bits
      if( mini_egroup_tohost->ENABLE_EC_TRUNCATION )
        truncation |= (1<<0);
      if( mini_egroup_tohost->ENABLE_IC_TRUNCATION )
        truncation |= (1<<1);
      if( mini_egroup_tohost->ENABLE_AUX_TRUNCATION )
        truncation |= (1<<2);
      // ..and configure
      _gbtConfig[gbt].setMaxChunkWord( truncation );
#endif // REGMAP_VERSION

      // Store in our local configuration
      _gbtConfig[gbt].setEnablesToHost( TOHOST_MINI_GROUP, enables );
      _gbtConfig[gbt].setModesToHost( TOHOST_MINI_GROUP, modes );

      // TTC Clock setting
      if( gbt == 0 )
        {
          bool ttc_clock = (bar2->MMCM_MAIN.LCLK_SEL == 0);
          _gbtConfig[gbt].setTtcClock( ttc_clock );
        }

#if REGMAP_VERSION >= 0x0500
      // DMA controller indices
      int elinknr;
      int max_pathnr;
      if( lpgbt_type )
        max_pathnr = 4;
      else
        max_pathnr = 8;
      if( gbt < FLX_LINKS/2 )
      for( int group=0; group<FLX_TOHOST_GROUPS-1; ++group )
        {
          uint32_t dma_indices = 0;
          for( int path=0; path<max_pathnr; ++path )
            {
              if( lpgbt_type )
                elinknr = ((gbt << BLOCK_LNK_SHIFT) |
                           (group << BLOCK_EGROUP_SHIFT_LPGBT) | path);
              else
                elinknr = ((gbt << BLOCK_LNK_SHIFT) |
                           (group << BLOCK_EGROUP_SHIFT) | path);

              // Set 'address' (and read current DMA index setting)
              bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
              // Read DMA index setting
              dma_indices |= ((bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) <<
                              (path*4));
            }
          _gbtConfig[gbt].setDmaIndices( group, dma_indices );
        }
      // EC
      uint32_t dma_indices = 0;
      elinknr = (gbt << BLOCK_LNK_SHIFT) | _ecToHostIndex;
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read EC DMA index setting, store as path 7
      dma_indices |= ((bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (7*4));
      // IC
      elinknr = (gbt << BLOCK_LNK_SHIFT) | _icToHostIndex;
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read EC DMA index setting, store as path 6
      dma_indices |= ((bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (6*4));
      /* AUX, ignore: LTDB has only one endpoint
      elinknr = (gbt << BLOCK_LNK_SHIFT) | (_ecToHostIndex - 1);
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read AUX DMA index setting, store as path 5
      dma_indices |= ((bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (5*4));
      */
      // TTCtoHost
      if( gbt == 0 )
        {
          elinknr = 24 << BLOCK_LNK_SHIFT; // E-link 0x600
          bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
          // Read TTCtoHost DMA index setting, store as path 0
          dma_indices |= (bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF);
        }
      _gbtConfig[gbt].setDmaIndices( TOHOST_MINI_GROUP, dma_indices );
#endif // REGMAP_VERSION

      // Stream ID bits
#if REGMAP_VERSION < 0x500
      uint64_t *p = (uint64_t *) &bar2->PATH_HAS_STREAM_ID[gbt].TOHOST;
#else
      uint64_t *p = (uint64_t *) &bar2->PATH_HAS_STREAM_ID[gbt];
#endif // REGMAP_VERSION
      uint64_t streamid = *p;
      for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
        {
          _gbtConfig[gbt].setStreamIdBits( egroup, streamid & 0xFF );
          streamid >>= 8;
        }
    }

  // From Host:
  // ==========
  for( gbt=0; gbt<chans; ++gbt )
    {
      if( gbt < FLX_LINKS/2 )
      for( egroup=0; egroup<FLX_FROMHOST_GROUPS-1; ++egroup )
        {
#if REGMAP_VERSION < 0x500
          flxcard_cr_fromhost_egroup_ctrl_t config =
                     bar2->CR_GBT_CTRL[gbt].EGROUP_FROMHOST[egroup].FROMHOST;
          enables  = (uint32_t) config.EPROC_ENA;
          uint64_t modebits;
          modebits = config.PATH_ENCODING; // 4 bits per E-path!

          // Translate 'mode' word from register to local format
          // 'modes' word i.e. 4 bits per E-PROC
          //modes = GbtConfig::epath4BitModeWordToModes( enables, modewrd );
          modes = GbtConfig::epath4BitModeWordToModes( modebits );
#else
          flxcard_encoding_egroup_ctrl_t config =
            bar2->ENCODING_EGROUP_CTRL_GEN[gbt].ENCODING_EGROUP[egroup].ENCODING_EGROUP_CTRL;
          uint32_t width, ttc_opt;
          enables = (uint32_t) config.EPATH_ENA;
          width   = (uint32_t) config.EPATH_WIDTH;
          modes   = config.PATH_ENCODING; // 4 bits per E-path!
          ttc_opt = config.TTC_OPTION;

          _gbtConfig[gbt].setWidthFromHost( egroup, width );
          _gbtConfig[gbt].setTtcOptionFromHost( egroup, ttc_opt );
#endif // REGMAP_VERSION

          // Store in our local configuration
          _gbtConfig[gbt].setEnablesFromHost( egroup, enables );
          _gbtConfig[gbt].setModesFromHost( egroup, modes );
        }

      // and the EC+IC+AUX egroup
      enables = 0x0000;

#if REGMAP_VERSION < 0x500
      flxcard_ec_fromhost_t *ec_fromhost =
                     &bar2->MINI_EGROUP_CTRL[gbt].EC_FROMHOST;

      // From-host EC enabled/disabled
      if( ec_fromhost->ENABLE )
        enables |= EC_ENABLED;

      // From-host EC encoding
      //modebits = ((config & 0x1E) >> 1) << (7*4);
      uint64_t modebits;
      modebits = ec_fromhost->ENCODING << (7*4);
      modes = GbtConfig::epath4BitModeWordToModes( modebits );

      // From-host IC enabled/disabled
      if( ec_fromhost->IC_ENABLE )
        enables |= IC_ENABLED;

      // From-host AUX enabled/disabled
      if( ec_fromhost->SCA_AUX_ENABLE && _fwMode == FIRMW_LTDB )
        enables |= AUX_ENABLED;
#else
      flxcard_mini_egroup_fromhost_t *mini_egroup_fromhost =
        &bar2->MINI_EGROUP_FROMHOST_GEN[gbt].MINI_EGROUP_FROMHOST;

      // From-host EC enabled/disabled
      if( mini_egroup_fromhost->EC_ENABLE )
        enables |= EC_ENABLED;

      // From-host EC encoding: 4 bits
      modes = (uint64_t) mini_egroup_fromhost->EC_ENCODING<<(EC_ENABLED_BIT*4);

      // From-host IC enabled/disabled
      if( mini_egroup_fromhost->IC_ENABLE )
        enables |= IC_ENABLED;

      // From-host AUX enabled/disabled
      if( mini_egroup_fromhost->AUX_ENABLE && _fwMode == FIRMW_LTDB )
        enables |= AUX_ENABLED;

      if( (bar2->LINK_FULLMODE_LTI & (1 << gbt)) != 0 )
        _gbtConfig[gbt].setLtiTtc( true );
      else
        _gbtConfig[gbt].setLtiTtc( false );
#endif // REGMAP_VERSION

      // Store in our local configuration
      _gbtConfig[gbt].setEnablesFromHost( FROMHOST_MINI_GROUP, enables );
      _gbtConfig[gbt].setModesFromHost( FROMHOST_MINI_GROUP, modes );
    }

  // Propagate firmware configuration limitations onto the link configuration
  // (because the (default) settings in the firmware may not be consistent)
  flxcard_include_egroup_t includes;
#if REGMAP_VERSION < 0x500
  // ToHost: do not include the EC/IC group
  for( unsigned long grp=0; grp<FLX_TOHOST_GROUPS-1; ++grp )
    {
      includes = _fwEgroupIncludes[grp];
      for( gbt=0; gbt<=FLX_LINKS; ++gbt )
        {
          GbtConfig *cfg = &_gbtConfig[gbt];
          uint32_t mask = 0;
          if( includes.TOHOST_02 == 1 || includes.TOHOST_HDLC == 1 )// 2-bit
            mask |= 0x07F80;
          if( includes.TOHOST_04 == 1 ) // 4-bit
            mask |= 0x00078;
          if( includes.TOHOST_08 == 1 ) // 8-bit
            mask |= 0x00006;
          if( includes.TOHOST_16 == 1 ) // 16-bit
            mask |= 0x00001;
          mask |= FULL_ENABLED; // FULL mode
          uint32_t enables = cfg->enablesToHost( grp );
          cfg->setEnablesToHost( grp, enables & mask );
        }
    }
  // FromHost: do not include the EC/IC group
  // ### Don't disable: all TTC modes are still possible
  /*
  for( unsigned long grp=0; grp<FLX_FROMHOST_GROUPS-1; ++grp )
    {
      includes = _fwEgroupIncludes[grp];
      for( gbt=0; gbt<=FLX_LINKS; ++gbt )
        {
          GbtConfig *cfg = &_gbtConfig[gbt];
          uint32_t mask = 0;
          if( includes.FROMHOST_02 == 1 || includes.FROMHOST_HDLC == 1 )// 2-bit
            mask |= 0x07F80;
          if( includes.FROMHOST_04 == 1 ) // 4-bit
            mask |= 0x00078;
          if( includes.FROMHOST_08 == 1 ) // 8-bit
            mask |= 0x00006;
          //if( includes.FROMHOST_16 == 1 ) // 16-bit
          mask |= 0x00001;
          enables = cfg->enablesFromHost( grp );
          cfg->setEnablesFromHost( grp, enables & mask );

          if( includes.FROMHOST_02 == 0 && includes.FROMHOST_HDLC == 0 &&
              includes.FROMHOST_04 == 0 && includes.FROMHOST_08 == 0 &&
              (enables & mask) == 0 ) {
            // Select first TTC mode
            cfg->setModesFromHost( grp, 0x333333333333333 );}
        }
    }
  */
#else
  // ToHost: do not include the EC/IC group
  for( unsigned long grp=0; grp<FLX_TOHOST_GROUPS-1; ++grp )
    {
      includes = _fwEgroupIncludes[grp];
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        {
          GbtConfig *cfg = &_gbtConfig[gbt];
          if( (includes.TOHOST_02 == 0 && includes.TOHOST_HDLC == 0 &&
               cfg->widthToHost(grp) == 0) || // 2-bit
              (includes.TOHOST_04 == 0 &&
               cfg->widthToHost(grp) == 1) || // 4-bit
              (includes.TOHOST_08 == 0 &&
               cfg->widthToHost(grp) == 2) || // 8-bit
              (includes.TOHOST_16 == 0 &&
               cfg->widthToHost(grp) == 3) || // 16-bit
              (includes.TOHOST_32 == 0 &&
               cfg->widthToHost(grp) == 4) )  // 32-bit
            {
              cfg->setEnablesToHost( grp, 0 );
            }
        }
    }
  // FromHost: do not include the EC/IC group
  // ### Don't disable: all TTC modes are still possible
  /*
  for( unsigned long grp=0; grp<FLX_FROMHOST_GROUPS-1; ++grp )
    {
      includes = _fwEgroupIncludes[grp];
      for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
        {
          GbtConfig *cfg = &_gbtConfig[gbt];
          if( (includes.FROMHOST_02 == 0 && includes.FROMHOST_HDLC == 0 &&
               cfg->widthFromHost(grp) == 0) || // 2-bit
              (includes.FROMHOST_04 == 0 &&
               cfg->widthFromHost(grp) == 1) || // 4-bit
              (includes.FROMHOST_08 == 0 &&
               cfg->widthFromHost(grp) == 2) )  // 8-bit
            {
              cfg->setEnablesFromHost( grp, 0 );
            }

          if( includes.FROMHOST_02 == 0 && includes.FROMHOST_HDLC == 0 &&
              includes.FROMHOST_04 == 0 && includes.FROMHOST_08 == 0 &&
              (enables & mask) == 0 )
            // Select first TTC mode
            cfg->setModesFromHost( grp, 0x0x33333333 );
        }
    }
    */
#endif // REGMAP_VERSION

  // Configure EMU link as Link #0 (ToHost-direction, disable FromHost part)
  _gbtConfig[FLX_LINKS] = _gbtConfig[0];
  for( egroup=0; egroup<FLX_FROMHOST_GROUPS; ++egroup )
    _gbtConfig[FLX_LINKS].setEnablesFromHost( egroup, 0 );

  // Maximum link according to the firmware
  //_spinBoxLinkNr->setValue( 0 );
  _spinBoxLinkNr->setMaximum( chans-1 );

  // Configure appropriate link mode checkbox (and combobox)
  if( linkmode == LINKMODE_GBT )
    {
      _radioButtonNormalMode->setChecked( true );
    }
  else if( linkmode == LINKMODE_GBTWIDE )
    {
      _radioButtonWideMode->setChecked( true );
      _checkBoxAdvanced->setChecked( true );
      this->showAdvancedOptions( true );
    }
  else if( linkmode == LINKMODE_LPGBT10_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10.24Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 0 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5.12Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 1 );
    }
  else if( linkmode == LINKMODE_LPGBT10_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10.24Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 2 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5.12Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 3 );
    }
  else
    {
      _radioButtonFullMode->setChecked( true );
    }

  // Update display, but don't initialize EC and IC E-link numbers
  // and number of channels, which were read from the firmware
  this->applyLinkMode( false );

  // Disable Egroup widget combobox items depending on firmware configuration
  this->disableGuiItems();

  QCoreApplication::processEvents();

  QString qs = QString( "Configuration (%1 chans) read from FLX-device" )
    .arg( chans );
  //QMessageBox::information( this, "Read FLX-device Configuration", qs );
  this->showMessage( qs );

  // Indicate the currently selected FLX device's configuration has been read
  this->selectedDeviceChanged( -1 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::disableGuiItems()
{
  // Disable Egroup/Elink widget items depending on the firmware configuration:
  // this has been read from the corresponding registers
  // (E-link mode options availability is applied in displayEgroupToHost()
  //  and displayEgroupFromHost())
  bool is_gbtmode = !(_fwMode==FIRMW_FULL ||
                      _fwMode==FIRMW_FMEMU || _fwMode==FIRMW_MROD);
  flxcard_include_egroup_t includes;
  for( unsigned long grp=0; grp<7; ++grp )
    {
      includes = _fwEgroupIncludes[grp];

      // ToHost
      if( grp < _egroupToHostWidgets.size() && is_gbtmode )
        {
          QComboBox *cb = _egroupToHostWidgets[grp].ewidthCombo;
          this->setComboItemEnabled( cb, 0, (includes.TOHOST_02 == 1 ||
                                             includes.TOHOST_HDLC == 1) );
          this->setComboItemEnabled( cb, 1, (includes.TOHOST_04 == 1) );
          this->setComboItemEnabled( cb, 2, (includes.TOHOST_08 == 1) );
          this->setComboItemEnabled( cb, 3, (includes.TOHOST_16 == 1) );
#if REGMAP_VERSION >= 0x500
          if( cb->count() > 4 )
            this->setComboItemEnabled( cb, 4, (includes.TOHOST_32 == 1) );
#else
          if( cb->count() > 4 )
            this->setComboItemEnabled( cb, 4, false );
#endif // REGMAP_VERSION

          if( includes.TOHOST_HDLC == 0 && includes.TOHOST_02 == 0 &&
              includes.TOHOST_04 == 0 && includes.TOHOST_08 == 0 &&
#if REGMAP_VERSION >= 0x500
              includes.TOHOST_16 == 0 && includes.TOHOST_32 == 0 )
#else
              includes.TOHOST_16 == 0 )
#endif // REGMAP_VERSION
            {
              // Nothing enabled
              cb->setEnabled( false );
              for( epathWidget_t &ew : _egroupToHostWidgets[grp].epaths )
                {
                  ew.enableCheck->setChecked( false );
                  ew.enableCheck->setEnabled( false );
                  ew.modeCombo->setCurrentIndex( -1 );
                  ew.modeCombo->setEnabled( false );
                }
              //if( gbt == 0 ) //###DEBUG
              //QMessageBox::information( this, "EGROUP TH disable",
              //                          QString::number(grp) );
            }
          else if( !this->comboItemEnabled( cb, cb->currentIndex() ) )
            {
              // Select the first item that *is* enabled
              if( this->comboItemEnabled( cb, 0 ) )
                cb->setCurrentIndex( 0 );
              else if( this->comboItemEnabled( cb, 1 ) )
                cb->setCurrentIndex( 1 );
              else if( this->comboItemEnabled( cb, 2 ) )
                cb->setCurrentIndex( 2 );
              else if( this->comboItemEnabled( cb, 3 ) )
                cb->setCurrentIndex( 3 );
              else if( cb->count() > 4 && this->comboItemEnabled( cb, 4 ) )
                cb->setCurrentIndex( 4 );
            }
        }

      // ###FromHost: reflected mostly in the mode selection options
      //    (see functions displayEgroupTo/FromHost()),
      //    since the TTC modes remain available
      //    (so simple E-link width disabling as for ToHost is not possible:
      //     i.e. outcommented below)
      if( grp < _egroupFromHostWidgets.size() )
        {
          QComboBox *cb = _egroupFromHostWidgets[grp].ewidthCombo;
          this->setComboItemEnabled( cb, 3, false ); // 16-bit never enabled?
          if( cb->currentIndex() == 3 )
            cb->setCurrentIndex( 0 );
        }
      /*
      if( grp < _egroupFromHostWidgets.size() )
        {
          QComboBox *cb = _egroupFromHostWidgets[grp].ewidthCombo;
          this->setComboItemEnabled( cb, 0, (includes.FROMHOST_02 == 1 ||
                                             includes.FROMHOST_HDLC == 1) );
          this->setComboItemEnabled( cb, 1, (includes.FROMHOST_04 == 1) );
          this->setComboItemEnabled( cb, 2, (includes.FROMHOST_08 == 1) );
          this->setComboItemEnabled( cb, 3, false ); // 16-bit

          if( includes.FROMHOST_HDLC == 0 && includes.FROMHOST_02 == 0 &&
              includes.FROMHOST_04 == 0 && includes.FROMHOST_08 == 0 )
            {
              // Nothing enabled
              cb->setEnabled( false );
              for( epathWidget_t &ew : _egroupFromHostWidgets[grp].epaths )
                {
                  ew.enableCheck->setChecked( false );
                  ew.enableCheck->setEnabled( false );
                  ew.modeCombo->setCurrentIndex( -1 );
                  ew.modeCombo->setEnabled( false );
                }
            }
          else if( !this->comboItemEnabled( cb, cb->currentIndex() ) )
            {
              // Select the first item that *is* enabled
              if( this->comboItemEnabled( cb, 0 ) )
                cb->setCurrentIndex( 0 );
              else if( this->comboItemEnabled( cb, 1 ) )
                cb->setCurrentIndex( 1 );
              else if( this->comboItemEnabled( cb, 2 ) )
                cb->setCurrentIndex( 2 );
              else if( this->comboItemEnabled( cb, 3 ) )
                cb->setCurrentIndex( 3 );
            }

          if( !direct_mode_included )
            for( epathWidget_t &ew : _egroupFromHostWidgets[grp].epaths )
              {
                cb = ew.modeCombo;
                this->setComboItemEnabled( cb, 0, false ); // direct mode
              }

          // Propagate firmware configuration found to the link configuration
#if REGMAP_VERSION < 0x500
          for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
            {
              GbtConfig *cfg = &_gbtConfig[gbt];
              uint32_t mask = 0;
              if( includes.FROMHOST_02 == 1 ||
                  includes.FROMHOST_HDLC == 1 ) // 2-bit
                mask |= 0x07F80;
              if( includes.FROMHOST_04 == 1 ) // 4-bit
                mask |= 0x00078;
              if( includes.FROMHOST_08 == 1 ) // 8-bit
                mask |= 0x00006;
              uint32_t enables = cfg->enablesFromHost( grp );
              cfg->setEnablesFromHost( grp, enables & mask );
            }
#else
          for( int gbt=0; gbt<=FLX_LINKS; ++gbt )
            {
              GbtConfig *cfg = &_gbtConfig[gbt];
              if( (includes.FROMHOST_02 == 0 && includes.FROMHOST_HDLC == 0 &&
                   cfg->widthFromHost(grp) == 0) || // 2-bit
                  (includes.FROMHOST_04 == 0 &&
                   cfg->widthFromHost(grp) == 1) || // 4-bit
                  (includes.FROMHOST_08 == 0 &&
                   cfg->widthFromHost(grp) == 2) )   // 8-bit
                {
                  cfg->setEnablesFromHost( grp, 0 );
                }
            }
#endif // REGMAP_VERSION
        }
      */
    }

  // Disable direct mode
  if( _radioButtonToHostEmode->isChecked() )
    this->setComboItemEnabled( _comboEcToHostMode, 0, // direct mode
                               _fwDirectModeIncluded );
  this->setComboItemEnabled( _comboEcFromHostMode, 0, // direct mode
                             _fwDirectModeIncluded );

  // Firmware-specific stuff
  if( _fwMode == FIRMW_LTDB )
    {
      if( _radioButtonToHostEmode->isChecked() )
        this->setComboItemEnabled( _comboEcToHostMode, 1, false ); // 8b10b
      this->setComboItemEnabled( _comboEcFromHostMode, 1, false ); // 8b10b
    }
  if( _fwMode == FIRMW_STRIP )
    {
      // Hide all FromHost E-group configuration stuff
      // (STRIP image no longer used, now configurable, 14 Feb 2025)
      /*
      for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
        {
          _egroupFromHostWidgets[grp].enabled = false;
          displayEgroupFromHost( grp );
        }

      // Display a static figure instead...
      if( _stripsImage == 0 )
        {
          _stripsImage = new QLabel;
          QPixmap pm( ":/icon/icons/elinkconfig_strips_no_numbers.png" );
          _stripsImage->setPixmap( pm );
          _hLayoutFromHost->insertWidget( 0, _stripsImage );
        }
      _stripsImage->show();
      this->resize( 0, this->size().height() ); // Squeeze window horizontally
      */
      // EC comboboxes: display "Endeavour" and disable
      if( _radioButtonToHostEmode->isChecked() )
        _comboEcToHostMode->setItemText( 2, "Endeavour" );
      _comboEcFromHostMode->setItemText( 2, "Endeavour" );
      if( _radioButtonToHostEmode->isChecked() )
        _comboEcToHostMode->setEnabled( false );
      _comboEcFromHostMode->setEnabled( false );
    }
}

// ----------------------------------------------------------------------------

void ElinkConfig::reEnableGuiItems()
{
  // Re-enable any disabled ToHost E-link widths
  for( egroupWidget_t widget : _egroupToHostWidgets )
    {
      QComboBox *cb = widget.ewidthCombo;
      cb->setEnabled( true );
      if( !_radioButtonLpGbtMode->isChecked() )
        {
          this->setComboItemEnabled( cb, 0, true ); // 2-bit
          this->setComboItemEnabled( cb, 1, true ); // 4-bit
          if( cb->count() > 4 )
            // Item should not be there...
            this->setComboItemEnabled( cb, 4, false ); // 32-bit
        }
      else
        {
          // lpGBT
          this->setComboItemEnabled( cb, 4, true ); // 32-bit
        }
      this->setComboItemEnabled( cb, 2, true ); // 8-bit
      this->setComboItemEnabled( cb, 3, true ); // 16-bit
    }

  // Re-enable any disabled FromHost E-link widths
  for( egroupWidget_t widget : _egroupFromHostWidgets )
    {
      QComboBox *cb = widget.ewidthCombo;
      this->setComboItemEnabled( cb, 3, true ); // 16-bit
    }

  // Re-enable disabled E-link mode options in the combos
  for( uint32_t grp=0; grp<_egroupToHostWidgets.size(); ++grp )
    this->changeToHostEwidth( grp );
  for( uint32_t grp=0; grp<_egroupFromHostWidgets.size(); ++grp )
    this->changeFromHostEwidth( grp );
  this->setComboItemEnabled( _comboEcToHostMode, 0,   // direct mode
                             _fwDirectModeIncluded );
  this->setComboItemEnabled( _comboEcFromHostMode, 0, // direct mode
                             _fwDirectModeIncluded );

  // Revert FIRMW_LTDB specific GUI configuration (see disableGuiItems())
  this->setComboItemEnabled( _comboEcToHostMode, 1, true ); // 8b10b
  this->setComboItemEnabled( _comboEcFromHostMode, 1, true ); // 8b10b

  // Revert FIRMW_STRIP specific GUI configuration (see disableGuiItems())
  // (STRIP image no longer used, configurable now, 14 Feb 2025)
  //if( _stripsImage )
  //  _stripsImage->hide();

  // EC comboboxes
  if( _radioButtonToHostEmode->isChecked() )
    _comboEcToHostMode->setItemText( 2, "HDLC" );
  _comboEcFromHostMode->setItemText( 2, "HDLC" );
  _comboEcToHostMode->setEnabled( true );
  _comboEcFromHostMode->setEnabled( true );
}

// ----------------------------------------------------------------------------

void ElinkConfig::toHostFanOutSelect()
{
  QString title = QString("GBT ToHost FanOut Select: E=emulator" );
  this->fanOutSelect( 0, title );
}

// ----------------------------------------------------------------------------

void ElinkConfig::fromHostFanOutSelect()
{
  QString title = QString("GBT FromHost FanOut Select: E=emulator" );
  this->fanOutSelect( 1, title );
}

// ----------------------------------------------------------------------------

void ElinkConfig::fanOutSelect( uint64_t reg_index, QString &title )
{
  // Read the current configuration from the selected FLX-device
  int device_nr = _comboBoxFelix->currentIndex();
  FlxCard felix;
  flxcard_bar2_regs_t *bar2;
  try {
    felix.card_open( device_nr, LOCK_NONE );
    bar2 = (flxcard_bar2_regs_t *) felix.bar2Address();
  }
  catch( FlxException &ex ) {
    QString qs( "Failed to open FLX-device:\n" );
    qs += QString::fromStdString( ex.what() );
    QMessageBox::warning( this, "Read FLX-device configuration", qs );
    return;
  }

  volatile uint64_t *this_reg, *other_reg;
  if( reg_index == 0 )
    {
      this_reg  = (volatile uint64_t *) &bar2->GBT_TOHOST_FANOUT;
      other_reg = (volatile uint64_t *) &bar2->GBT_TOFRONTEND_FANOUT;
    }
  else
    {
      this_reg  = (volatile uint64_t *) &bar2->GBT_TOFRONTEND_FANOUT;
      other_reg = (volatile uint64_t *) &bar2->GBT_TOHOST_FANOUT;
    }
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  //if( bar2->CARD_TYPE == 711 || bar2->CARD_TYPE == 712 ) chans /= 2;
  if( chans > FLX_LINKS ) chans = FLX_LINKS;
  uint64_t select_mask = *this_reg;
  bool     locked = (select_mask & 0x1000000000000);
  // The bit in either register indicates locked state
  locked = locked || ((*other_reg) & 0x1000000000000);

  FanOutSelectDialog diag( this, chans, &select_mask, &locked, title );
  if( diag.exec() == QDialog::Rejected )
    return;

  if( locked )
    select_mask |= 0x1000000000000; // Set 'lock' bit
  *this_reg = select_mask;

  // Update other register's lock bit too..
  select_mask = *other_reg;
  if( locked )
    select_mask |= 0x1000000000000; // Set 'lock' bit
  else
    select_mask &= ~0x1000000000000; // Set 'lock' bit
  *other_reg = select_mask;
}

// ----------------------------------------------------------------------------

void ElinkConfig::timeoutConfig()
{
  // Read the current configuration from the selected FELIX card
  int device_nr = _comboBoxFelix->currentIndex();
  FlxCard felix;
  flxcard_bar2_regs_t *bar2;
  try {
    felix.card_open( device_nr, LOCK_NONE );
    bar2 = (flxcard_bar2_regs_t *) felix.bar2Address();
  }
  catch( FlxException &ex ) {
    QString qs( "Failed to open FLX-device:\n" );
    qs += QString::fromStdString( ex.what() );
    QMessageBox::warning( this, "Read FLX-device configuration", qs );
    return;
  }

  bool timeout_enabled = (bar2->TIMEOUT_CTRL.ENABLE == 1);
  uint64_t timeout_val = bar2->TIMEOUT_CTRL.TIMEOUT;
  uint64_t timeout_max = bar2->MAX_TIMEOUT;
  if( timeout_max > 0 )
    {
      timeout_val %= (timeout_max + 1);
    }
  else
    {
      // Assume time-out value is 10-bit significant
      timeout_val &= 0x3FF;
      timeout_max  = 1023;
    }
  uint64_t timeout_min = 64;
  if( felix.fullmode_type() )
    timeout_min = 1;

  bool ttc_timeout_enabled;
  uint64_t ttc_timeout_val;
#if REGMAP_VERSION < 0x500
  ttc_timeout_enabled = (bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE == 1);
  ttc_timeout_val = bar2->CR_TTC_TOHOST.TIMEOUT_VALUE;
#else
  ttc_timeout_enabled = true;
  ttc_timeout_val = 0;
#endif // REGMAP_VERSION

  TimeoutDialog diag( this, &timeout_enabled, &timeout_val,
                      timeout_min, timeout_max,
                      &ttc_timeout_enabled, &ttc_timeout_val );
  if( diag.exec() == QDialog::Rejected )
    return;

  // Configure (global) time-out
  if( timeout_enabled )
    bar2->TIMEOUT_CTRL.ENABLE = 1;
  else
    bar2->TIMEOUT_CTRL.ENABLE = 0;
  bar2->TIMEOUT_CTRL.TIMEOUT = timeout_val;

  // Configure TTC-to-Host time-out
#if REGMAP_VERSION < 0x500
  if( ttc_timeout_enabled )
    bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE = 1;
  else
    bar2->CR_TTC_TOHOST.TIMEOUT_ENABLE = 0;
  bar2->CR_TTC_TOHOST.TIMEOUT_VALUE = ttc_timeout_val;
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void ElinkConfig::clockConfig()
{
  // Read the current configuration from the selected FELIX card
  int device_nr = _comboBoxFelix->currentIndex();
  FlxCard felix;
  flxcard_bar2_regs_t *bar2;
  try {
    felix.card_open( device_nr, LOCK_NONE );
    bar2 = (flxcard_bar2_regs_t *) felix.bar2Address();
  }
  catch( FlxException &ex ) {
    QString qs( "Failed to open FLX-device:\n" );
    qs += QString::fromStdString( ex.what() );
    QMessageBox::warning( this, "Read FLX-device configuration", qs );
    return;
  }

  bool local_clk = (bar2->MMCM_MAIN.LCLK_SEL == 1);
  bool pll_lock  = (bar2->MMCM_MAIN.PLL_LOCK == 1);

  ClockDialog diag( this, &local_clk, pll_lock );
  if( diag.exec() == QDialog::Rejected )
    return;

  if( local_clk )
    bar2->MMCM_MAIN.LCLK_SEL = 1;
  else
    bar2->MMCM_MAIN.LCLK_SEL = 0;
}

// ----------------------------------------------------------------------------
#if REGMAP_VERSION < 0x500
// ----------------------------------------------------------------------------

// The JSON file keys of the {key,value}-pairs
// NB: from json v3.9.0 there is a type 'ordered_json'
//     preserving the insert order (rather than alphabetical)
static const char *KEY_LINK       = "Link";
static const char *KEY_LINKMODE   = "LinkMode";
static const char *KEY_TTCCLOCK   = "SetTtcClock";
static const char *KEY_CHUNKMAX   = "LinkChunkMaxSize";
static const char *KEY_EGROUPS    = "TheEgroups";
static const char *KEY_EGROUP     = "Egroup";
static const char *KEY_ENA_FH     = "EnableFromHost";
static const char *KEY_ENA_TH     = "EnableToHost";
static const char *KEY_MODE_FH    = "ModeFromHost";
static const char *KEY_MODE_TH    = "ModeToHost";
static const char *KEY_STREAMID   = "StreamId";
static const char *KEY_SETTINGS   = "RegisterSettings";
static const char *KEY_FIELDNAME  = "Name";
static const char *KEY_FIELDVAL   = "Value";

void ElinkConfig::openConfigFile()
{
  QString filename( QFileDialog::getOpenFileName( this,
                                                  "Open Elink config file",
                                                  _cfgDir,
                                                  "JELC-file (*.jelc);;"
                                                  "ELC-file (*.elc);;"
                                                  "All Files (*.*)" ) );
  if ( filename.isEmpty() ) return;

  // Open file
  QFile file( filename );
  if( !this->openFile( file, QIODevice::ReadOnly ) ) return;

  // Read file contents (ASCII)
  QTextStream in( &file );
  int32_t  linknr = 0;
  uint32_t egroup = 0;
  uint32_t enables, chunksizes, streamid;
  uint32_t linkmode = LINKMODE_GBT;
  uint64_t modes;
  bool     ttc_clock;
  if( filename.right(4) == QString("jelc") )
    {
      _regSettings.clear();
      try {
        // Read JSON formatted file
        QString all = in.readAll();
        json j = json::parse( all.toStdString() );

        for( json::iterator it = j.begin(); it != j.end(); ++it )
          {
            json lnk = *it;
            linknr = lnk[KEY_LINK];
            if( linknr > FLX_LINKS ) // Shouldn't read in past this number!
              continue;
            if( linknr == -1 ) // Emulator configuration
              linknr = FLX_LINKS;

            chunksizes = lnk[KEY_CHUNKMAX];
            _gbtConfig[linknr].setMaxChunkWord( chunksizes );

            linkmode = lnk[KEY_LINKMODE];
            _gbtConfig[linknr].setLinkMode( linkmode );

            if( linknr == 0 )
              {
                if( lnk.find( KEY_TTCCLOCK ) != lnk.end() )
                  ttc_clock = lnk[KEY_TTCCLOCK];
                else
                  ttc_clock = false;
                _gbtConfig[linknr].setTtcClock( ttc_clock );
              }

            json egrps = lnk[KEY_EGROUPS];
            istringstream iss;
            for( json::iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
              {
                json egrp = *it2;
                egroup = egrp[KEY_EGROUP];

                // Convert to integers (from a hexadecimal representation)
                enables = 0; modes = 0;
                iss.clear();
                iss.str( egrp[KEY_ENA_TH] );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_TH] );
                iss >> hex >> modes;
                _gbtConfig[linknr].setEnablesToHost( egroup, enables );
                _gbtConfig[linknr].setModesToHost( egroup, modes );

                enables = 0; modes = 0;
                iss.clear();
                iss.str( egrp[KEY_ENA_FH] );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_FH] );
                iss >> hex >> modes;
                _gbtConfig[linknr].setEnablesFromHost( egroup, enables );
                _gbtConfig[linknr].setModesFromHost( egroup, modes );

                // Optional item (for backwards-compatibility, when not there)
                if( egrp.find( KEY_STREAMID ) != egrp.end() )
                  {
                    streamid = 0;
                    iss.clear();
                    iss.str( egrp[KEY_STREAMID] );
                    iss >> hex >> streamid;
                    _gbtConfig[linknr].setStreamIdBits( egroup, streamid );
                  }
              }

            // Any additional FELIX register/bitfield settings present ?
            if( lnk.find( KEY_SETTINGS ) != lnk.end() )
              {
                regsetting_t setting;
                json jregs = lnk[KEY_SETTINGS];
                json::iterator it;
                for( it = jregs.begin(); it != jregs.end(); ++it )
                  {
                    json jsetting = *it;
                    setting.name = jsetting[KEY_FIELDNAME];
                    istringstream iss;
                    iss.str( jsetting[KEY_FIELDVAL] );
                    iss >> hex >> setting.value;

                    // Add the setting to our list
                    _regSettings.push_back( setting );
                  }
              }
          }
      }
      catch( std::exception &ex ) {
        QMessageBox::critical( this, "Reading JELC-file",
                               ex.what() );
      }
    }
  else if( filename.right(4) == QString(".elc") )
    {
      in >> linknr;
      while( !in.atEnd() )
        {
          if( linknr > FLX_LINKS )
            break; // Shouldn't read in more than this!

          if( linknr == -1 ) // Emulator configuration
            linknr = FLX_LINKS;

          in >> egroup >> enables >> modes;

          if( !GbtConfig::enablesIsValid( enables ) )
            {
              QString qs = QString("Link %1, Egroup %2: illegal value" ).
                arg( linknr ).arg( egroup );
              QMessageBox::warning( this, "Reading ELC-file", qs );
              break;
            }

          if( egroup < FLX_TOHOST_GROUPS )
            {
              _gbtConfig[linknr].setEnablesToHost( egroup, enables );
              _gbtConfig[linknr].setModesToHost( egroup, modes );
            }
          else
            {
              if( linknr == FLX_LINKS ) enables = 0; // EMU link
              _gbtConfig[linknr].setEnablesFromHost( egroup-FLX_TOHOST_GROUPS,
                                                     enables );
              _gbtConfig[linknr].setModesFromHost( egroup-FLX_TOHOST_GROUPS,
                                                   modes );
            }

          // 7+1 From-GBT groups followed by 5+1 To-GBT groups
          // followed by chunksize word and linkmode parameter (0, 1 or 2)
          if( egroup == FLX_TOHOST_GROUPS + FLX_FROMHOST_GROUPS - 1 )
            {
              in >> chunksizes;
              _gbtConfig[linknr].setMaxChunkWord( chunksizes );

              // Linkmode value is incremented by 1000
              // to indicate TTC Clock is to be selected
              in >> linkmode;
              if( linknr == 0 )
                {
                  ttc_clock = (linkmode >= 1000);
                  linkmode %= 1000;
                  _gbtConfig[linknr].setTtcClock( ttc_clock );
                }

              _gbtConfig[linknr].setLinkMode( linkmode );

              // Next GBT?
              if( !in.atEnd() )
                in >> linknr;
            }
        }
    }
  else
    {
      QMessageBox::critical( this, "Reading file",
                             "Unhandled file extension" );
    }
  file.close();

  // Configure appropriate link mode checkbox
  if( linkmode == LINKMODE_GBT )
    {
      _radioButtonNormalMode->setChecked( true );
    }
  else if( linkmode == LINKMODE_GBTWIDE )
    {
      _radioButtonWideMode->setChecked( true );
      _checkBoxAdvanced->setChecked( true );
      this->showAdvancedOptions( true );
    }
  else if( linkmode == LINKMODE_LPGBT10_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 0 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 1 );
    }
  else if( linkmode == LINKMODE_LPGBT10_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 2 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 3 );
    }
  else
    {
      _radioButtonFullMode->setChecked( true );
    }

  // Initialize firmware configuration as 'unread' (i.e. all features enabled)
  this->initFwParameters();

  // Update display
  this->applyLinkMode();

  // Re-enable any Egroup widget combobox items disabled
  // because of the previously set/read firmware configuration
  this->reEnableGuiItems();

  // Indicate the currently selected FLX device's configuration is 'unread'
  this->selectedDeviceChanged( 0 );

  // Make E-group 0 the currently selected E-group
  this->changeEgroupToHost( 0 );
  this->changeEgroupFromHost( 0 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::saveConfigFile()
{
  QString filename( _cfgFileName );
  filename = QFileDialog::getSaveFileName( this,
                                           "Save Elink config file",
                                           filename, //_cfgDir,
                                           "JELC-file (*.jelc);;"
                                           "ELC-file (*.elc);;"
                                           "All Files (*.*)" );
  if( filename.isEmpty() ) return;
  if( !filename.contains( '.' ) )
    filename += ".jelc";

  // Open file
  QFile file( filename );
  if( !this->openFile( file, QIODevice::WriteOnly ) ) return;

  // Write elink configuration settings to file (ASCII)
  QTextStream out( &file );
  GbtConfig  *cfg;
  uint32_t    linknr, egroup;
  if( filename.right(4) == QString("jelc") )
    {
      // Store file in JSON format
      ordered_json jmain;
      for( linknr=0; linknr<=FLX_LINKS; ++linknr )
        {
          ordered_json jo;
          int lnk;
          if( linknr == FLX_LINKS ) // Emulator configuration
            lnk = -1;
          else
            lnk = linknr;
          jo[KEY_LINK] = lnk;

          std::string ena_th, mode_th;
          std::string ena_fh, mode_fh;
          std::string streamid;
          cfg = &_gbtConfig[linknr];

          jo[KEY_LINKMODE] = cfg->linkMode();
          jo[KEY_CHUNKMAX] = cfg->maxChunkWord();

          if( linknr == 0 )
            jo[KEY_TTCCLOCK] = cfg->ttcClock();

          std::ostringstream oss;
          ordered_json jgroup;
          for( egroup=0; egroup<FLX_TOHOST_GROUPS; ++egroup )
            {
              oss << setfill('0') << hex;

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(4) << cfg->enablesToHost( egroup );
              ena_th = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(15) << cfg->modesToHost( egroup );
              mode_th = oss.str();

              if( egroup < FLX_FROMHOST_GROUPS && lnk >= 0 )
                {
                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(4) << cfg->enablesFromHost( egroup );
                  ena_fh = oss.str();

                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(15) << cfg->modesFromHost( egroup );
                  mode_fh = oss.str();
                }
              else
                {
                  ena_fh = std::string( "" );
                  mode_fh = std::string( "" );
                }

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(2) << cfg->streamIdBits( egroup );
              streamid = oss.str();

              ordered_json o_sub =
                { { KEY_EGROUP,   egroup },
                  { KEY_ENA_TH,   ena_th },
                  { KEY_MODE_TH,  mode_th },
                  { KEY_STREAMID, streamid },
                  { KEY_ENA_FH,   ena_fh },
                  { KEY_MODE_FH,  mode_fh }
                };
              jgroup.push_back( o_sub );
            }

          // NB: order in the dump() is alphabetical,
          // rather than insertion order (which I find particularly BAD)
          // so "Egroups" has been renamed to "TheEgroups"
          // NB: now fixed in new json version by 'ordered_json' type
          jo[KEY_EGROUPS] = jgroup;

          // In case of additional FELIX register or bitfield settings,
          // append them to Link 0 settings
          if( _regSettings.size() > 0 && linknr == 0 )
            {
              json jregs;
              for( regsetting_t r : _regSettings )
                {
                  ostringstream oss;
                  oss << "0x" << hex << uppercase << r.value;
                  json jsetting = { { KEY_FIELDNAME, r.name },
                                    { KEY_FIELDVAL,  oss.str() } };
                  jregs.push_back( jsetting );
                }
              jo[KEY_SETTINGS] = jregs;
            }

          // Add it to 'jmain'
          jmain.push_back( jo );
        }

      // Store the compiled JSON string in the file
      std::string str = jmain.dump( 2 );
      out << str.c_str();
    }
  else
    {
      for( linknr=0; linknr<=FLX_LINKS; ++linknr )
        {
          if( linknr == FLX_LINKS ) // Emulator configuration
            out << QString( "-1\n" );
          else
            out << QString( "%1\n" ).arg( linknr );

          cfg = &_gbtConfig[linknr];
          for( egroup=0; egroup<FLX_TOHOST_GROUPS; ++egroup )
            out << QString( "%1  " ).arg( egroup, 2 )
                << QString( "0x%1  " ).
              arg( cfg->enablesToHost( egroup ), 4, 16, QChar('0') )
                << QString( "0x%1\n" ).
              arg( cfg->modesToHost( egroup ), 15, 16, QChar('0') );

          for( egroup=0; egroup<FLX_FROMHOST_GROUPS; ++egroup )
            out << QString( "%1  " ).arg( egroup+FLX_TOHOST_GROUPS, 2 )
                << QString( "0x%1  " ).
              arg( cfg->enablesFromHost( egroup ), 4, 16, QChar('0') )
                << QString( "0x%1\n" ).
              arg( cfg->modesFromHost( egroup ), 15, 16, QChar('0') );

          out << QString( "0x%1" ).arg( cfg->maxChunkWord(),3,16,QChar('0') );
          // Increment link mode by 1000 to indicate TTC Clock setting
          if( linknr == 0 && cfg->ttcClock() )
            out << QString( "  %1" ).arg( cfg->linkMode() + 1000 );
          else
            out << QString( "  %1" ).arg( cfg->linkMode() );
          out << "\n\n";
        }

    }
  file.close();
  QString info_str = (QString( "File saved: " )
                      + QString(filename) + QString( "          " ));
  QMessageBox::information( this, "Saving (J)ELC-file",
                            info_str );
  //                        "File saved          " );
}

// ----------------------------------------------------------------------------
#else
// ----------------------------------------------------------------------------

// The YAML (or JSON?) file keys of the {key,value}-pairs
// NB: from json v3.9.0 onwards there is a type 'ordered_json'
//     preserving the insert order (rather than alphabetical)
static const char *KEY_FORMAT     = "Format";
static const char *KEY_LINKS      = "Links";
static const char *KEY_LINK       = "Link";
static const char *KEY_LINKMODE   = "LinkMode";
static const char *KEY_LTITTC     = "LtiTtc";
static const char *KEY_TTCCLOCK   = "SetTtcClock";
static const char *KEY_CHUNKMAX   = "LinkChunkMaxSize";
static const char *KEY_EGROUPS    = "Egroups";   //.jelc backwards compatibility
static const char *KEY_EGROUPS_X  = "TheEgroups";//.jelc backwards compatibility
static const char *KEY_EGROUP     = "Egroup";
static const char *KEY_EGROUPS_TH = "EgroupsToHost";
static const char *KEY_WIDTH_TH   = "WidthToHost";
static const char *KEY_ENA_TH     = "EnableToHost";
static const char *KEY_MODE_TH    = "ModeToHost";
static const char *KEY_STREAMID   = "StreamId";
static const char *KEY_DMAINDICES = "DmaIndices";
static const char *KEY_EGROUPS_FH = "EgroupsFromHost";
static const char *KEY_WIDTH_FH   = "WidthFromHost";
static const char *KEY_ENA_FH     = "EnableFromHost";
static const char *KEY_MODE_FH    = "ModeFromHost";
static const char *KEY_TTCOPTION  = "TtcOption";
static const char *KEY_SETTINGS   = "RegisterSettings";
static const char *KEY_FIELDNAME  = "Name";
static const char *KEY_FIELDVAL   = "Value";

void ElinkConfig::openConfigFile()
{
  // Clear the file name line edit
  _lineEditFileName->setText( "" );

  QString filename( QFileDialog::getOpenFileName( this,
                                                  "Open Elink config file",
                                                  _cfgDir,
                                                  "YELC-file (*.yelc);;"
                                                  //"JELC-file (*.jelc);;"
                                                  "All Files (*.*)" ) );
  if ( filename.isEmpty() ) return;

  // Open file
  QFile file( filename );
  if( !this->openFile( file, QIODevice::ReadOnly ) ) return;

  // Read file contents (ASCII)
  QTextStream in( &file );
  int32_t  linknr = 0;
  uint32_t egroup = 0;
  uint32_t width, enables, chunksizes, streamid, dmaindices;
  uint32_t linkmode = LINKMODE_GBT;
  bool     lti_ttc, ttc_clock;
  uint64_t modes;
  uint32_t ttc_option = 0;
  if( filename.right(4) == QString("yelc") )
    {
      try {
        QString all = in.readAll();
        YAML::Node yaml = YAML::Load( all.toStdString() );
        if( !yaml[KEY_FORMAT] )
          {
            QMessageBox::critical( this, "Reading YELC-file",
                                   "Wrong format (\"Format\" missing)" );
            return;
          }
        int format = yaml[KEY_FORMAT].as<int>();
        if( format != 2 )
          {
            QMessageBox::critical( this, "Reading YELC-file",
                                   QString("Require \"Format\" 2, got %1    ").
                                   arg( format ) );
            return;
          }

        YAML::Node links = yaml[KEY_LINKS];
        for( YAML::const_iterator it = links.begin(); it != links.end(); ++it )
          {
            YAML::Node lnk = *it;
            linknr = lnk[KEY_LINK].as<int>();
            if( linknr > FLX_LINKS ) // Shouldn't read in past this number!
              continue;
            if( linknr == -1 ) // Emulator configuration
              linknr = FLX_LINKS;

            chunksizes = lnk[KEY_CHUNKMAX].as<int>();
            _gbtConfig[linknr].setMaxChunkWord( chunksizes );

            linkmode = lnk[KEY_LINKMODE].as<int>();
            _gbtConfig[linknr].setLinkMode( linkmode );

            if( lnk[KEY_LTITTC] )
              lti_ttc = lnk[KEY_LTITTC].as<bool>();
            else
              lti_ttc = false;
            _gbtConfig[linknr].setLtiTtc( lti_ttc );

            if( linknr == 0 )
              {
                if( lnk[KEY_TTCCLOCK] )
                  ttc_clock = lnk[KEY_TTCCLOCK].as<bool>();
                else
                  ttc_clock = false;
                _gbtConfig[linknr].setTtcClock( ttc_clock );
              }

            YAML::Node egrps = lnk[KEY_EGROUPS_TH];
            istringstream iss;
            for( YAML::const_iterator it2 = egrps.begin();
                 it2 != egrps.end(); ++it2 )
              {
                YAML::Node egrp = *it2;
                egroup = egrp[KEY_EGROUP].as<int>();

                // Convert to integers (some from a hexadecimal representation)
                width = 0, enables = 0; modes = 0;
                width = egrp[KEY_WIDTH_TH].as<int>();
                iss.clear();
                iss.str( egrp[KEY_ENA_TH].as<std::string>() );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_TH].as<std::string>() );
                iss >> hex >> modes;
                _gbtConfig[linknr].setWidthToHost( egroup, width );
                _gbtConfig[linknr].setEnablesToHost( egroup, enables );
                _gbtConfig[linknr].setModesToHost( egroup, modes );
                //if( linknr == 0 ) //###DEBUG
                //QMessageBox::information( this, "EGROUP TH-ENA YELC-file",
                //                          QString::number(egroup) +
                //                          QString( " - ") +
                //                          QString::number(enables,16) );

                // Optional item (for backwards-compatibility)
                if( egrp[KEY_DMAINDICES] )
                  {
                    dmaindices = 0;
                    iss.clear();
                    iss.str( egrp[KEY_DMAINDICES].as<std::string>() );
                    iss >> hex >> dmaindices;
                    _gbtConfig[linknr].setDmaIndices( egroup, dmaindices );
                  }

                streamid = 0;
                iss.clear();
                iss.str( egrp[KEY_STREAMID].as<std::string>() );
                iss >> hex >> streamid;
                _gbtConfig[linknr].setStreamIdBits( egroup, streamid );
              }

            egrps = lnk[KEY_EGROUPS_FH];
            for( YAML::const_iterator it2 = egrps.begin();
                 it2 != egrps.end(); ++it2 )
              {
                YAML::Node egrp = *it2;
                egroup = egrp[KEY_EGROUP].as<int>();

                // Convert to integers (some from a hexadecimal representation)
                width = 0; enables = 0; modes = 0;
                width = egrp[KEY_WIDTH_FH].as<int>();
                iss.clear();
                iss.str( egrp[KEY_ENA_FH].as<std::string>() );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_FH].as<std::string>() );
                iss >> hex >> modes;
                if( egrp[KEY_TTCOPTION] )
                  {
                    ttc_option = egrp[KEY_TTCOPTION].as<int>();
                    _gbtConfig[linknr].setTtcOptionFromHost( egroup, ttc_option );
                  }
                _gbtConfig[linknr].setWidthFromHost( egroup, width );
                _gbtConfig[linknr].setEnablesFromHost( egroup, enables );
                _gbtConfig[linknr].setModesFromHost( egroup, modes );
              }
          }

        // Additional FELIX register settings
        _regSettings.clear();
        YAML::Node settings = yaml[KEY_SETTINGS];
        regsetting_t regset;
        for( YAML::const_iterator it = settings.begin();
             it != settings.end(); ++it )
          {
            YAML::Node setting = *it;
            std::string field = setting[KEY_FIELDNAME].as<std::string>();
            uint64_t value = setting[KEY_FIELDVAL].as<uint64_t>();
            // Convert fieldname to uppercase and replace '-' by '_'
            char *c = field.data();
            for( size_t i=0; i<field.size(); ++i, ++c )
              {
                *c = toupper( *c );
                if( *c == '-' ) *c = '_';
              }
            regset.name = field;
            regset.value = value;

            // Add the setting to our list
            _regSettings.push_back( regset );
          }
      }
      catch( std::exception &ex ) {
        QMessageBox::critical( this, "Reading YELC-file",
                               //"Error in file          " );
                               ex.what() );
      }
    }
  else if( filename.right(4) == QString("jelc") )
    {
      try {
        // Read JSON formatted file (this is to become obsolete)
        QString all = in.readAll();
        json j = json::parse( all.toStdString() );

        // "Format" key should be there...
        if( j.find( KEY_FORMAT ) == j.end() )
          {
            QMessageBox::critical( this, "Reading JELC-file",
                                   "Wrong format (\"Format\" missing)" );
            return;
          }
        //int format = j[KEY_FORMAT];

        // Any additional FELIX register/bitfield settings?
        _regSettings.clear();
        if( j.find( KEY_SETTINGS ) != j.end() )
          {
            regsetting_t setting;
            json jregs = j[KEY_SETTINGS];
            json::iterator it;
            for( it = jregs.begin(); it != jregs.end(); ++it )
              {
                json jsetting = *it;
                setting.name = jsetting[KEY_FIELDNAME];
                istringstream iss;
                iss.str( jsetting[KEY_FIELDVAL] );
                iss >> hex >> setting.value;

                // Add the setting to our list
                _regSettings.push_back( setting );
              }
          }

        for( json::iterator it = j[KEY_LINKS].begin();
             it != j[KEY_LINKS].end(); ++it )
          {
            json lnk = *it;
            linknr = lnk[KEY_LINK];
            if( linknr > FLX_LINKS ) // Shouldn't read in past this number!
              continue;
            if( linknr == -1 ) // Emulator configuration
              linknr = FLX_LINKS;

            chunksizes = lnk[KEY_CHUNKMAX];
            _gbtConfig[linknr].setMaxChunkWord( chunksizes );

            linkmode = lnk[KEY_LINKMODE];
            _gbtConfig[linknr].setLinkMode( linkmode );

            if( linknr == 0 )
              {
                if( lnk.find( KEY_TTCCLOCK ) != lnk.end() )
                  ttc_clock = lnk[KEY_TTCCLOCK];
                else
                  ttc_clock = false;
                _gbtConfig[linknr].setTtcClock( ttc_clock );
              }

            json egrps;
            if( lnk.find( KEY_EGROUPS ) != lnk.end() )
              egrps = lnk[KEY_EGROUPS];
            else
              egrps = lnk[KEY_EGROUPS_X]; // Old keyword
            istringstream iss;
            for( json::iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
              {
                json egrp = *it2;
                egroup = egrp[KEY_EGROUP];

                // Convert to integers (some from a hexadecimal representation)
                width = 0, enables = 0; modes = 0;
                width = egrp[KEY_WIDTH_TH];
                iss.clear();
                iss.str( egrp[KEY_ENA_TH] );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_TH] );
                iss >> hex >> modes;
                _gbtConfig[linknr].setWidthToHost( egroup, width );
                _gbtConfig[linknr].setEnablesToHost( egroup, enables );
                _gbtConfig[linknr].setModesToHost( egroup, modes );

                width = 0; enables = 0; modes = 0;
                width = egrp[KEY_WIDTH_FH];
                iss.clear();
                iss.str( egrp[KEY_ENA_FH] );
                iss >> hex >> enables;
                iss.clear();
                iss.str( egrp[KEY_MODE_FH] );
                iss >> hex >> modes;
                _gbtConfig[linknr].setWidthFromHost( egroup, width );
                _gbtConfig[linknr].setEnablesFromHost( egroup, enables );
                _gbtConfig[linknr].setModesFromHost( egroup, modes );

                // Optional item (for backwards-compatibility)
                if( egrp.find( KEY_DMAINDICES ) != egrp.end() )
                  {
                    dmaindices = 0;
                    iss.clear();
                    iss.str( egrp[KEY_DMAINDICES] );
                    iss >> hex >> dmaindices;
                    _gbtConfig[linknr].setDmaIndices( egroup, dmaindices );
                  }

                // Optional item (for backwards-compatibility)
                if( egrp.find( KEY_STREAMID ) != egrp.end() )
                  {
                    streamid = 0;
                    iss.clear();
                    iss.str( egrp[KEY_STREAMID] );
                    iss >> hex >> streamid;
                    _gbtConfig[linknr].setStreamIdBits( egroup, streamid );
                  }

                // Optional item (for backwards-compatibility)
                if( egrp.find( KEY_TTCOPTION ) != egrp.end() )
                  {
                    ttc_option = egrp[KEY_TTCOPTION];
                    _gbtConfig[linknr].setTtcOptionFromHost( egroup, ttc_option );
                  }
              }
          }
      }
      catch( std::exception &ex ) {
        QMessageBox::critical( this, "Reading JELC-file",
                               //"Error in file          " );
                               ex.what() );
      }
    }
  else
    {
      QMessageBox::critical( this, "Reading file",
                             "Unhandled file extension" );
    }
  file.close();

  // Configure appropriate link mode checkbox
  if( linkmode == LINKMODE_GBT )
    {
      _radioButtonNormalMode->setChecked( true );
    }
  else if( linkmode == LINKMODE_GBTWIDE )
    {
      _radioButtonWideMode->setChecked( true );
      _checkBoxAdvanced->setChecked( true );
      this->showAdvancedOptions( true );
    }
  else if( linkmode == LINKMODE_LPGBT10_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 0 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F5 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5Gbps-FEC5" );
      _comboLpGbtMode->setCurrentIndex( 1 );
    }
  else if( linkmode == LINKMODE_LPGBT10_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-10Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 2 );
    }
  else if( linkmode == LINKMODE_LPGBT5_F12 )
    {
      _radioButtonLpGbtMode->setChecked( true );
      _radioButtonLpGbtMode->setToolTip( "Link type: lpGBT-5Gbps-FEC12" );
      _comboLpGbtMode->setCurrentIndex( 3 );
    }
  else
    {
      _radioButtonFullMode->setChecked( true );
    }

  // Initialize firmware configuration as 'unread' (i.e. all features enabled)
  this->initFwParameters();

  // Update display
  this->applyLinkMode();

  // Re-enable any Egroup widget combobox items disabled
  // because of the previously set/read firmware configuration
  this->reEnableGuiItems();

  // Indicate the currently selected FLX device's configuration is 'unread'
  this->selectedDeviceChanged( 0 );

  // Make E-group 0 the currently selected E-group
  this->changeEgroupToHost( 0 );
  this->changeEgroupFromHost( 0 );
}

// ----------------------------------------------------------------------------

void ElinkConfig::saveConfigFile()
{
  // Clear the file name line edit
  _lineEditFileName->setText( "" );

  QString filename( _cfgFileName );
  filename = QFileDialog::getSaveFileName( this,
                                           "Save Elink config file",
                                           filename, //_cfgDir,
                                           "YELC-file (*.yelc);;"
                                           //"JELC-file (*.jelc);;"
                                           "All Files (*.*)" );
  if( filename.isEmpty() ) return;
  if( !filename.contains( '.' ) )
    filename += ".yelc";

  // Open file
  QFile file( filename );
  if( !this->openFile( file, QIODevice::WriteOnly ) ) return;

  // Write elink configuration settings to file (ASCII)
  QTextStream out( &file );
  GbtConfig  *cfg;
  uint32_t    linknr, egroup;
  if( filename.right(4) == QString("yelc") )
    {
      // Store file in YAML format
      YAML::Emitter yaml;
      yaml << YAML::BeginMap;
      yaml << YAML::Key << KEY_FORMAT << YAML::Value << 2;

      // If there are any FELIX register settings that need to be preserved
      // write them here in the file (near the beginning of the file)
      if( _regSettings.size() > 0 )
        {
          yaml << YAML::Key << KEY_SETTINGS
               << YAML::Value; // "RegisterSettings:"
          yaml << YAML::BeginSeq;
          for( regsetting_t r : _regSettings )
            {
              yaml << YAML::BeginMap;
              yaml << YAML::Key << KEY_FIELDNAME
                   << YAML::Value << r.name
                   << YAML::Key << KEY_FIELDVAL
                   << YAML::Value;
              ostringstream oss;
              oss << "0x" << hex << uppercase << r.value;
              yaml << oss.str();
              yaml << YAML::EndMap;
            }
          yaml << YAML::EndSeq;
        }

      yaml << YAML::Key << KEY_LINKS << YAML::Value; // "Links:"
      yaml << YAML::BeginSeq;
      for( linknr=0; linknr<=FLX_LINKS; ++linknr )
        {
          int lnk;
          if( linknr == FLX_LINKS ) // Emulator configuration
            lnk = -1;
          else
            lnk = linknr;

          cfg = &_gbtConfig[linknr];

          yaml << YAML::BeginMap;
          yaml << YAML::Key   << KEY_LINK
               << YAML::Value << lnk;

          yaml << YAML::Key   << KEY_LINKMODE
               << YAML::Value << cfg->linkMode()
               << YAML::Key   << KEY_CHUNKMAX
               << YAML::Value << cfg->maxChunkWord();

          if( cfg->linkMode() == LINKMODE_FULL )
            yaml << YAML::Key   << KEY_LTITTC
                 << YAML::Value << cfg->ltiTtc();

          if( linknr == 0 )
            yaml << YAML::Key   << KEY_TTCCLOCK
                 << YAML::Value << cfg->ttcClock();

          // "EgroupsToHost:"
          yaml << YAML::Key << KEY_EGROUPS_TH << YAML::Value;
          yaml << YAML::BeginSeq;
          std::string ena_th, mode_th;
          std::string dmaindices;
          std::string streamid;
          std::ostringstream oss;
          for( egroup=0; egroup<FLX_TOHOST_GROUPS; ++egroup )
            {
              oss << setfill('0') << hex;

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(4) << cfg->enablesToHost( egroup );
              ena_th = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(15) << cfg->modesToHost( egroup );
              mode_th = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(2) << cfg->streamIdBits( egroup );
              streamid = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(8) << cfg->dmaIndices( egroup );
              dmaindices = oss.str();

              yaml << YAML::BeginMap;
              yaml << YAML::Key   << KEY_EGROUP
                   << YAML::Value << egroup

                   << YAML::Key   << KEY_WIDTH_TH
                   << YAML::Value << cfg->widthToHost( egroup )
                   << YAML::Key   << KEY_ENA_TH
                   << YAML::Value << ena_th
                   << YAML::Key   << KEY_MODE_TH
                   << YAML::Value << mode_th
                   << YAML::Key   << KEY_DMAINDICES
                   << YAML::Value << dmaindices
                   << YAML::Key   << KEY_STREAMID
                   << YAML::Value << streamid;
              yaml << YAML::EndMap;
            }
          yaml << YAML::EndSeq;

          // "EgroupsFromHost:"
          yaml << YAML::Key << KEY_EGROUPS_FH << YAML::Value;
          yaml << YAML::BeginSeq;
          std::string ena_fh, mode_fh;
          for( egroup=0; egroup<FLX_FROMHOST_GROUPS; ++egroup )
            {
              if( lnk >= 0 )
                {
                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(4) << cfg->enablesFromHost( egroup );
                  ena_fh = oss.str();

                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(15) << cfg->modesFromHost( egroup );
                  mode_fh = oss.str();
                }
              else
                {
                  ena_fh = std::string( "" );
                  mode_fh = std::string( "" );
                }

              yaml << YAML::BeginMap;
              yaml << YAML::Key   << KEY_EGROUP
                   << YAML::Value << egroup

                   << YAML::Key   << KEY_WIDTH_FH
                   << YAML::Value << cfg->widthFromHost( egroup )
                   << YAML::Key   << KEY_ENA_FH
                   << YAML::Value << ena_fh
                   << YAML::Key   << KEY_MODE_FH
                   << YAML::Value << mode_fh
                   << YAML::Key   << KEY_TTCOPTION
                   << YAML::Value << cfg->ttcOptionFromHost( egroup );
              yaml << YAML::EndMap;
            }
          yaml << YAML::EndSeq;

          yaml << YAML::EndMap;
        }
      yaml << YAML::EndSeq;
      yaml << YAML::EndMap;
      out << yaml.c_str();
    }
  else if( filename.right(4) == QString("jelc") )
    {
      // Store file in JSON format (this is to become obsolete)
      ordered_json jmain;
      jmain[KEY_FORMAT] = 1;

      // Add additional FELIX register or bitfield settings, if any
      if( _regSettings.size() > 0 )
        {
          json jregs;
          for( regsetting_t r : _regSettings )
            {
              ostringstream oss;
              oss << "0x" << hex << uppercase << r.value;
              json jsetting = { { KEY_FIELDNAME, r.name },
                                { KEY_FIELDVAL,  oss.str() } };
              jregs.push_back( jsetting );
            }
          jmain[KEY_SETTINGS] = jregs;
        }

      for( linknr=0; linknr<=FLX_LINKS; ++linknr )
        {
          ordered_json jo;
          int lnk;
          if( linknr == FLX_LINKS ) // Emulator configuration
            lnk = -1;
          else
            lnk = linknr;
          jo[KEY_LINK] = lnk;

          ordered_json jgroup;
          std::string ena_th, mode_th;
          std::string ena_fh, mode_fh;
          std::string dmaindices;
          std::string streamid;
          cfg = &_gbtConfig[linknr];

          jo[KEY_LINKMODE] = cfg->linkMode();
          jo[KEY_CHUNKMAX] = cfg->maxChunkWord();

          if( linknr == 0 )
            jo[KEY_TTCCLOCK] = cfg->ttcClock();

          std::ostringstream oss;
          for( egroup=0; egroup<FLX_TOHOST_GROUPS; ++egroup )
            {
              oss << setfill('0') << hex;

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(4) << cfg->enablesToHost( egroup );
              ena_th = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(15) << cfg->modesToHost( egroup );
              mode_th = oss.str();

              if( egroup < FLX_FROMHOST_GROUPS && lnk >= 0 )
                {
                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(4) << cfg->enablesFromHost( egroup );
                  ena_fh = oss.str();

                  oss.clear(); oss.str( "" ); // Reset
                  oss << "0x" << setw(15) << cfg->modesFromHost( egroup );
                  mode_fh = oss.str();
                }
              else
                {
                  ena_fh = std::string( "" );
                  mode_fh = std::string( "" );
                }

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(8) << cfg->dmaIndices( egroup );
              dmaindices = oss.str();

              oss.clear(); oss.str( "" ); // Reset
              oss << "0x" << setw(2) << cfg->streamIdBits( egroup );
              streamid = oss.str();

              ordered_json o_sub =
                { { KEY_EGROUP, egroup },
                  { KEY_WIDTH_TH, cfg->widthToHost( egroup ) },
                  { KEY_ENA_TH, ena_th },
                  { KEY_MODE_TH, mode_th },
                  { KEY_DMAINDICES, dmaindices },
                  { KEY_STREAMID, streamid },
                  { KEY_WIDTH_FH, cfg->widthFromHost( egroup ) },
                  { KEY_ENA_FH, ena_fh },
                  { KEY_MODE_FH, mode_fh },
                  { KEY_TTCOPTION, cfg->ttcOptionFromHost( egroup ) }
                };
              jgroup.push_back( o_sub );
            }

          // NB: order in the dump() is alphabetical,
          // rather than insertion order (which I find particularly BAD)
          // so "Egroups" has been renamed to "TheEgroups"
          // NB: now fixed in new json version by 'ordered_json' type
          jo[KEY_EGROUPS] = jgroup;

          // Add it to the 'j[KEY_LINKS]' array
          jmain[KEY_LINKS].push_back( jo );
        }
      // Store the compiled JSON string in the file
      std::string str = jmain.dump( 2 );
      out << str.c_str();
    }
  file.close();
  QString info_str = (QString( "File saved: " )
                      + QString(filename) + QString( "          " ));
  QMessageBox::information( this, "Saving YELC-file",
                            info_str );
}

// ----------------------------------------------------------------------------
#endif // REGMAP_VERSION
// ----------------------------------------------------------------------------

bool ElinkConfig::openFile( QFile &file, QIODevice::OpenMode mode )
{
  if( !file.open( mode | QIODevice::Text ) )
    {
      // (Added extra spaces to the short string to make sure
      //  the messagebox title is visible)
      QMessageBox::warning( this, "Opening (J)ELC-file",
                            "Could not open file          " );
      return false;
    }

  // Show and remember file name
  QString filename = file.fileName();
  _lineEditFileName->setText( filename );
  _lineEditFileName->setEnabled( true );
  _cfgFileName = filename;

  // Remember the path for next time
  QFileInfo f_info( filename );
  _cfgDir = f_info.absolutePath();

  return true;
}

// ----------------------------------------------------------------------------

void ElinkConfig::showSettingsDialog()
{
  SettingsDialog diag( this, &_regSettings );
  diag.exec();
}

// ----------------------------------------------------------------------------

void ElinkConfig::showGenerateDialog()
{
  int device_nr = _comboBoxFelix->currentIndex();
  GenerateDialog diag( this, device_nr, _gbtNr,
                       _spinBoxLinkNr->maximum(), _gbtConfig,
                       &_regSettings );
  diag.exec();
}

// ----------------------------------------------------------------------------

void ElinkConfig::showMessage( QString qs )
{
  _labelMessage->setText( qs );
  _labelMessage->show();
  QTimer::singleShot( 3000, this, SLOT(hideMessage()) );
}

// ----------------------------------------------------------------------------

void ElinkConfig::hideMessage()
{
  _labelMessage->hide();
}

// ----------------------------------------------------------------------------

void ElinkConfig::readSettings()
{
  QSettings settings( "NIKHEF", "elinkconfig" );
  _cfgDir = settings.value( "configDir", "." ).toString();
  _cfgFileName = _cfgDir;
}

// ----------------------------------------------------------------------------

void ElinkConfig::writeSettings()
{
  QSettings settings( "NIKHEF", "elinkconfig" );
  settings.setValue( "configDir", _cfgDir );
}

// ----------------------------------------------------------------------------

void ElinkConfig::closeEvent( QCloseEvent *event )
{
  // When quitting the application save some of the current settings
  this->writeSettings();
  event->accept();
}

// ----------------------------------------------------------------------------
// Convenience functions for Qt: disable/enable a selected combobox item
// https://stackoverflow.com/questions/38915001/disable-specific-items-in-qcombobox

#include <QStandardItemModel>

void ElinkConfig::setComboItemEnabled( QComboBox *comboBox,
                                       int        index,
                                       bool       enabled )
{
  auto * model = qobject_cast<QStandardItemModel*>( comboBox->model() );
  //assert(model);
  if( !model ) return;

  auto * item = model->item( index );
  //assert(item);
  if( !item ) return;
  item->setEnabled( enabled );
}

bool ElinkConfig::comboItemEnabled( QComboBox *comboBox,
                                    int        index )
{
  auto * model = qobject_cast<QStandardItemModel*>( comboBox->model() );
  //assert(model);
  if( !model ) return false;

  auto * item = model->item( index );
  //assert(item);
  if( !item ) return false;
  return item->isEnabled();
}

// ----------------------------------------------------------------------------

void ElinkConfig::initFwParameters()
{
  // Initialize firmware configuration as 'unread' (i.e. all features enabled)
  // (_fwMode initialized in applyLinkMode())
  _fwDirectModeIncluded       = false; // Skip 'direct mode': never used
  _fwDmaToHostDescriptorCount = 5;     // 5x ToHost

  for( uint64_t group=0; group<7; ++group )
    {
      u_long *incl = (u_long *) &_fwEgroupIncludes[group];
      *incl = 0xFFFF;
    }
  for( uint64_t link=0; link<FLX_LINKS; ++link )
    //_fwElinksAligned[link] = 0x603050A0F0+link; // TEST
    _fwElinksAligned[link] = 0;

  _itkStripLcbR3l1ElinkSwap = 0;
}

// ----------------------------------------------------------------------------

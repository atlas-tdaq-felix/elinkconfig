#include "ClockDialog.h"
#include <QIntValidator>

// ----------------------------------------------------------------------------

ClockDialog::ClockDialog( QWidget *parent,
                          bool    *local_clk,
                          bool     pll_lock )
  : QDialog( parent ),
    _localClk( local_clk )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );
  this->setWindowTitle( "Clock Configuration" );

  _radioButtonLocal->setChecked( *local_clk );
  _radioButtonTtc->setChecked( !(*local_clk) );
  _checkBoxPllLock->setChecked( pll_lock );

  connect( _pushButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( _pushButtonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

// ----------------------------------------------------------------------------

ClockDialog::~ClockDialog()
{
}

// ----------------------------------------------------------------------------

void ClockDialog::accept()
{
  *_localClk = _radioButtonLocal->isChecked();
  QDialog::accept();
}

// ----------------------------------------------------------------------------

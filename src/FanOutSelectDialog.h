#ifndef FANOUTSELECTDIALOG_H
#define FANOUTSELECTDIALOG_H

#include "ui_ReplicateDialog.h"
#include <QDialog>
#include <QString>
#include <vector>
class QCheckBox;

class FanOutSelectDialog: public QDialog, Ui_ReplicateDialog
{
  Q_OBJECT

 public:
  FanOutSelectDialog( QWidget  *parent,
		      uint64_t  range,
		      uint64_t *select_mask,
		      bool     *locked,
		      QString   title );
  ~FanOutSelectDialog();

 public slots:
   void updateButtons();
   void selectAll();
   void deselectAll();

   void accept();

 private:
   uint64_t  _range;
   uint64_t *_selectMask;
   bool     *_locked;
   std::vector<QPushButton *> _buttons;  
   QCheckBox *_cb;
};

#endif // FANOUTSELECTDIALOG_H

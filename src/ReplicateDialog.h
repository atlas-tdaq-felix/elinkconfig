#ifndef REPLICATEDIALOG_H
#define REPLICATEDIALOG_H

#include "ui_ReplicateDialog.h"
#include <QDialog>
#include <QString>
#include <vector>

class ReplicateDialog: public QDialog, Ui_ReplicateDialog
{
  Q_OBJECT

 public:
  ReplicateDialog( QWidget  *parent,
		   uint32_t  ref_nr,
		   uint32_t  range,
		   uint32_t *selection_mask,
		   QString   title );
  ~ReplicateDialog();

 public slots:
   void selectAll();
   void deselectAll();

   void accept();

 private:
  uint32_t  _refNr;
  uint32_t  _range;
  uint32_t *_selectionMask;
  std::vector<QPushButton *> _buttons;  
};

#endif // REPLICATEDIALOG_H

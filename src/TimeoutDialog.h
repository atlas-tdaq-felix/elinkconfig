#ifndef TIMEOUTDIALOG_H
#define TIMEOUTDIALOG_H

#include "ui_TimeoutDialog.h"
#include <QDialog>

class TimeoutDialog: public QDialog, Ui_TimeoutDialog
{
  Q_OBJECT

 public:
  TimeoutDialog( QWidget  *parent,
                 bool     *timeout_enabled,
                 uint64_t *timeout_val,
                 uint64_t  timeout_min,
                 uint64_t  timeout_max,
                 bool     *ttc_timeout_enabled,
                 uint64_t *ttc_timeout_val );
  ~TimeoutDialog();

 public slots:
   void accept();

 private:
   bool          *_timeoutEnabled;
   uint64_t      *_timeoutVal;
   bool          *_ttcTimeoutEnabled;
   uint64_t      *_ttcTimeoutVal;
};

#endif // TIMEOUTDIALOG_H

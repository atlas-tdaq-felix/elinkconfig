#include <QApplication>
#include "ElinkConfig.h"

int main(int argc, char *argv[])
{
  QApplication app( argc, argv );
  ElinkConfig econfig;
  econfig.show();
  return app.exec();  
}

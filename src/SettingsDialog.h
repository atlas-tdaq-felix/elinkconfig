#ifndef SETTINGSDIALOG_H
#define SETTTINGSDIALOG_H

#include "ui_SettingsDialog.h"
#include <QDialog>

#if REGMAP_VERSION < 0x500
#include "GbtConfig.h"
#else
#include "GbtConfig5.h"
#endif // REGMAP_VERSION

class SettingsDialog: public QDialog, Ui_SettingsDialog
{
  Q_OBJECT

public:
  SettingsDialog( QWidget *parent,
                  std::vector<regsetting_t> *settings );
  ~SettingsDialog();
                   
  void resizeEvent( QResizeEvent *event );

public slots:
  void addSetting();
  void itemClicked( QTableWidgetItem *item );
  void itemDoubleClicked( QTableWidgetItem *item );
  void itemChanged( QTableWidgetItem *item );
  void itemSelectionChanged();
  void moveUp();
  void moveDown();
  void resizeIt();
  void accept();
  
private:
  std::vector<regsetting_t> *_settings;
  QString _itemTextCopy;
};

#endif // SETTINGSDIALOG_H

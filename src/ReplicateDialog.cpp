#include <QPushButton>
#include <QPalette>

#include "ReplicateDialog.h"

// ----------------------------------------------------------------------------

ReplicateDialog::ReplicateDialog( QWidget  *parent,
                                  uint32_t  ref_nr,
                                  uint32_t  range,
                                  uint32_t *selection_mask,
                                  QString   title )
  : QDialog( parent ),
    _refNr( ref_nr ),
    _range( range ),
    _selectionMask( selection_mask )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );
  this->setWindowTitle( title );

  QPushButton *pb;
  for( uint32_t i=0; i<range; ++i )
    {
      if( i == 24 ) // SPECIAL: represents EMU link
        pb = new QPushButton( QString("EMU"), this );
      else
        pb = new QPushButton( QString::number(i), this );
      pb->setCheckable( true );
      pb->setMaximumWidth( 35 );
      if( i == ref_nr )
        {
          pb->setChecked( true );
          pb->setDisabled( true );

          QPalette qp = pb->palette();
          qp.setColor( QPalette::Base, QColor("green") );
          pb->setPalette( qp );
          pb->setAutoFillBackground( true );
        }

      _buttons.push_back( pb );
      this->horizontalLayout->addWidget( pb );
    }

  // Connect the buttons
  connect( _pushButtonAll, SIGNAL( clicked() ), this, SLOT( selectAll() ) );
  connect( _pushButtonNone, SIGNAL( clicked() ), this, SLOT( deselectAll() ) );
  connect( _pushButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( _pushButtonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );

  *_selectionMask = 0x0000;
}

// ----------------------------------------------------------------------------

ReplicateDialog::~ReplicateDialog()
{
}

// ----------------------------------------------------------------------------

void ReplicateDialog::selectAll()
{
  for( uint32_t i=0; i<_buttons.size(); ++i )
    if( i != _refNr )
      _buttons[i]->setChecked( true );
}

// ----------------------------------------------------------------------------

void ReplicateDialog::deselectAll()
{
  for( uint32_t i=0; i<_buttons.size(); ++i )
    if( i != _refNr )
      _buttons[i]->setChecked( false );
}

// ----------------------------------------------------------------------------

void ReplicateDialog::accept()
{
  // Establish which buttons were selected and return them in the mask
  *_selectionMask = 0x0000;
  for( uint32_t i=0; i<_buttons.size(); ++i )
    {
      if( i != _refNr && _buttons[i]->isChecked() )
        *_selectionMask |= (1 << i);
    }

  QDialog::accept();
}

// ----------------------------------------------------------------------------

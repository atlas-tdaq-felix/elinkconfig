#include "TimeoutDialog.h"
#include <QIntValidator>

// ----------------------------------------------------------------------------

TimeoutDialog::TimeoutDialog( QWidget  *parent,
                              bool     *timeout_enabled,
                              uint64_t *timeout_val,
                              uint64_t  timeout_min,
                              uint64_t  timeout_max,
                              bool     *ttc_timeout_enabled,
                              uint64_t *ttc_timeout_val )
  : QDialog( parent ),
    _timeoutEnabled( timeout_enabled ),
    _timeoutVal( timeout_val ),
    _ttcTimeoutEnabled( ttc_timeout_enabled ),
    _ttcTimeoutVal( ttc_timeout_val )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );
  this->setWindowTitle( "Datablocks Time-out Configuration" );

  _checkBoxTimeoutEnabled->setChecked( *timeout_enabled );
  _checkBoxTtcTimeoutEnabled->setChecked( *ttc_timeout_enabled );

  // Time-out value:
  _spinBoxTimeout->setMinimum( timeout_min );
  _spinBoxTimeout->setMaximum( timeout_max );
  _spinBoxTimeout->setValue( *timeout_val );
  QString qs = QString( "Time-out value: [%1..%2]" ).
    arg( timeout_min ).arg( timeout_max );
  _spinBoxTimeout->setToolTip( qs );

  // TTC time-out value
  _spinBoxTtcTimeout->setValue( *ttc_timeout_val );

  connect( _pushButtonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
  connect( _pushButtonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

// ----------------------------------------------------------------------------

TimeoutDialog::~TimeoutDialog()
{
}

// ----------------------------------------------------------------------------

void TimeoutDialog::accept()
{
  *_timeoutEnabled    = _checkBoxTimeoutEnabled->isChecked();
  *_timeoutVal        = _spinBoxTimeout->value();
  *_ttcTimeoutEnabled = _checkBoxTtcTimeoutEnabled->isChecked();
  *_ttcTimeoutVal     = _spinBoxTtcTimeout->value();
  QDialog::accept();
}

// ----------------------------------------------------------------------------

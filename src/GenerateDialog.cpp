#include <QFile>
#include <QPushButton>
#include <QMessageBox>
#include <QSettings>
#include <QTextStream>
#include <QThread> // for msleep()
#include <QTimer>  // for singleShot()

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
// Additional definitions for FlxCard registers and register contents
#define EGROUP_CTRL_MODE_SHIFT     15
#define EGROUP_CTRL_MAXCHUNK_SHIFT 31
#if REGMAP_VERSION < 0x500
#define EMU_CONFIG_ADDR_SHIFT      32
#define FM_EMU_CONFIG_ADDR_SHIFT   40
#endif // REGMAP_VERSION
//#define GBT_EGROUP_CTRL_I          CR_TH_GBT00_EGROUP0_CTRL_OFFSET
//#define GBT_EGROUP_CTRL_SIZE       (0x10*(7+5))
//#define GBT_EGROUP_CTRL_TOGBT_OFFS (0x10*7)
//#define GBT_CTRL_FROMGBT_UPDATE_I  CR_TH_UPDATE_CTRL_OFFSET
//#define GBT_CTRL_TOGBT_UPDATE_I    CR_FH_UPDATE_CTRL_OFFSET
//#define GBT_EMU_ENA_I              GBT_EMU_ENA_OFFSET
//#define GBT_EMU_CONFIG_I           GBT_EMU_CONFIG_OFFSET
//#define GBT_EMU_CONFIG_EGROUP_I    GBT_EMU_CONFIG_WE_ARRAY_OFFSET

#include "GenerateDialog.h"
#include "EmuDataGenerator.h"

// ----------------------------------------------------------------------------

GenerateDialog::GenerateDialog( QWidget   *parent,
                                int        card_nr,
                                int        link_nr,
                                int        nchans,
                                GbtConfig *link_config,
                                std::vector<regsetting_t> *settings )
  : QDialog( parent ),
    _allLinks( false ),
    _cardNr( card_nr ),
    _linkNr( link_nr ),
    _emuEnable( 0 ),
    _linkConfig( link_config ),
    _felixCard( 0 ),
    _regSettings( settings )
{
  this->setupUi(this);
  this->setWindowFlags( Qt::Dialog | Qt::WindowCloseButtonHint );

  _spinBoxLink->setMaximum( nchans );
  _spinBoxLink->setValue( _linkNr );
  _comboBoxFelix->hide();
  _labelFelix->hide();
  _progressBarData->hide();
  this->setSpinBoxMaxChunkSize();
  _labelEmuInfo->setText( "" );
  _labelLinkCnt->setText( "" );
  _labelDone->hide();
  _labelDone2->hide();

  // For the time being...
  _checkBoxSocK287->hide();

  // Connect various widgets
  connect( _spinBoxLink, SIGNAL( valueChanged(int) ),
           this, SLOT( changeLink(int) ) );

  connect( _checkBoxAllLinks, SIGNAL(stateChanged(int)),
           this, SLOT(changeAllLinksChecked(int)) );
  connect( _checkBoxDeveloper, SIGNAL(stateChanged(int)),
           this, SLOT(showDeveloperOptions(int)) );

  connect( _pushButtonGenConfig, SIGNAL( clicked() ),
           this, SLOT( generateLinkConfig() ) );
  connect( _pushButtonGenEmuData, SIGNAL( clicked() ),
           this, SLOT( generateEmulatorData() ) );
  connect( _pushButtonClose, SIGNAL( clicked() ),
           this, SLOT( close() ) );

  this->outputModeChanged();

  connect( _radioButtonFile, SIGNAL( clicked(bool) ),
             this, SLOT( outputModeChanged() ) );
  connect( _radioButtonFelix, SIGNAL( clicked(bool) ),
             this, SLOT( outputModeChanged() ) );

  connect( _comboBoxFelix, SIGNAL( activated(int) ),
           this, SLOT( changeFlx(int) ) );

  this->readSettings();

  this->showDeveloperOptions( _checkBoxDeveloper->checkState() );

  if( _linkConfig[0].linkMode() == LINKMODE_FULL )
    {
      // FULL mode
      QString qs = _groupBoxElinkConfig->title();
      qs += " (FULL MODE)";
      _groupBoxElinkConfig->setTitle( qs );
      _groupBoxEmulatorData->setTitle( "Emulator Data (FULL MODE)" );
      _checkBoxWidthPropSize->setEnabled( false );
      //_checkBoxAddBusy->setEnabled( false );
      _checkBoxCrcErr->setEnabled( true );
      _checkBoxFei4b->setEnabled( false );
      _comboBoxPattern->addItem( "ProtoDUNE" );
      _spinBoxChunkSize->setValue( ((_spinBoxChunkSize->value()+3)/4)*4 );
      _spinBoxChunkSize->setSingleStep( 4 );
    }

  // Show register settings checkbox if applicable
  if( _regSettings->size() > 0 )
    {
      _checkBoxExtraSettings->show();
      QString qs = _checkBoxExtraSettings->text();
      qs += QString( " (%1x)" ).arg( _regSettings->size() );
      _checkBoxExtraSettings->setText( qs );
    }
  else
    {
      _checkBoxExtraSettings->hide();
    }
}

// ----------------------------------------------------------------------------

GenerateDialog::~GenerateDialog()
{
}

// ----------------------------------------------------------------------------

void GenerateDialog::showDeveloperOptions( int checked )
{
  // Show/hide the (experimental) developer options
  if( checked > 0 )
    {
      _radioButtonFile->show();
      _checkBoxLsbFirst->show();
      _checkBoxAddBusy->show();
      _checkBoxOmitSoc->show();
      _checkBoxOmitEoc->show();
      _checkBoxCrcErr->show();
      _checkBoxFei4b->show();
      _labelLink->show();
      _spinBoxLink->show();
      _checkBoxAllLinks->show();
    }
  else
    {
      _radioButtonFile->hide();
      _checkBoxLsbFirst->hide();
      _checkBoxAddBusy->hide();
      _checkBoxOmitSoc->hide();
      _checkBoxOmitEoc->hide();
      _checkBoxCrcErr->hide();
      _checkBoxFei4b->hide();
      _labelLink->hide();
      _spinBoxLink->hide();
      _checkBoxAllLinks->hide();
      // Make sure we switch to 'all links' when hiding this checkbox
      // (to prevent confusion)
      _checkBoxAllLinks->setChecked( true );
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::changeAllLinksChecked( int checked )
{
  int lnk = FLX_LINKS;
  if( checked > 0 )
    {
      _allLinks = true;
      _spinBoxLink->setEnabled( false );
    }
  else
    {
      _allLinks = false;
      _spinBoxLink->setEnabled( true );
      lnk = _linkNr;
    }

  QString title;
  if( lnk == FLX_LINKS )
    title = QString( "Emulator Data (NB: based on link 'EMU' configuration)" );
  else
    title = QString( "Emulator Data "
                     "(NB: based on Link #%1 config)" ).arg( lnk );
  _groupBoxEmulatorData->setTitle( title );
}

// ----------------------------------------------------------------------------

void GenerateDialog::changeLink( int index )
{
  _linkNr = index;
  this->setSpinBoxMaxChunkSize();
  if( !_allLinks )
    {
      QString title = QString( "Emulator Data (NB: based on Link #%1 "
                               "configuration)" ).arg(_linkNr);
      _groupBoxEmulatorData->setTitle( title );
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::changeFlx( int index )
{
  if( index < 0 ) return;

  if( _felixCard )
    {
      try {
        _felixCard->card_close();
      }
      catch( FlxException &ex ) {
      }
    }

  if( _felixCard == 0 )
    _felixCard = new FlxCard;

  try {
    _felixCard->card_open( index, LOCK_NONE );
    _cardNr = index;
    _labelFelix->setText( this->flxVersionString() );
    _pushButtonGenConfig->setEnabled( true );
    _pushButtonGenEmuData->setEnabled( true );
  }
  catch( FlxException &ex ) {
    QString qs( "Failed to open FLX-card: " );
    qs += QString::fromStdString( ex.what() );
    delete _felixCard;
    _felixCard = 0;
    _labelFelix->setText( "version: ???" );
    _pushButtonGenConfig->setEnabled( false );
    _pushButtonGenEmuData->setEnabled( false );
    QMessageBox::warning( this, "Open FLX-card", qs );
  }
}

// ----------------------------------------------------------------------------

void GenerateDialog::outputModeChanged()
{
  if( _radioButtonFile->isChecked() )
    {
      _pushButtonGenConfig->setEnabled( true );
      _pushButtonGenEmuData->setEnabled( true );
    }

  if( _radioButtonFelix->isChecked() )
    {
      if( _felixCard == 0 )
        {
          // Determine the number of FELIX cards present and
          // configure the card selection combobox accordingly
          int nr_of_devs = FlxCard::number_of_devices();

          _comboBoxFelix->clear();
          if( nr_of_devs > 0 )
            {
              for( int i=0; i<nr_of_devs; ++i )
                _comboBoxFelix->addItem( QString::number(i) );

              _comboBoxFelix->setCurrentIndex( _cardNr );
              _felixCard = new FlxCard;
              try {
                _felixCard->card_open( _cardNr, LOCK_NONE );
                _labelFelix->setText( this->flxVersionString() );
              }
              catch( FlxException &ex ) {
                delete _felixCard;
                _felixCard = 0;
              }
            }
        }
      _comboBoxFelix->show();
      _labelFelix->show();

      _pushButtonGenConfig->setText( "Upload" );
      _pushButtonGenEmuData->setText( "Upload" );
      if( _comboBoxFelix->count() == 0 || _felixCard == 0 )
        {
          _pushButtonGenConfig->setEnabled( false );
          _pushButtonGenEmuData->setEnabled( false );
        }
      else
        {
          _pushButtonGenConfig->setEnabled( true );
          _pushButtonGenEmuData->setEnabled( true );
        }
    }
  else
    {
      if( _felixCard )
        {
          try {
            _felixCard->card_close();
          }
          catch( FlxException &ex ) {
          }       
          delete _felixCard;
          _felixCard = 0;
        }

      _comboBoxFelix->hide();
      _labelFelix->hide();

      _pushButtonGenConfig->setText( "Generate" );
      _pushButtonGenEmuData->setText( "Generate" );
      _pushButtonGenConfig->setEnabled( true );
      _pushButtonGenEmuData->setEnabled( true );
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::generateLinkConfig()
{
  if( _radioButtonFile->isChecked() )
    {
      this->writeConfigToFile( _linkNr, &_linkConfig[_linkNr] );
    }
  else if( _radioButtonFelix->isChecked() )
    {
      if( _felixCard == 0 ) return;

      // Determine card's channel (i.e. link) count
      uint32_t chans = _felixCard->cfg_get_reg( REG_NUM_OF_CHANNELS );
      if( chans > FLX_LINKS ) chans = FLX_LINKS;

      // Check firmware/configuration compatibility
      uint32_t mode = _linkConfig[0].linkMode();
      if( mode == LINKMODE_FULL && !_felixCard->fullmode_type() )
        {
          QMessageBox::warning( this, "E-link Config FULLMODE",
                                "Firmware and Configuration "
                                "for FULL mode are incompatible!" );
          return;
        }
      else if( (mode == LINKMODE_GBT || mode == LINKMODE_GBTWIDE) &&
               (_felixCard->lpgbt_type() || _felixCard->fullmode_type()) )
        {
          QMessageBox::warning( this, "E-link Config GBT",
                                "Firmware and Configuration "
                                "for GBT are incompatible!" );
          return;
        }
      else if( (mode == LINKMODE_LPGBT10_F5 ||
                mode == LINKMODE_LPGBT5_F5 ||
                mode == LINKMODE_LPGBT10_F12 ||
                mode == LINKMODE_LPGBT5_F12) &&
               !_felixCard->lpgbt_type() )
        {
          QMessageBox::warning( this, "E-link Config lpGBT",
                                "Firmware and Configuration "
                                "for lpGBT are incompatible!" );
          return;
        }

      _dmaIndexInvalidCount = 0;
      int linkcnt_from = 0, linkcnt_to = 0;
      int eciccnt_from = 0, eciccnt_to = 0;
      if( _allLinks )
        {
          //for( uint32_t lnk=0; lnk<chans; ++lnk )
          for( uint32_t lnk=0; lnk<FLX_LINKS; ++lnk )
            this->writeConfigToFlx( lnk, &_linkConfig[lnk] );

          // Determine cumulative numbers of E-links enabled
          for( uint32_t lnk=0; lnk<chans; ++lnk )
            {
              linkcnt_to   += _linkConfig[lnk].numberOfElinksToHost();
              linkcnt_from += _linkConfig[lnk].numberOfElinksFromHost();
              eciccnt_to   += _linkConfig[lnk].numberOfEcIcToHost();
              eciccnt_from += _linkConfig[lnk].numberOfEcIcFromHost();
            }
        }
      else
        {
          this->writeConfigToFlx( _linkNr, &_linkConfig[_linkNr] );

          // Determine cumulative numbers of E-links enabled
          linkcnt_from += _linkConfig[_linkNr].numberOfElinksToHost();
          linkcnt_to   += _linkConfig[_linkNr].numberOfElinksFromHost();
          eciccnt_from += _linkConfig[_linkNr].numberOfEcIcToHost();
          eciccnt_to   += _linkConfig[_linkNr].numberOfEcIcFromHost();
        }

      // Report about DMA indices in the configuration that were out of range
      if( _dmaIndexInvalidCount > 0 )
        {
          uint32_t dma_count;
          // DMA count includes the FromHost DMA
          dma_count = _felixCard->cfg_get_option( BF_GENERIC_CONSTANTS_DESCRIPTORS );
          --dma_count;
          QMessageBox::warning( this, "FELIX device configuration",
                                QString("DMA index of %1 (E-)links not in range "
                                        "[0,%2]\nand have been set to 0").
                                arg(_dmaIndexInvalidCount).arg(dma_count-1) );
        }

      // Display the number of enabled E-links
      QString qs = QString("(Elinks ToHost=%1+%2, FromHost=%3+%4)").
        arg( linkcnt_to ).arg( eciccnt_to ).
        arg( linkcnt_from ).arg( eciccnt_from );
      _labelLinkCnt->setText( qs );

      // Write additional register settings, if any and if enabled
      if( _regSettings->size() > 0 && _checkBoxExtraSettings->isChecked() )
        for( regsetting_t &r : *_regSettings )
          {
            try {
              _felixCard->cfg_set_option( r.name.c_str(), r.value );
            }
            catch( FlxException &e ) {
              try {
                _felixCard->cfg_set_reg( r.name.c_str(), r.value );
              }
              catch( FlxException &e ) {
                QMessageBox::critical( this, "Additional register settings",
                                       e.what() );
              }
            }
          }

      //QString qs = "Written to FLX card";
      //QMessageBox::information( this, "Link Configuration", qs );
      _labelDone->show();
      QTimer::singleShot( 600, this, SLOT( hideDoneLabel() ) );
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::generateEmulatorData()
{
  // Check firmware/configuration compatibility
  if( _felixCard )
    {
      uint32_t mode = _linkConfig[0].linkMode();
      if( mode == LINKMODE_FULL && !_felixCard->fullmode_type() )
        {
          QMessageBox::warning( this, "Emu Data FULLMODE",
                                "Firmware and Configuration "
                                "for FULL mode are incompatible!" );
          return;
        }
      else if( (mode == LINKMODE_GBT || mode == LINKMODE_GBTWIDE) &&
               (_felixCard->lpgbt_type() || _felixCard->fullmode_type()) )
        {
          QMessageBox::warning( this, "Emu Data GBT",
                                "Firmware and Configuration "
                                "for GBT are incompatible!" );
          return;
        }
      else if( (mode == LINKMODE_LPGBT10_F5 ||
                mode == LINKMODE_LPGBT5_F5 ||
                mode == LINKMODE_LPGBT10_F12 ||
                mode == LINKMODE_LPGBT5_F12) &&
               !_felixCard->lpgbt_type() )
        {
          QMessageBox::warning( this, "Emu Data lpGBT",
                                "Firmware and Configuration "
                                "for lpGBT are incompatible!" );
          return;
        }
    }

  if( _linkConfig[0].linkMode() == LINKMODE_FULL )
    this->generateFmEmulatorData();
  else
    this->generateGbtEmulatorData();
}

// ----------------------------------------------------------------------------

void GenerateDialog::generateGbtEmulatorData()
{
  EmuDataGenerator datagenerator;
  uint32_t   emudata[16384];
  uint32_t   emusize = 16384;
  uint64_t   nbits_word;
  uint64_t   nbits_index_word;
  uint32_t   modes_word;
  uint32_t   truncation_word;
  uint32_t   req_chunksize = _spinBoxChunkSize->value();
  bool       width_prop_sz = _checkBoxWidthPropSize->isChecked();
  bool       random_sz     = _checkBoxRandomSize->isChecked();
  bool       use_streamid  = _checkBoxStreamId->isChecked();
  uint32_t   pattern_id    = _comboBoxPattern->currentIndex();
  uint32_t   idle_chars    = _spinBoxIdleChars->value();
  bool       lsb_first     = _checkBoxLsbFirst->isChecked();
  bool       soc_is_k287   = _checkBoxSocK287->isChecked();
  bool       add_a_busy    = _checkBoxAddBusy->isChecked();
  bool       omit_one_soc  = _checkBoxOmitSoc->isChecked();
  bool       omit_one_eoc  = _checkBoxOmitEoc->isChecked();
  bool       fei4b_kchars  = _checkBoxFei4b->isChecked();
  GbtConfig *cfg;
  QString    ext;

  // Added 5 Nov 2018
  //bool emuram_msb_first = false;
  //if( _felixCard &&
  //    (_felixCard->cfg_get_reg( REG_REG_MAP_VERSION ) > 0x404 ||
  //     (_felixCard->cfg_get_reg( REG_REG_MAP_VERSION ) == 0x404 &&
  //      _felixCard->cfg_get_reg( REG_GIT_COMMIT_NUMBER ) >= 119)) )
  //  emuram_msb_first = !lsb_first;
  // Above changed to this (29 Jan 2020)
  bool emuram_msb_first = !lsb_first;

  // Generate emulator data based on the configuration of link 'EMU'
  // (link with index=FLX_LINKS) in case of all-links-selected
  if( _allLinks )
    cfg = &_linkConfig[FLX_LINKS];
  else
    cfg = &_linkConfig[_linkNr];

  uint32_t linkmode = cfg->linkMode();
  uint32_t enables;
  uint64_t modes;
  // GBT: 5, GBT Wide or lpGBT-FEC5: 7 groups, lpGBT-FEC12: 6 groups
  uint32_t max_grp;
  if( linkmode == LINKMODE_GBTWIDE ||
      linkmode == LINKMODE_LPGBT10_F5 ||
      linkmode == LINKMODE_LPGBT5_F5 )
    max_grp = 7;
  else if( linkmode == LINKMODE_LPGBT10_F12 ||
           linkmode == LINKMODE_LPGBT5_F12 )
    max_grp = 6;
  else
    max_grp = 5;
  // Check for nothing enabled: issue a warning to the user
  bool nothing_enabled = true;
  for( uint32_t grp=0; grp<max_grp; ++grp )
    {
      enables = cfg->enablesToHost( grp );
      if( enables != 0 ) nothing_enabled = false;
    }
  if( nothing_enabled )
    QMessageBox::warning( this, "EMU Link Configuration",
                          "Nothing enabled in EMU Link" );
  for( uint32_t grp=0; grp<max_grp; ++grp )
    {
      enables = cfg->enablesToHost( grp );
      modes   = cfg->modesToHost( grp );
#if REGMAP_VERSION < 0x500
      nbits_word = GbtConfig::epathBitsWord( enables );
      modes_word = GbtConfig::epath4BitModeWord( enables, modes );
      nbits_index_word = GbtConfig::epathBitIndexWord( enables );
#else
      uint32_t width_in_bits = GbtConfig::widthCodeToBits( cfg->widthToHost(grp) );
      nbits_word = GbtConfig::epathBitsWord( enables, width_in_bits );
      modes_word = modes;
      if( linkmode == LINKMODE_LPGBT10_F5 ||
          linkmode == LINKMODE_LPGBT10_F12 )
        nbits_index_word = 0x0000000018100800;
      else if( linkmode == LINKMODE_LPGBT5_F5 ||
               linkmode == LINKMODE_LPGBT5_F12 )
        nbits_index_word = 0x000000000C080400;
      else
        nbits_index_word = 0x0E0C0A0806040200;
#endif // REGMAP_VERSION
      //if( grp == 0 )
      //  {
      //    QMessageBox::warning( this, "DEBUG 1",
      //                          QString("nbits=%1 ena=%2 index=%3").
      //                          arg(nbits_word,0,16).
      //                          arg(enables,0,16).arg(nbits_index_word,0,16));
      //  }
      truncation_word = cfg->maxChunkWord();
      datagenerator.generate( emudata, emusize,
                              nbits_word, nbits_index_word, modes_word,
                              truncation_word, req_chunksize, grp,
                              pattern_id, idle_chars, lsb_first,
                              emuram_msb_first,
                              width_prop_sz, random_sz, use_streamid,
                              soc_is_k287,
                              add_a_busy, omit_one_soc, omit_one_eoc,
                              fei4b_kchars );

      QString qs;
      int chnks = datagenerator.chunkCount();
      if( datagenerator.errorId() == 0 )
        {
          // Clock 40.079 MHz
          double khz = ((double) chnks * 40079.0) / ((double) emusize);
          qs = QString( "(Chunks=%1, per Elink: %2 KHz)" ).
            arg( chnks ).arg( khz, 0, 'f', 1 );
        }
      else
        {
          qs = QString( "(Err=%1, Chunks=%2)" ).
            arg( datagenerator.errorId() ).arg( chnks );
        }
      _labelEmuInfo->setText( qs );

      _progressBarData->show();
      if( _radioButtonFile->isChecked() )
        {
#if REGMAP_VERSION < 0x500
          ext = QString( ".coe" );
#else
          ext = QString( ".mem" );
#endif // REGMAP_VERSION
          this->writeEmuDataToFile( emudata, emusize, grp, ext );
        }
      else if( _radioButtonFelix->isChecked() )
        {
          this->writeEmuDataToFlx( emudata, emusize, grp );
        }
      _progressBarData->setValue( (100*(grp+1))/max_grp );
      QCoreApplication::processEvents();
      //QThread::msleep( 50 );
    }

  QString qs;
  if( _radioButtonFile->isChecked() )
    {
      qs = QString( "Files (*" ) + ext + QString( ") saved" );
    }
  else if( _radioButtonFelix->isChecked() )
    {
      qs = "Written to FLX card";
    }
  //qs += QString("                   "); // Gain some string width to display

  if( _radioButtonFelix->isChecked() )
    QThread::msleep( 200 );
  else
    QMessageBox::information( this, "Emulator Data", qs );

  _progressBarData->hide();
  _progressBarData->setValue( 0 );
}

// ----------------------------------------------------------------------------

void GenerateDialog::generateFmEmulatorData()
{
  // Generate FULLMODE emulator data
  EmuDataGenerator datagenerator;
  uint64_t emudata[8192];
  uint64_t emusize = 8192;
  //uint64_t emusize = 1024;
  uint32_t req_chunksize = ((_spinBoxChunkSize->value()+3)/4)*4;
  uint32_t pattern_id    = _comboBoxPattern->currentIndex();
  uint32_t idle_chars    = _spinBoxIdleChars->value();
  bool     random_sz     = _checkBoxRandomSize->isChecked();
  bool     use_streamid  = _checkBoxStreamId->isChecked();
  bool     add_a_busy    = _checkBoxAddBusy->isChecked();
  bool     omit_one_soc  = _checkBoxOmitSoc->isChecked();
  bool     omit_one_eoc  = _checkBoxOmitEoc->isChecked();
  bool     add_crc_err   = _checkBoxCrcErr->isChecked();
  bool     crc_new       = false;

  // Larger emulator RAM implemented? (###now the default, 30 Oct 2019)
  //if( _felixCard &&
  //    (_felixCard->cfg_get_reg( REG_BOARD_ID_SVN ) >= 5635 ||
  //     _felixCard->cfg_get_reg( REG_BOARD_ID_SVN ) == 0) ) // Moved to GIT
  //  emusize = 8192;

  // CRC version to use
  //if( _felixCard &&
  //    (_felixCard->cfg_get_reg( REG_REG_MAP_VERSION ) > 0x402 ||
  //     (_felixCard->cfg_get_reg( REG_REG_MAP_VERSION ) == 0x402 &&
  //      _felixCard->cfg_get_reg( REG_GIT_COMMIT_NUMBER ) >= 47)) )
  crc_new = true;

  if( pattern_id == 4 )
    {
      datagenerator.generateFmDune( emudata, emusize, &req_chunksize,
                                    idle_chars );
    }
  else
    {
      datagenerator.generateFm( emudata, emusize, req_chunksize,
                                pattern_id, idle_chars, random_sz, crc_new,
                                use_streamid,
                                add_a_busy, omit_one_soc, omit_one_eoc,
                                add_crc_err );
    }
  _spinBoxChunkSize->setValue( req_chunksize );

  QString qs, ext;
  int chnks = datagenerator.chunkCount();
  if( datagenerator.errorId() == 0 )
    {
      // Clock 240.474 MHz
      double khz = ((double) chnks * 240474.0) / ((double) emusize);
      qs = QString( "(Chunks=%1, per Link: %2 KHz)" ).
        arg( chnks ).arg( khz, 0, 'f', 1 );
    }
  else
    {
      qs = QString( "(Err=%1, Chunks=%2)" ).
        arg( datagenerator.errorId() ).arg( chnks );
    }
  _labelEmuInfo->setText( qs );

  if( _radioButtonFile->isChecked() )
    {
#if REGMAP_VERSION < 0x500
      ext = QString( ".coe" );
#else
      ext = QString( ".mem" );
#endif // REGMAP_VERSION
      this->writeFmEmuDataToFile( emudata, emusize, ext );
    }
  else if( _radioButtonFelix->isChecked() )
    {
      this->writeFmEmuDataToFlx( emudata, emusize );
    }

  if( _radioButtonFile->isChecked() )
    {
#if REGMAP_VERSION < 0x500
      qs = "File (fmemuram.coe) saved";
#else
      qs = "File (fmemuram.mem) saved";
#endif // REGMAP_VERSION
    }
  else if( _radioButtonFelix->isChecked() )
    {
      qs = "Written to FLX card";
    }

  if( _radioButtonFelix->isChecked() )
    {
      _labelDone2->show();
      QTimer::singleShot( 600, this, SLOT( hideDoneLabel() ) );
    }
  else
    {
      QMessageBox::information( this, "Emulator Data", qs );
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::hideDoneLabel()
{
  _labelDone->hide();
  _labelDone2->hide();
}

// ----------------------------------------------------------------------------

void GenerateDialog::setSpinBoxMaxChunkSize()
{
  // Set the *maximum* chunksize to the lowest configured maximum chunksize
  // of the currently selected GBT number
  uint32_t truncation_word = _linkConfig[_linkNr].maxChunkWord();
  int32_t  max_chunksz = 3584;
  if( _linkConfig[0].linkMode() == LINKMODE_FULL )
    {
      max_chunksz = 10000;
    }
  else
    {
      for( int i=0; i<4; ++i )
        {
          int32_t max = (truncation_word>>(i*3) & 0x7) * FLX_MAXCHUNK_UNIT;
          if( max == 0 ) max = 3584;
          if( max < max_chunksz )
            max_chunksz = max;
        }
    }

  if( _spinBoxChunkSize->value() > max_chunksz )
    _spinBoxChunkSize->setValue( max_chunksz );

  _spinBoxChunkSize->setMaximum( max_chunksz );
  // ###For test purposes allow large size
  //_spinBoxChunkSize->setMaximum( 4000 );

  _spinBoxChunkSize->setToolTip( QString("Max=%1").arg( max_chunksz ) );
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeConfigToFile( uint32_t lnk, GbtConfig *cfg )
{
  // Write configuration to a file in the form of VHDL code a-la Julia's

  // Compose a file name
  QString filename;
  filename = QString("centralrouter-G%1.vhd").arg( lnk );

  // Open the file
  QFile file( filename );
  if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
      QMessageBox::warning( this, "Opening (vhd) config file",
                            QString("Could not open file ")+
                            filename );
      return;
    }

  uint32_t enables;
  uint64_t modes;
  uint32_t modes_word;
  uint32_t truncation_word;

  // Write to the file
  QTextStream out( &file );

  out << "--<< begin\n--\n-- 1. EPROC_ENA_bits 15 bit vector per EGROUP "
      << "(15 EPROCs in one EGROUP)\n";
  out << "--   [EPROC_IN2 EPROC_IN2 EPROC_IN2 EPROC_IN2 EPROC_IN2 EPROC_IN2 "
      << "EPROC_IN2 EPROC_IN2 EPROC_IN4 EPROC_IN4 EPROC_IN4 EPROC_IN4 "
      << "EPROC_IN8 EPROC_IN8 EPROC_IN16]\n--\n";

  out << "type EPROC_ENA_bits_array_type is array (0 to 6) of "
      << "std_logic_vector(14 downto 0);\n";
  out << "constant EPROC_ENA_bits_array : EPROC_ENA_bits_array_type :=(";

  for( uint32_t grp=0; grp<7; ++grp )
    {
      enables = cfg->enablesToHost( grp );
      out << '\n';
      out << QString( "\"%1\"" ).arg( enables, 15, 2, QChar('0') );
      if( grp != 6 ) out << ',';
    }
  out << ");\n";

  out << "--\n-- 2. PATH_ENCODING, 16 bit vector per EGROUP (2 bits per PATH, "
      << "8 PATHs in one EGROUP)\n";
  out << "--   for each of 8 output paths: \"00\"=non,  \"01\"=8b10b,  "
      << "\"10\"=HDLC\n--\n";
  out << "type EPROC_ENCODING_array_type is array (0 to 6) "
      << "of std_logic_vector(15 downto 0);\n";
  out << "constant PATH_ENCODING_array  : EPROC_ENCODING_array_type :=(";

  for( uint32_t grp=0; grp<7; ++grp )
    {
      enables    = cfg->enablesToHost( grp );
      modes      = cfg->modesToHost( grp );
#if REGMAP_VERSION < 0x500
      //modes_word = GbtConfig::epath4BitModeWord( enables, modes );
      modes_word = GbtConfig::epath2BitModeWord( enables, modes );
#else
      modes_word = modes;
#endif // REGMAP_VERSION
      out << '\n';

      // Julia uses only 2 bits for the mode per E-path, for the time being
      //out << '\"';
      //for( int epath=0; epath<8; ++epath )
      //out << QString( "%1" ).arg( (modes_word >> (epath*4)) & 0x3,
      //                                2, 2, QChar('0') );
      //out << '\"';
      out << QString( "\"%1\"" ).arg( modes_word, 16, 2, QChar('0') );

      if( grp != 6 ) out << ',';
    }
  out << ");\n";

  out << "--\n-- 3. Maximum valid CHUNK length for data truncation \n";
  out << "--   per GBT channel, 3MSBs per Eproc type \n--\n";
  out << "constant MAX_CHUNK_LEN_array : std_logic_vector(11 downto 0) "
      << ":= ";

  truncation_word = cfg->maxChunkWord();
  out << QString( "\"%1\";" ).arg( truncation_word, 12, 2, QChar('0') );
  out << "\n--<< end\n";

  // Close the file
  file.close();

  QString qs = "Written to file: ";
  qs += filename;
  QMessageBox::information( this, "Link Configuration", qs );
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeConfigToFlx( uint32_t lnk, GbtConfig *cfg )
{
  // Write configuration directly into the selected FLX card
  uint32_t linkmode = cfg->linkMode();
  uint32_t enables;
  uint64_t modes;
#if REGMAP_VERSION < 0x0500
  uint32_t modes_word;
#endif // REGMAP_VERSION
  uint32_t truncation_word = 0;

  if( _felixCard == 0 ) return;

  // Disable any active emulators
  // (re-enable only when the emulator data is updated too)
  uint64_t emu_ena;
#if REGMAP_VERSION < 0x500
  if( linkmode == LINKMODE_FULL )
    {
      emu_ena = _felixCard->cfg_get_reg( REG_GBT_FM_EMU_ENA_TOHOST );
      if( emu_ena != 0 )
        _felixCard->cfg_set_reg( REG_GBT_FM_EMU_ENA_TOHOST, 0 );
    }
  else
    {
      emu_ena = _felixCard->cfg_get_reg( REG_GBT_EMU_ENA );
      if( emu_ena != 0 )
        _felixCard->cfg_set_reg( REG_GBT_EMU_ENA, 0 );
    }
#else
  emu_ena = _felixCard->cfg_get_option( BF_FE_EMU_ENA_EMU_TOHOST );
  if( emu_ena != 0 )
    _felixCard->cfg_set_option( BF_FE_EMU_ENA_EMU_TOHOST, 0 );
#endif // REGMAP_VERSION
  _emuEnable = emu_ena;

  // Configure requested 8b10b word bit order
#if REGMAP_VERSION < 0x500
  if( _checkBoxLsbFirst->isChecked() )
    _felixCard->cfg_set_reg( REG_CR_REVERSE_10B, 0 );
  else
    _felixCard->cfg_set_reg( REG_CR_REVERSE_10B, 3 ); // TOHOST/FROMHOST bits
#else
  if( _checkBoxLsbFirst->isChecked() )
    {
      _felixCard->cfg_set_reg( REG_DECODING_REVERSE_10B, 0 ); // TOHOST
      _felixCard->cfg_set_reg( REG_ENCODING_REVERSE_10B, 0 ); // FROMHOST
    }
  else
    {
      _felixCard->cfg_set_reg( REG_DECODING_REVERSE_10B, 1 ); // TOHOST
      _felixCard->cfg_set_reg( REG_ENCODING_REVERSE_10B, 1 ); // FROMHOST
    }
#endif // REGMAP_VERSION

  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) _felixCard->bar2Address();

  // To Host:
  // ========
  int max_grp;
  if( linkmode == LINKMODE_FULL )
    {
      if( lnk >= FLX_LINKS/2 )
        return;

      // FULL mode: do egroup configuration eventhough it is meaningless..
      // (the FULL mode enable bit is 'hidden' in group 0)
      max_grp = FLX_TOHOST_GROUPS-1;
      for( int grp=0; grp<max_grp; ++grp )
        {
          enables = 0;
          if( grp == 0 )
            {
#if REGMAP_VERSION < 0x500
              if( (cfg->enablesToHost( grp ) & FULL_ENABLED) != 0 )
                //enables = 0x0001;
                bar2->CR_FM_PATH_ENA |= (1 << lnk);
              else
                //enables = 0;
                bar2->CR_FM_PATH_ENA &= ~(1 << lnk);
#else
                bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[0].EGROUP.EPATH_ENA = ( (cfg->enablesToHost( grp ) & FULL_ENABLED) != 0 );
#endif // REGMAP_VERSION
            }
#if REGMAP_VERSION < 0x0500
          modes           = cfg->modesToHost( grp );
          modes_word      = GbtConfig::epath2BitModeWord( enables, modes );
          truncation_word = cfg->maxChunkWord();
          
          flxcard_cr_tohost_egroup_ctrl_t *ctrl =
            &bar2->CR_GBT_CTRL[lnk].EGROUP_TOHOST[grp].TOHOST;
          ctrl->EPROC_ENA     = enables;
          ctrl->PATH_ENCODING = modes_word;
          ctrl->MAX_CHUNK_LEN = truncation_word;
#else
          // Do nothing...
#endif // REGMAP_VERSION
        }
    }
  else
    {
      //max_grp=(linkmode==LINKMODE_GBTWIDE ? 7 : 5);//normal:5,wide:7
      max_grp = FLX_TOHOST_GROUPS-1;

      if( lnk < FLX_LINKS/2 )
      for( int grp=0; grp<max_grp; ++grp )
        {
          enables         = cfg->enablesToHost( grp );
          modes           = cfg->modesToHost( grp );
          truncation_word = cfg->maxChunkWord();
#if REGMAP_VERSION < 0x0500
          modes_word        = GbtConfig::epath2BitModeWord( enables, modes );
          flxcard_cr_tohost_egroup_ctrl_t *ctrl =
            &bar2->CR_GBT_CTRL[lnk].EGROUP_TOHOST[grp].TOHOST;
          ctrl->EPROC_ENA     = enables;
          ctrl->PATH_ENCODING = modes_word;
          ctrl->MAX_CHUNK_LEN = truncation_word;
#else
          flxcard_decoding_egroup_ctrl_t *ctrl =
            &bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[grp].EGROUP;
          uint32_t width      = cfg->widthToHost( grp );
          ctrl->EPATH_ENA     = enables;
          ctrl->EPATH_WIDTH   = width;
          ctrl->PATH_ENCODING = modes;
          // For now: set HDLC truncation globally
          if( truncation_word > 0 )
            ctrl->ENABLE_TRUNCATION = 1;
          else
            ctrl->ENABLE_TRUNCATION = 0;
#endif // REGMAP_VERSION
        }

      // and the EC+IC+AUX+TTC egroup
      enables = cfg->enablesToHost( TOHOST_MINI_GROUP );
      modes   = cfg->modesToHost( TOHOST_MINI_GROUP );

#if REGMAP_VERSION < 0x500
      flxcard_ec_tohost_t *ec_tohost =
        &bar2->MINI_EGROUP_CTRL[lnk].EC_TOHOST;

      // To-host EC enabled/disabled
      if( enables & EC_ENABLED )
        ec_tohost->ENABLE = 1;
      else
        ec_tohost->ENABLE = 0;

      // To-host EC mode
      ec_tohost->ENCODING = (modes >> (EC_ENABLED_BIT*4)) & 0xF;

      // To-host IC enabled/disabled
      if( enables & IC_ENABLED )
        ec_tohost->IC_ENABLE = 1;
      else
        ec_tohost->IC_ENABLE = 0;

      // To-host SCA-AUX enabled/disabled
      if( enables & AUX_ENABLED )
        ec_tohost->SCA_AUX_ENABLE = 1;
      else
        ec_tohost->SCA_AUX_ENABLE = 0;
#else
      flxcard_mini_egroup_tohost_t *mini_egroup_tohost =
        &bar2->MINI_EGROUP_TOHOST_GEN[lnk].MINI_EGROUP_TOHOST;

      // To-host EC enabled/disabled
      if( enables & EC_ENABLED )
        mini_egroup_tohost->EC_ENABLE = 1;
      else
        mini_egroup_tohost->EC_ENABLE = 0;
      
      // To-host EC mode
      mini_egroup_tohost->EC_ENCODING = (modes >> (EC_ENABLED_BIT*4)) & 0xF;

      // To-host IC enabled/disabled
      if( enables & IC_ENABLED )
        mini_egroup_tohost->IC_ENABLE = 1;
      else
        mini_egroup_tohost->IC_ENABLE = 0;

      // To-host SCA-AUX enabled/disabled
      if( enables & AUX_ENABLED )
        mini_egroup_tohost->AUX_ENABLE = 1;
      else
        mini_egroup_tohost->AUX_ENABLE = 0;

      // For now: set HDLC truncation globally
      if( truncation_word > 0 )
        {
          // Don't truncate IC
          mini_egroup_tohost->ENABLE_EC_TRUNCATION = 1;
          mini_egroup_tohost->ENABLE_IC_TRUNCATION = 0;
          mini_egroup_tohost->ENABLE_AUX_TRUNCATION = 1;
        }
      else
        {
          mini_egroup_tohost->ENABLE_EC_TRUNCATION = 0;
          mini_egroup_tohost->ENABLE_IC_TRUNCATION = 0;
          mini_egroup_tohost->ENABLE_AUX_TRUNCATION = 0;
        }
#endif // REGMAP_VERSION
    }

  // Some global settings (done when configuring link 0)
  if( lnk == 0 )
    {
      // To-host TTC enabled/disabled
      enables = cfg->enablesToHost( TOHOST_MINI_GROUP );
#if REGMAP_VERSION < 0x500
      if( enables & TTC2H_ENABLED )
        bar2->CR_TTC_TOHOST.ENABLE = 1;
      else
        bar2->CR_TTC_TOHOST.ENABLE = 0;
#else
      if( enables & TTC2H_ENABLED )
        bar2->TTC_TOHOST_ENABLE = 1;
      else
        bar2->TTC_TOHOST_ENABLE = 0;
#endif // REGMAP_VERSION

      // Select TTC Clock ?
      if( cfg->ttcClock() )
        bar2->MMCM_MAIN.LCLK_SEL = 0;

#if REGMAP_VERSION >= 0x0500
      // lpGBT link mode flavour setting: 10.24Gbs or 5.12Gbs
      if( linkmode == LINKMODE_LPGBT10_F5 ||
          linkmode == LINKMODE_LPGBT10_F12 )
        bar2->LPGBT_DATARATE = 0x000000000000;
      else if( linkmode == LINKMODE_LPGBT5_F5 ||
               linkmode == LINKMODE_LPGBT5_F12 )
        bar2->LPGBT_DATARATE = 0xFFFFFFFFFFFF;

      // lpGBT link mode flavour setting: FEC5 or FEC12
      if( linkmode == LINKMODE_LPGBT10_F5 ||
          linkmode == LINKMODE_LPGBT5_F5 )
        bar2->LPGBT_FEC = 0x000000000000;
      else if( linkmode == LINKMODE_LPGBT5_F12 ||
               linkmode == LINKMODE_LPGBT10_F12 )
        bar2->LPGBT_FEC = 0xFFFFFFFFFFFF;
#endif // REGMAP_VERSION
    }

#if REGMAP_VERSION >= 0x0500
  // E-link DMA controller indices
  int elinknr, dma_index;
  int max_pathnr;
  if( linkmode == LINKMODE_LPGBT10_F5 ||
      linkmode == LINKMODE_LPGBT5_F5 ||
      linkmode == LINKMODE_LPGBT10_F12 ||
      linkmode == LINKMODE_LPGBT5_F12 )
    max_pathnr = 4;
  else
    max_pathnr = 8;
  // DMA count includes the FromHost DMA
  int dma_count = bar2->GENERIC_CONSTANTS.DESCRIPTORS - 1;
  if( lnk < FLX_LINKS/2 )
  for( int group=0; group<max_grp; ++group )
    {
      uint32_t dma_indices = cfg->dmaIndices( group );
      for( int path=0; path<max_pathnr; ++path )
        {
          if( linkmode == LINKMODE_LPGBT10_F5 ||
              linkmode == LINKMODE_LPGBT5_F5 ||
              linkmode == LINKMODE_LPGBT10_F12 ||
              linkmode == LINKMODE_LPGBT5_F12 )
            elinknr = ((lnk << BLOCK_LNK_SHIFT) |
                       (group << BLOCK_EGROUP_SHIFT_LPGBT) | path);
          else
            elinknr = ((lnk << BLOCK_LNK_SHIFT) |
                       (group << BLOCK_EGROUP_SHIFT) | path);

          // Set 'address' (and read current DMA index setting)
          bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
          // Write DMA index setting, but check for a valid number
          dma_index = (dma_indices >> (path*4)) & 0xF;
          if( dma_index > dma_count-1 )
            {
              dma_index = 0; // Use DMA index 0 instead
              ++_dmaIndexInvalidCount;
              //QMessageBox::warning( this, "DEBUG",
              //                      QString("%1,%2").arg(group).arg(path) );
            }
          bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;
        }
    }

  uint32_t dma_indices = cfg->dmaIndices( TOHOST_MINI_GROUP );
  if( linkmode != LINKMODE_FULL )
    {
      // EC E-link DMA index
      elinknr = (lnk << BLOCK_LNK_SHIFT) | bar2->AXI_STREAMS_TOHOST.EC_INDEX;
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Setting stored in Egroup 7, path 7
      dma_index = (dma_indices >> (7*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++_dmaIndexInvalidCount;
        }
      bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;

      // IC E-link DMA index
      elinknr = (lnk << BLOCK_LNK_SHIFT) | bar2->AXI_STREAMS_TOHOST.IC_INDEX;
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Setting stored in Egroup 7, path 6
      dma_index = (dma_indices >> (6*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++_dmaIndexInvalidCount;
        }
      bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;

      // AUX E-link DMA index
      /* Ignore: LTDB has only 1 endpoint; would clash with e-link 0x26
      elinknr = (lnk << BLOCK_LNK_SHIFT) | (bar2->AXI_STREAMS_TOHOST.EC_INDEX - 1);
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Setting stored in Egroup 7, path 5
      dma_index = (dma_indices >> (5*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++_dmaIndexInvalidCount;
        }
      bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;
      */
    }

  // TTCtoHost E-link DMA index
  if( lnk == 0 )
    {
      elinknr = 24 << BLOCK_LNK_SHIFT; // E-link 0x600
      bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Setting stored in Egroup 7, path 0
      dma_index = dma_indices & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++_dmaIndexInvalidCount;
        }
      bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;
    }
#endif // REGMAP_VERSION
      
  // Stream ID bits
#if REGMAP_VERSION < 0x500
  uint64_t *p = (uint64_t *) &bar2->PATH_HAS_STREAM_ID[lnk].TOHOST;
#else
  uint64_t *p = (uint64_t *) &bar2->PATH_HAS_STREAM_ID[lnk];
#endif // REGMAP_VERSION
  uint64_t val = *p;
  for( int grp=0; grp<max_grp; ++grp )
    {
      // Clear and set Egroup's streamid bits
      val &= ~(0xFFULL << (grp*8));
      val |= (((uint64_t) cfg->streamIdBits( grp ) & 0xFFULL) << (grp*8));
    }
  *p = val;

  // From Host:
  // ==========
  //max_grp=(linkmode==LINKMODE_GBTWIDE ? 5 : 3);// normal:3, wide:5
  max_grp = FLX_FROMHOST_GROUPS-1;

  if( lnk < FLX_LINKS/2 )
  for( int grp=0; grp<max_grp; ++grp )
    {
      enables = cfg->enablesFromHost( grp );
      modes   = cfg->modesFromHost( grp );
#if REGMAP_VERSION < 0x500      
      modes_word = GbtConfig::epath4BitModeWord( enables, modes );
      flxcard_cr_fromhost_egroup_ctrl_t *ctrl =
        &bar2->CR_GBT_CTRL[lnk].EGROUP_FROMHOST[grp].FROMHOST;
      ctrl->EPROC_ENA     = enables;
      ctrl->PATH_ENCODING = modes_word;
#else
      flxcard_encoding_egroup_ctrl_t *ctrl =
        &bar2->ENCODING_EGROUP_CTRL_GEN[lnk].ENCODING_EGROUP[grp].ENCODING_EGROUP_CTRL;
      uint32_t width = cfg->widthFromHost( grp );
      uint32_t ttc_opt = cfg->ttcOptionFromHost( grp );
      ctrl->EPATH_ENA     = enables;
      ctrl->EPATH_WIDTH   = width;
      ctrl->PATH_ENCODING = modes;
      ctrl->TTC_OPTION    = ttc_opt;
#endif // REGMAP_VERSION
    }

  // and the EC+IC+AUX egroup
  enables = cfg->enablesFromHost( FROMHOST_MINI_GROUP );
  modes   = cfg->modesFromHost( FROMHOST_MINI_GROUP );

#if REGMAP_VERSION < 0x500
  flxcard_ec_fromhost_t *ec_fromhost =
    &bar2->MINI_EGROUP_CTRL[lnk].EC_FROMHOST;

  // From-host EC enabled/disabled
  if( enables & EC_ENABLED )
    ec_fromhost->ENABLE = 1;
  else
    ec_fromhost->ENABLE = 0;

  // From-host EC mode
  ec_fromhost->ENCODING = (modes >> (EC_ENABLED_BIT*4)) & 0xF;

  // From-host IC enabled/disabled
  if( enables & IC_ENABLED )
    ec_fromhost->IC_ENABLE = 1;
  else
    ec_fromhost->IC_ENABLE = 0;

  // From-host SCA_AUX enabled/disabled
  if( enables & AUX_ENABLED )
    ec_fromhost->SCA_AUX_ENABLE = 1;
  else
    ec_fromhost->SCA_AUX_ENABLE = 0;

#else
  flxcard_mini_egroup_fromhost_t *mini_egroup_fromhost =
    &bar2->MINI_EGROUP_FROMHOST_GEN[lnk].MINI_EGROUP_FROMHOST;

  // From-host EC enabled/disabled
  if( enables & EC_ENABLED )
    mini_egroup_fromhost->EC_ENABLE = 1;
  else
    mini_egroup_fromhost->EC_ENABLE = 0;

  // From-host EC mode
  mini_egroup_fromhost->EC_ENCODING = (modes >> (EC_ENABLED_BIT*4)) & 0xF;

  // From-host IC enabled/disabled
  if( enables & IC_ENABLED )
    mini_egroup_fromhost->IC_ENABLE = 1;
  else
    mini_egroup_fromhost->IC_ENABLE = 0;

  // From-host SCA_AUX enabled/disabled
  if( enables & AUX_ENABLED )
    mini_egroup_fromhost->AUX_ENABLE = 1;
  else
    mini_egroup_fromhost->AUX_ENABLE = 0;

  if( linkmode == LINKMODE_FULL )
    {
      // FromHost GBT or LTI
      uint64_t lti = bar2->LINK_FULLMODE_LTI;
      // SPECIAL: since the LINK_FULLMODE_LTI register is in Endpoint-0 only,
      // configure Endpoint-0/-1 links identically
      // (NB: of course only has effect when applied to FLX device #0)
      if( cfg->ltiTtc() )
        lti |= ((1 << lnk) | (1 << (12+lnk)));
      else
        lti &= ~((1 << lnk) | (1 << (12+lnk)));
      bar2->LINK_FULLMODE_LTI = lti;
    }
#endif // REGMAP_VERSION

  _felixCard->soft_reset();
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeEmuDataToFile( uint32_t *emudata,
                                         uint32_t  emusize,
                                         uint32_t  egroup,
                                         QString   ext )
{
  // Compose a file name
  QString filename = QString("emuram");
  filename += QString("Grp%1").arg( egroup );
  filename += ext;

  // Open the file
  QFile file( filename );
  if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
      QMessageBox::warning( this, "Opening Emulator Data file",
                            QString("Could not open file ")+
                            filename );
      return;
    }

  // Write to the file
  QTextStream out( &file );
  if( ext == QString(".coe") )
    {
      // .coe file:
      // First a kind of header...
      out << "; 16-bit wide by " << emusize << " deep\n"
          << "memory_initialization_radix = 2;\n"
          << "memory_initialization_vector =\n";
      // The data words in binary representation
      for( uint32_t i=0; i<emusize; ++i )
        out << QString("%1\n").arg( emudata[i], 16, 2, QChar('0') );
    }
  else
    {
      // .mem file:
      // The data words in hexadecimal representation
      for( uint32_t i=0; i<emusize; ++i )
        out << QString("%1\n").arg( emudata[i], 16/4, 16, QChar('0') );
    }

  // Close the file
  file.close();
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeEmuDataToFlx( uint32_t *emudata,
                                        uint32_t  emusize,
                                        uint32_t  egroup )
{
  if( _felixCard == 0 ) return;

  // Disable any active emulators
  // (re-enable when the emulator data has been updated, if appropriate)
  //uint64_t emu_ena = _felixCard->readBar2Register64( GBT_EMU_ENA_I );
#if REGMAP_VERSION < 0x500
  uint64_t emu_ena = _felixCard->cfg_get_reg( REG_GBT_EMU_ENA );
  if( emu_ena != 0 )
    {
      _emuEnable = emu_ena;
      //_felixCard->writeBar2Register64( GBT_EMU_ENA_I, 0 );
      _felixCard->cfg_set_reg( REG_GBT_EMU_ENA, 0 );
    }
#else
  uint64_t emu_ena = _felixCard->cfg_get_reg( REG_FE_EMU_ENA );
  if( emu_ena != 0 )
    {
      _emuEnable = emu_ena;
      //_felixCard->writeBar2Register64( GBT_EMU_ENA_I, 0 );
      _felixCard->cfg_set_reg( REG_FE_EMU_ENA, 0 );
    }
#endif // REGMAP_VERSION

  // Write emulator data for this egroup directly into the selected FELIX card
  uint64_t egroup_bitmask = (1 << egroup);
  uint64_t addr = 0;
  uint32_t i;
#if REGMAP_VERSION < 0x500
  uint64_t addr_plus_data;
  for( i=0; i<emusize; ++i, addr+=(1ULL<<EMU_CONFIG_ADDR_SHIFT) )
    {
      addr_plus_data = emudata[i] | addr;
      _felixCard->cfg_set_reg( REG_GBT_EMU_CONFIG, addr_plus_data );
      _felixCard->cfg_set_reg( REG_GBT_EMU_CONFIG_WE_ARRAY, egroup_bitmask );
      _felixCard->cfg_set_reg( REG_GBT_EMU_CONFIG_WE_ARRAY, 0 );
#else
  for( i=0; i<emusize; ++i, addr+=1ULL )
    {
      _felixCard->cfg_set_option(BF_FE_EMU_CONFIG_WRADDR, addr);
      _felixCard->cfg_set_option(BF_FE_EMU_CONFIG_WRDATA, emudata[i]);
      _felixCard->cfg_set_option(BF_FE_EMU_CONFIG_WE, egroup_bitmask);
      _felixCard->cfg_set_option(BF_FE_EMU_CONFIG_WE, 0);
#endif // REGMAP_VERSION
    }

  // Re-enable emulator(s)
  if( _emuEnable != 0 )
    {
      //_felixCard->writeBar2Register64( GBT_EMU_ENA_I, _emuEnable );
#if REGMAP_VERSION < 0x500
      _felixCard->cfg_set_reg( REG_GBT_EMU_ENA, _emuEnable );
#else
      _felixCard->cfg_set_reg( REG_FE_EMU_ENA, _emuEnable );
#endif // REGMAP_VERSION
      _emuEnable = 0;
    }
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeFmEmuDataToFile( uint64_t *emudata,
                                           uint64_t  emusize,
                                           QString   ext )
{
  // Write emulator data to a file in the form of VHDL code

  // Compose a file name
  QString filename = QString("fmemuram") + ext;

  // Open the file
  QFile file( filename );
  if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
      QMessageBox::warning( this, "Opening Emulator Data file",
                            QString("Could not open file ")+
                            filename );
      return;
    }

  // Write to the file
  QTextStream out( &file );
  if( ext == QString(".coe") )
    {
      // A kind of header...
      out << "; 36-bit wide by " << emusize << " deep\n"
          << "memory_initialization_radix = 2;\n"
          << "memory_initialization_vector =\n";
      // The data words in binary representation
      for( uint32_t i=0; i<emusize; ++i )
        out << QString("%1\n").arg( emudata[i], 36, 2, QChar('0') );
    }
  else
    {
      // The data words in hexadecimal representation
      for( uint32_t i=0; i<emusize; ++i )
        out << QString("%1\n").arg( emudata[i], 36/4, 16, QChar('0') );
    }

  // Close the file
  file.close();
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeFmEmuDataToFlx( uint64_t *emudata,
                                          uint64_t  emusize )
{
  if( _felixCard == 0 ) return;

  // Disable any active emulators
  // (re-enable when the emulator data has been updated, if appropriate)
#if REGMAP_VERSION < 0x500
  uint64_t emu_ena = _felixCard->cfg_get_reg( REG_GBT_FM_EMU_ENA_TOHOST );
  if( emu_ena != 0 )
    {
      _emuEnable = emu_ena;
      _felixCard->cfg_set_reg( REG_GBT_FM_EMU_ENA_TOHOST, 0 );
    }
#else
  uint64_t emu_ena = _felixCard->cfg_get_option( BF_FE_EMU_ENA_EMU_TOHOST );
  if( emu_ena != 0 )
    {
      _emuEnable = emu_ena;
      _felixCard->cfg_set_option( BF_FE_EMU_ENA_EMU_TOHOST, 0 );
    }
#endif // REGMAP_VERSION
  // Write emulator data directly into the selected FLX card
#if REGMAP_VERSION < 0x500
  uint64_t addr = 0, addr_plus_data;
  uint64_t i;
  for( i=0; i<emusize; ++i, addr+=(1ULL<<FM_EMU_CONFIG_ADDR_SHIFT) )
    {
      addr_plus_data = emudata[i] | addr;
      _felixCard->cfg_set_reg( REG_GBT_FM_EMU_CONFIG, addr_plus_data );
      _felixCard->cfg_set_reg( REG_GBT_FM_EMU_CONFIG_WE_ARRAY, 1 );
      _felixCard->cfg_set_reg( REG_GBT_FM_EMU_CONFIG_WE_ARRAY, 0 );
    }
#else
  uint64_t addr = 0;
  uint64_t i;
  for( i=0; i<emusize; ++i, addr+=1ULL )
    {
      _felixCard->cfg_set_option( BF_FE_EMU_CONFIG_WRADDR, addr );
      _felixCard->cfg_set_option( BF_FE_EMU_CONFIG_WRDATA, emudata[i] );
      _felixCard->cfg_set_option( BF_FE_EMU_CONFIG_WE, 1 );
      _felixCard->cfg_set_option( BF_FE_EMU_CONFIG_WE, 0 );
    }
#endif // REGMAP_VERSION
  // Re-enable emulator(s)
  if( _emuEnable != 0 )
    {
#if REGMAP_VERSION < 0x500
      _felixCard->cfg_set_reg( REG_GBT_FM_EMU_ENA_TOHOST, _emuEnable );
#else
      _felixCard->cfg_set_option( BF_FE_EMU_ENA_EMU_TOHOST, _emuEnable );
#endif // REGMAP_VERSION
      _emuEnable = 0;
    }
}

// ----------------------------------------------------------------------------

QString GenerateDialog::flxVersionString()
{
  QString qs;
  if( _felixCard == 0 ) return qs;
  qs = QString::fromStdString( _felixCard->firmware_string() );
  return qs;
}

// ----------------------------------------------------------------------------

void GenerateDialog::readSettings()
{
  QSettings settings( "NIKHEF", "elinkconfig" );

  //int link_nr = settings.value( "linkNr", 0 ).toInt();
  //if( link_nr != -1 && link_nr < _spinBoxLink->maximum() )
  //  _spinBoxLink->setValue( link_nr );

  bool all_links = settings.value( "allLinks", true ).toBool();
  _checkBoxAllLinks->setChecked( all_links );

  //int flx = settings.value( "selectedFlx", 0 ).toInt();
  //if( flx != -1 && flx < _comboBoxFelix->count() )
  //  _comboBoxFelix->setCurrentIndex( flx );

  bool extra_settings = settings.value( "extraSettings", true ).toBool();
  _checkBoxExtraSettings->setChecked( extra_settings );

  int sz = settings.value( "chunkSize", 0 ).toInt();
  if( sz != -1 && sz <= _spinBoxChunkSize->maximum() )
    _spinBoxChunkSize->setValue( sz );

  bool dep = settings.value( "eWidthDepSize", false ).toBool();
  _checkBoxWidthPropSize->setChecked( dep );

  bool ran = settings.value( "randomSize", false ).toBool();
  _checkBoxRandomSize->setChecked( ran );

  bool strid = settings.value( "streamId", false ).toBool();
  _checkBoxStreamId->setChecked( strid );

  //bool soc = settings.value( "socIsK287", false ).toBool();
  //_checkBoxSocK287->setChecked( soc );
  _checkBoxSocK287->setChecked( false );

  int idles = settings.value( "idleChars", 8 ).toInt();
  if( idles != -1 && idles <= _spinBoxIdleChars->maximum() )
    _spinBoxIdleChars->setValue( idles );

  bool dev = settings.value( "developerOptions", false ).toBool();
  _checkBoxDeveloper->setChecked( dev );

  int patt = settings.value( "selectedPattern", 0 ).toInt();
  if( patt != -1 && patt < _comboBoxPattern->count() )
    _comboBoxPattern->setCurrentIndex( patt );

  bool lsb = settings.value( "lsbFirst", false ).toBool();
  _checkBoxLsbFirst->setChecked( lsb );
}

// ----------------------------------------------------------------------------

void GenerateDialog::writeSettings()
{
  // To prevent funny message on terminal:
  // "setNativeLocks failed: Resource temporarily unavailable"
  // (due to Qt bug), use setValue() only when necessary (1 Feb 2017)

  QSettings settings( "NIKHEF", "elinkconfig" );

  //settings.setValue( "linkNr", _spinBoxLink->value() );

  bool all_links = settings.value( "allLinks", true ).toBool();
  if( _checkBoxAllLinks->isChecked() != all_links )
    settings.setValue( "allLinks", _checkBoxAllLinks->isChecked() );

  //int flx = settings.value( "selectedFlx", 0 ).toInt();
  //if( _comboBoxFelix->currentIndex() != flx )
  //  settings.setValue( "selectedFlx", _comboBoxFelix->currentIndex() );

  bool extra_settings = settings.value( "extraSettings", true ).toBool();
  if( _checkBoxExtraSettings->isChecked() != extra_settings )
    settings.setValue( "extraSettings", _checkBoxExtraSettings->isChecked() );

  int sz = settings.value( "chunkSize", 0 ).toInt();
  if( _spinBoxChunkSize->value() != sz )
    settings.setValue( "chunkSize", _spinBoxChunkSize->value() );

  bool dep = settings.value( "eWidthDepSize", false ).toBool();
  if( _checkBoxWidthPropSize->isChecked() != dep )
    settings.setValue( "eWidthDepSize", _checkBoxWidthPropSize->isChecked() );

  bool ran = settings.value( "randomSize", false ).toBool();
  if( _checkBoxRandomSize->isChecked() != ran )
    settings.setValue( "randomSize", _checkBoxRandomSize->isChecked() );

  bool strid = settings.value( "streamId", false ).toBool();
  if( _checkBoxStreamId->isChecked() != strid )
    settings.setValue( "streamId", _checkBoxStreamId->isChecked() );

  //settings.setValue( "socIsK287", _checkBoxSocK287->isChecked() );

  int idles = settings.value( "idleChars", 8 ).toInt();
  if( _spinBoxIdleChars->value() != idles )
    settings.setValue( "idleChars", _spinBoxIdleChars->value() );

  bool dev = settings.value( "developerOptions", false ).toBool();
  if( _checkBoxDeveloper->isChecked() != dev )
    settings.setValue( "developerOptions", _checkBoxDeveloper->isChecked() );

  int patt = settings.value( "selectedPattern", 0 ).toInt();
  if( _comboBoxPattern->currentIndex() != patt )
    settings.setValue( "selectedPattern", _comboBoxPattern->currentIndex() );

  bool lsb = settings.value( "lsbFirst", false ).toBool();
  if( _checkBoxLsbFirst->isChecked() != lsb )
    settings.setValue( "lsbFirst", _checkBoxLsbFirst->isChecked() );
}

// ----------------------------------------------------------------------------

void GenerateDialog::closeEvent( QCloseEvent *event )
{
  // When quitting the application save some of the current settings
  this->writeSettings();
  event->accept();
}

// ----------------------------------------------------------------------------
